DEFINES+=QTUITEST_TARGET

enable_valgrind:DEFINES+=QTUITEST_HAVE_VALGRIND

PRIVATE_HEADERS +=        \
    qeventwatcher_p.h     \
    qinputgenerator_p.h   \
    qtuitestconnectionmanager_p.h \
    qtuitestwidgets_p.h

SEMI_PRIVATE_HEADERS += \
    qalternatestack_p.h \
    qtuitestelapsedtimer_p.h   \
    qtestprotocol_p.h   \
    recordevent_p.h

HEADERS +=                     \
    qtuitestglobal.h          \
    qtuitestnamespace.h       \
    qtuitestrecorder.h        \
    qtuitestwidgetinterface.h \
    testwidget.h

SOURCES +=                        \
    qtuitestelapsedtimer.cpp      \
    qeventwatcher.cpp             \
    qinputgenerator.cpp           \
    qtestprotocol.cpp             \
    qtuitestconnectionmanager.cpp \
    qtuitestnamespace.cpp       \
    qtuitestrecorder.cpp        \
    qtuitestwidgetinterface.cpp \
    qtuitestwidgets.cpp \
    testwidget.cpp

unix:!symbian {
    SOURCES+=qalternatestack_unix.cpp
    embedded:SOURCES += qinputgenerator_qws.cpp
    !embedded:!mac {
        SOURCES += qinputgenerator_x11.cpp
        LIBS    += -lXtst
    }
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
    DESTDIR=$$BUILDROOT/lib
    target.path=$$[QT_INSTALL_LIBS]
    INSTALLS+=target
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData NetworkServices
    SOURCES += qalternatestack_symbian.cpp qinputgenerator_symbian.cpp
    LIBS += -lws32
    LIBS += -leuser -lcone
}

mac {
    SOURCES += qinputgenerator_mac.cpp
    LIBS += -framework ApplicationServices -framework Carbon
}

win32 {
    SOURCES += qalternatestack_win.cpp  \
               qinputgenerator_win.cpp
    LIBS += -luser32
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
}

symbian {
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
    CONFIG+=staticlib
}

TEMPLATE=lib
QT+=network
HEADERS+=$$SEMI_PRIVATE_HEADERS $$PRIVATE_HEADERS
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
TARGET=qtuitest
TARGET=$$qtLibraryTarget($$TARGET)
CONFIG-=debug_and_release_target

maemo5|maemo6 {
    target.path=$$[QT_INSTALL_LIBS]
    INSTALLS+=target
}
