/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qalternatestack_p.h"
struct QAlternateStackPrivate
{
    // All instances of QAlternateStack.
//    static QList<QAlternateStack*> instances;
};

//QList<QAlternateStack*> QAlternateStackPrivate::instances;

QAlternateStack::QAlternateStack(quint32 size, QObject* parent)
    : QObject(parent),
      d(new QAlternateStackPrivate)
{
    Q_UNUSED(size);
    Q_UNUSED(parent);
    qFatal("QAlternateStack: not available on this platform");
}

QAlternateStack::~QAlternateStack()
{
}

void QAlternateStack::start(QAlternateStackEntryPoint entry, const QVariant& data)
{
    Q_UNUSED(entry);
    Q_UNUSED(data);
}

void QAlternateStack::switchTo()
{
}

void QAlternateStack::switchFrom()
{
}

bool QAlternateStack::isActive() const
{ return false; }

bool QAlternateStack::isCurrentStack() const
{ return false; }

QList<QAlternateStack*> QAlternateStack::instances()
{ static QList<QAlternateStack*> l;
  return l;
  //QAlternateStackPrivate::instances; }
}

bool QAlternateStack::isAvailable()
{ return false; }

