/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTUITESTWIDGETS_P_H
#define QTUITESTWIDGETS_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtUiTest API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QObject>
#include <QMetaType>
#include <QHash>
#include <Qt>
#include <qtuitestnamespace.h>

class QtUiTestWidgetsPrivate;
class QPoint;

class QTUITEST_EXPORT QtUiTestWidgets : public QObject
{
    Q_OBJECT

public:
    static QtUiTestWidgets* instance();

    virtual ~QtUiTestWidgets();

    void setInputOption(QtUiTest::InputOption,bool = true);
    bool testInputOption(QtUiTest::InputOption) const;

    bool mousePreferred() const;
    void setMousePreferred(bool);

    QtUiTest::LabelOrientation labelOrientation() const;
    void setLabelOrientation(QtUiTest::LabelOrientation);

    QString errorString() const;
    void setErrorString(QString const&);

    void registerFactory(QtUiTest::WidgetFactory*);

    void mousePress  (QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            QtUiTest::InputOption = QtUiTest::NoOptions);
    void mouseRelease(QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            QtUiTest::InputOption = QtUiTest::NoOptions);
    void mouseClick  (QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            QtUiTest::InputOption = QtUiTest::NoOptions);

    void keyPress  (Qt::Key,Qt::KeyboardModifiers = 0,QtUiTest::InputOption = QtUiTest::NoOptions);
    void keyRelease(Qt::Key,Qt::KeyboardModifiers = 0,QtUiTest::InputOption = QtUiTest::NoOptions);
    void keyClick  (Qt::Key,Qt::KeyboardModifiers = 0,QtUiTest::InputOption = QtUiTest::NoOptions);

private:
    Q_DISABLE_COPY(QtUiTestWidgets)

    friend class QtUiTestWidgetsPrivate;

    QtUiTestWidgetsPrivate* d;

    QtUiTestWidgets();
    QObject* findWidget(QtUiTest::WidgetType);
    QObject* testWidget(QObject*,QByteArray const&);

    void refreshPlugins();
    void clear();

    Q_PRIVATE_SLOT(d, void _q_objectDestroyed())
    Q_PRIVATE_SLOT(d, void _q_postNextKeyEvent())
    Q_PRIVATE_SLOT(d, void _q_postNextMouseEvent())

    friend QObject* QtUiTest::testWidget(QObject*,const char*);
    friend QObject* QtUiTest::findWidget(QtUiTest::WidgetType);
};

#endif

