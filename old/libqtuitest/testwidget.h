/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTWIDGET_H
#define TESTWIDGET_H

#include <QObject>
#include <qtuitestwidgetinterface.h>

class QWidget;

namespace QtUiTest {

class QTUITEST_EXPORT TestWidget : public QObject, public QtUiTest::Widget,
    public QtUiTest::InputWidget
{
    Q_OBJECT
    Q_INTERFACES(QtUiTest::Widget QtUiTest::InputWidget)

public:
    TestWidget(QObject*);

    virtual const QRect& geometry() const;
    virtual QRect rect() const;
    virtual bool isVisible() const;
    virtual QRegion visibleRegion() const;
    virtual const QObjectList &children() const;
    virtual QObject* parent() const;
    virtual QString windowTitle() const;
    virtual QPoint mapToGlobal(QPoint const&) const;
    virtual QPoint mapFromGlobal(QPoint const&) const;
    virtual bool hasFocus() const;
    virtual Qt::WindowFlags windowFlags() const;
    virtual bool ensureVisibleRegion(QRegion const&);
    virtual bool canEnter(QVariant const&) const;
    virtual bool enter(QVariant const&,bool);
    virtual void focusOutEvent();

#ifdef Q_WS_QWS
    virtual bool hasEditFocus() const;
#endif
    virtual bool setEditFocus(bool);
    virtual QObject* focusProxy() const;
    virtual Qt::FocusPolicy focusPolicy () const;
    virtual bool grabPixmap(QPixmap &pixmap) const;

    static bool canWrap(QObject*);
    static QString printable(QString const&);
    static QString labelText(QString const&);
    virtual QVariant getProperty(const QString&) const;
    virtual bool setProperty(const QString&, const QVariant&);
    virtual QObject *inputProxy() const;

signals:
    void gotFocus();

protected:
    bool eventFilter(QObject*,QEvent*);

private:
    QWidget *q;
};

}

#endif

