/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtuitestelapsedtimer_p.h"

#include <time.h>
#include <errno.h>
#include <string.h>
#include <QtGlobal>

/*!
    \internal
    \class QtUiTestElapsedTimer
    \inpublicgroup QtUiTestModule

    QtUiTestElapsedTimer provides a lightweight
    subset of the QTime API while keeping correct time even if the system time
    is changed.
*/

int qtuitestelapsedtimer_gettime() {
#if _POSIX_TIMERS
    struct timespec tms;
    if (-1 == clock_gettime(
#ifdef _POSIX_MONOTONIC_CLOCK
                CLOCK_MONOTONIC
#else
                CLOCK_REALTIME
#endif
                , &tms)) {
        qFatal("QtUitest: clock_gettime failed: %s",
               strerror(errno));
    }
    return tms.tv_sec*1000 + tms.tv_sec/1000000;
#else
    /*
        FIXME: won't keep correct time if system time is changed during test.
        Should warn about this?
    */
    return time(0)*1000;
#endif
}

/*!
    Creates a new timer.
    start() is automatically called on the new timer.
*/
QtUiTestElapsedTimer::QtUiTestElapsedTimer()
{ start(); }

/*!
    Starts or restarts the timer.
*/
void QtUiTestElapsedTimer::start()
{ start_ms = qtuitestelapsedtimer_gettime(); }

/*!
    Returns the elapsed time (in milliseconds) since start() was called.

    This function returns the correct value even if the system time has
    been changed.  The value may overflow, however.
 */
int QtUiTestElapsedTimer::elapsed()
{ return qtuitestelapsedtimer_gettime() - start_ms; }

