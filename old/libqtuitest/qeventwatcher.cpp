/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qeventwatcher_p.h"

#include <QSet>
#include <QPointer>
#include <QCoreApplication>

struct QEventWatcherPrivate
{
    QSet<QObject*>              objects;
    QList<QEventWatcherFilter*> filters;
    int                         count;
};

QEventWatcher::QEventWatcher(QObject* parent)
    : QObject(parent),
      d(new QEventWatcherPrivate)
{
    d->count = 0;
}

QEventWatcher::~QEventWatcher()
{
    qDeleteAll(d->filters);
    delete d;
    d = 0;
}

void QEventWatcher::addObject(QObject* obj)
{
    d->objects << obj;
    qApp->installEventFilter(this);
}

void QEventWatcher::addFilter(QEventWatcherFilter* filter)
{ d->filters << filter; }

int QEventWatcher::count() const
{ return d->count; }

QString QEventWatcher::toString() const
{
    QString ret;
    QString sep;
    for (int i = d->filters.count()-1; i >= 0; --i) {
        ret += sep + d->filters.at(i)->toString();
        sep = ", ";
    }
    return ret;
}

bool QEventWatcher::eventFilter(QObject* obj, QEvent* e)
{
    if (!d->objects.contains(obj)) return false;

    bool accept = (d->filters.count() ? false : true);
    for (int i = d->filters.count()-1; i >= 0 && !accept; --i) {
        accept = d->filters.at(i)->accept(obj,e);
    }

    if (accept) {
        ++d->count;
        emit event(obj, e->type());
    }

    return false;
}

