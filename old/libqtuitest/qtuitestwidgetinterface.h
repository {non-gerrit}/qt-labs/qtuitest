/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTUITESTWIDGETINTERFACE_H
#define QTUITESTWIDGETINTERFACE_H

#include <QStringList>
#include <QVariantList>
#include <QtGlobal>
#include <qtuitestnamespace.h>
#include <qtuitestglobal.h>

class QRect;
class QRegion;
class QPoint;
class QPixmap;

namespace QtUiTest
{
#ifndef Q_QDOC
    template<class T> T qtuitest_cast_helper(QObject*,T);
#endif

#define QTUITEST_INTERFACE(Klass)                                    \
    public:                                                            \
        virtual ~Klass() {}                                            \
        static const char* _q_interfaceName() { return #Klass; }       \
    private:                                                           \
        template<class T> friend T qtuitest_cast_helper(QObject*,T);

    class QTUITEST_EXPORT WidgetFactory
    {
        QTUITEST_INTERFACE(WidgetFactory)
    public:
        virtual QObject* create(QObject*) = 0;
        virtual QObject* find(QtUiTest::WidgetType);
        virtual QStringList keys() const = 0;
    };

    class QTUITEST_EXPORT Widget
    {
        QTUITEST_INTERFACE(Widget)
    public:
        virtual const QRect& geometry() const = 0;
        virtual QRect rect() const;
        virtual int x() const;
        virtual int y() const;
        virtual int width() const;
        virtual int height() const;
        virtual bool isVisible() const = 0;
        virtual QRegion visibleRegion() const = 0;
        virtual QObject* parent() const = 0;
        virtual QString windowTitle() const;
        virtual const QObjectList &children() const = 0;
        virtual void descendants(QObjectList &list) const;
        virtual QRegion childrenVisibleRegion() const;
        virtual QPoint mapToGlobal(const QPoint&) const = 0;
        virtual QPoint mapFromGlobal(const QPoint&) const = 0;
        virtual QPoint center() const;
        virtual bool ensureVisibleRegion(const QRegion&) = 0;
        bool ensureVisiblePoint(const QPoint&);
        virtual bool setFocus();
        virtual void focusOutEvent();
        virtual bool hasFocus() const = 0;
        virtual QObject* focusProxy() const;
        virtual Qt::FocusPolicy focusPolicy () const;
        virtual Qt::WindowFlags windowFlags() const;
#ifdef Q_WS_QWS
        virtual bool hasEditFocus() const = 0;
        virtual bool setEditFocus(bool)   = 0;
#else
        virtual bool hasEditFocus() const;
        virtual bool setEditFocus(bool);
#endif
        virtual bool inherits(QtUiTest::WidgetType) const;
        virtual bool grabPixmap(QPixmap &pixmap) const;
        virtual bool ignoreScan() const;
        virtual bool ignoreBuddy() const;
        virtual QVariant getProperty(const QString&) const;
        virtual bool setProperty(const QString&, const QVariant&);

#ifdef Q_QDOC
    signals:
        void gotFocus();
#endif
    };

    class QTUITEST_EXPORT ActivateWidget
    {
        QTUITEST_INTERFACE(ActivateWidget)
    public:
        virtual bool activate() = 0;

#ifdef Q_QDOC
    signals:
        void activated();
#endif
    };

    class QTUITEST_EXPORT LabelWidget
    {
        QTUITEST_INTERFACE(LabelWidget)
    public:
        virtual QString labelText() const = 0;
        virtual QObject* buddy() const;
    };

    class QTUITEST_EXPORT CheckWidget
    {
        QTUITEST_INTERFACE(CheckWidget)
    public:
        virtual bool isTristate() const;
        virtual Qt::CheckState checkState() const = 0;
        virtual bool setCheckState(Qt::CheckState);

#ifdef Q_QDOC
    signals:
        void stateChanged(int);
#endif
    };

    class QTUITEST_EXPORT TextWidget
    {
        QTUITEST_INTERFACE(TextWidget)
    public:
        virtual QString selectedText() const;
        virtual QVariant selectedValue() const;
        virtual QString text() const = 0;
        virtual QVariant value() const;
    };

    class QTUITEST_EXPORT ListWidget
    {
        QTUITEST_INTERFACE(ListWidget)
    public:
        virtual QStringList list() const = 0;
        virtual QRect visualRect(const QString&) const = 0;
        virtual bool ensureVisible(const QString&);
    };

    class QTUITEST_EXPORT InputWidget
    {
        QTUITEST_INTERFACE(InputWidget)
    public:
        virtual bool canEnter(const QVariant&) const = 0;
        virtual bool enter(const QVariant&,bool) = 0;
        virtual QObject *inputProxy() const;

#ifdef Q_QDOC
    signals:
        void entered(const QVariant&);
#endif
    };

    class QTUITEST_EXPORT SelectWidget
    {
        QTUITEST_INTERFACE(SelectWidget)
    public:
        virtual bool isMultiSelection() const;

        virtual bool canSelect(const QString&) const = 0;
        virtual bool canSelectMulti(const QStringList&) const;
        virtual bool select(const QString&) = 0;
        virtual bool selectMulti(const QStringList&);

#ifdef Q_QDOC
    signals:
        void selected(const QString&);
#endif
    };

    class QTUITEST_EXPORT CheckItemWidget
    {
        QTUITEST_INTERFACE(CheckItemWidget)
    public:
        virtual bool isCheckable(const QString&);
        virtual bool isChecked(const QString&) const = 0;
        virtual bool setChecked(const QString&, bool) = 0;
    };

    class QTUITEST_EXPORT IndexedWidget
    {
        QTUITEST_INTERFACE(IndexedWidget)
    public:
        virtual bool selectIndex(const QVariantList&);
        virtual QVariantList selectedIndex() const;
    };

};

Q_DECLARE_INTERFACE(
        QtUiTest::WidgetFactory,
        "com.nokia.qt.QtUiTest.WidgetFactory/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::Widget,
        "com.nokia.qt.QtUiTest.Widget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::ActivateWidget,
        "com.nokia.qt.QtUiTest.ActivateWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::LabelWidget,
        "com.nokia.qt.QtUiTest.LabelWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::CheckWidget,
        "com.nokia.qt.QtUiTest.CheckWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::TextWidget,
        "com.nokia.qt.QtUiTest.TextWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::ListWidget,
        "com.nokia.qt.QtUiTest.ListWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::InputWidget,
        "com.nokia.qt.QtUiTest.InputWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::SelectWidget,
        "com.nokia.qt.QtUiTest.SelectWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::CheckItemWidget,
        "com.nokia.qt.QtUiTest.CheckItemWidget/1.0")
Q_DECLARE_INTERFACE(
        QtUiTest::IndexedWidget,
        "com.nokia.qt.QtUiTest.IndexedWidget/1.0")
#endif

