/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qinputgenerator_p.h"

#include <QApplication>
#include <QPoint>
#include <QWidget>

#define QINPUTGENERATOR_DEBUG() if (1); else qDebug() << "QInputGenerator:"

Qt::KeyboardModifier const QInputGenerator::AllModifiers[] =
    { Qt::ShiftModifier, Qt::ControlModifier, Qt::AltModifier, Qt::MetaModifier };

/*
    Given a co-ordinate relative to the application's currently active window,
    maps it to this input generator's co-ordinate system.
*/
QPoint QInputGenerator::mapFromActiveWindow(QPoint const& pos) const
{
    QPoint ret(pos);

    QWidget *w = QApplication::activeWindow();
    if (!w) w = QApplication::activePopupWidget();
    if (!w) w = QApplication::activeModalWidget();

    if (w) {
       ret = w->mapToGlobal(pos);
    }

    return ret;
}

Qt::Key QInputGenerator::modifierToKey(Qt::KeyboardModifier mod)
{
    switch (mod) {
        case Qt::ShiftModifier:
            return Qt::Key_Shift;
        case Qt::ControlModifier:
            return Qt::Key_Control;
        case Qt::AltModifier:
            return Qt::Key_Alt;
        case Qt::MetaModifier:
            return Qt::Key_Meta;
        default:
            break;
    }
    return Qt::Key(0);
}

