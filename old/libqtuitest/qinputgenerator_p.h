/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QKEYGENERATOR_P_H
#define QKEYGENERATOR_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtUiTest API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QtGlobal>
#include <QObject>
#include <qtuitestglobal.h>

class QPoint;
class QInputGeneratorPrivate;

class QTUITEST_EXPORT QInputGenerator : public QObject
{
Q_OBJECT
public:
    explicit QInputGenerator(QObject* =0);
    virtual ~QInputGenerator();

    void keyPress  (Qt::Key, Qt::KeyboardModifiers, bool=false);
    void keyRelease(Qt::Key, Qt::KeyboardModifiers);
    void keyClick  (Qt::Key, Qt::KeyboardModifiers);

    void mousePress  (QPoint const&, Qt::MouseButtons);
    void mouseRelease(QPoint const&, Qt::MouseButtons);
    void mouseClick  (QPoint const&, Qt::MouseButtons);

    QPoint mapFromActiveWindow(QPoint const&) const;

private:
    QInputGeneratorPrivate* d;
    friend class QInputGeneratorPrivate;

    static Qt::Key modifierToKey(Qt::KeyboardModifier);
    static Qt::KeyboardModifier const AllModifiers[4];
};

#endif

