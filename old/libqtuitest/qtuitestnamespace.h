/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTUITESTNAMESPACE_H
#define QTUITESTNAMESPACE_H

#include <qtuitestglobal.h>

#include <QEvent>
#include <QObject>
#include <Qt>

class QPoint;

namespace QtUiTest
{
    enum InputOption {
        NoOptions      = 0x0,
        DemoMode       = 0x1,
        KeyRepeat      = 0x2
    };

    enum WidgetType {
        InputMethod,
        TabBar
    };

    enum Key {
#ifdef Q_WS_QWS
        Key_Activate = Qt::Key_Select,
#else
        Key_Activate = Qt::Key_Enter,
#endif

#ifdef Q_WS_QWS
        Key_Select   = Qt::Key_Select,
#else
        Key_Select   = Qt::Key_Space,
#endif

#if defined(Q_WS_QWS) || defined (Q_OS_SYMBIAN)
        Key_ActivateButton   = Qt::Key_Select
#else
        Key_ActivateButton   = Qt::Key_Space
#endif
    };

    enum LabelOrientation {
        LabelLeft       = 0x01,
        LabelRight      = 0x02,
        LabelAbove      = 0x03,
        LabelBelow      = 0x04
    };

    QTUITEST_EXPORT void setInputOption(InputOption,bool = true);
    QTUITEST_EXPORT bool testInputOption(InputOption);

    QTUITEST_EXPORT QString errorString();
    QTUITEST_EXPORT void setErrorString(QString const&);

    QTUITEST_EXPORT void mousePress  (QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            InputOption = NoOptions);
    QTUITEST_EXPORT void mouseRelease(QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            InputOption = NoOptions);
    QTUITEST_EXPORT void mouseClick  (QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            InputOption = NoOptions);
    QTUITEST_EXPORT bool mouseClick  (QObject*,QPoint const&,Qt::MouseButtons = Qt::LeftButton,
            InputOption = NoOptions);
    QTUITEST_EXPORT bool mouseClick  (QObject*,QByteArray const&,QPoint const&,
            Qt::MouseButtons = Qt::LeftButton, InputOption = NoOptions);

    QTUITEST_EXPORT void keyPress  (int,Qt::KeyboardModifiers = 0,
            InputOption = NoOptions);
    QTUITEST_EXPORT void keyRelease(int,Qt::KeyboardModifiers = 0,
            InputOption = NoOptions);
    QTUITEST_EXPORT void keyClick  (int,Qt::KeyboardModifiers = 0,
            InputOption = NoOptions);
    QTUITEST_EXPORT bool keyClick  (QObject*,int,Qt::KeyboardModifiers = 0,
            InputOption = NoOptions);
    QTUITEST_EXPORT bool keyClick  (QObject*,QByteArray const&,int,Qt::KeyboardModifiers = 0,
            InputOption = NoOptions);

    QTUITEST_EXPORT bool mousePreferred();
    QTUITEST_EXPORT void setMousePreferred(bool);

    QTUITEST_EXPORT LabelOrientation labelOrientation();
    QTUITEST_EXPORT void setLabelOrientation(LabelOrientation);

    QTUITEST_EXPORT int maximumUiTimeout();

    QTUITEST_EXPORT Qt::Key asciiToKey(char);
    QTUITEST_EXPORT Qt::KeyboardModifiers asciiToModifiers(char);

    QTUITEST_EXPORT QObject* findWidget(WidgetType);

    QTUITEST_EXPORT QObject* testWidget(QObject*,const char*);

    QTUITEST_EXPORT bool connectFirst   (const QObject*, const char*, const QObject*, const char*);
    QTUITEST_EXPORT bool disconnectFirst(const QObject*, const char*, const QObject*, const char*);

    template<class T> inline T qtuitest_cast_helper(QObject* object, T)
    {
        T ret;
        if ((ret = qobject_cast<T>(object))) {}
        else {
            ret = qobject_cast<T>(QtUiTest::testWidget(object,
                        static_cast<T>(0)->_q_interfaceName()));
        }
        return ret;
    }

    QTUITEST_EXPORT void wait(int);
    QTUITEST_EXPORT bool waitForSignal(QObject*, const char*, int = QtUiTest::maximumUiTimeout(), Qt::ConnectionType = Qt::QueuedConnection);
    QTUITEST_EXPORT bool waitForEvent(QObject*, QEvent::Type, int = QtUiTest::maximumUiTimeout(), Qt::ConnectionType = Qt::QueuedConnection);
    QTUITEST_EXPORT bool waitForEvent(QObject*, QList<QEvent::Type> const&, int = QtUiTest::maximumUiTimeout(), Qt::ConnectionType = Qt::QueuedConnection);
};

template<class T> inline
T qtuitest_cast(const QObject* object)
{
    return QtUiTest::qtuitest_cast_helper<T>(const_cast<QObject*>(object),0);
}

#include <qtuitestwidgetinterface.h>

#endif

