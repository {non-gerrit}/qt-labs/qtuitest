/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testwidget.h"

#include <QAbstractScrollArea>
#include <QApplication>
#include <QScrollBar>
#include <QVariant>
#include <QWidget>
#include <QGraphicsProxyWidget>
#include <QInputContext>

namespace QtUiTest {

TestWidget::TestWidget(QObject* _q)
    : q(qobject_cast<QWidget*>(_q))
{ q->installEventFilter(this); }

bool TestWidget::eventFilter(QObject*,QEvent* e)
{
    if (e->type() == QEvent::FocusIn) {
        emit gotFocus();
    }
    return false;
}

const QRect& TestWidget::geometry() const
{ return q->geometry(); }

QRect TestWidget::rect() const
{ return q->rect(); }

bool TestWidget::isVisible() const
{ return q->isVisible(); }

QRegion TestWidget::visibleRegion() const
{ return q->visibleRegion(); }

bool TestWidget::ensureVisibleRegion(const QRegion& region)
{
    if (region.intersected(visibleRegion()) == region) return true;

    if (!QtUiTest::mousePreferred()) {
        /*
            FIXME this is technically not correct, since we're assuming
            that giving the widget keyboard focus makes the entire widget
            visible.  A low priority fix until we come across a case where
            this matters.
        */
        return setFocus();
    }

    /* Try to find a scroll area which contains this widget, then scroll. */

    QAbstractScrollArea *sa = 0;
    QWidget *parent = q->parentWidget();
    while (parent && !sa) {
        sa = qobject_cast<QAbstractScrollArea*>(parent);
        parent = parent->parentWidget();
    }

    if (!sa) return false;

    /* Figure out the points to click for scrolling in each direction */
    QScrollBar *vbar = sa->verticalScrollBar();
    QScrollBar *hbar = sa->horizontalScrollBar();
    QPoint up    = vbar->mapToGlobal(QPoint(vbar->width()/2,5));
    QPoint down  = vbar->mapToGlobal(QPoint(vbar->width()/2,vbar->height()-5));
    QPoint left  = hbar->mapToGlobal(QPoint(5,              hbar->height()/2));
    QPoint right = hbar->mapToGlobal(QPoint(hbar->width()-5,hbar->height()/2));

    QRect brect_origin = region.boundingRect();
    QRect brect = brect_origin;
    brect.moveTopLeft(q->mapTo(sa, brect.topLeft()));

    static const int MAX_CLICKS = 200;
    int clicks = 0;

    /* Handle up... */
    while (brect.top() < 0) {
        if (!vbar->isVisible()) return false;
        QtUiTest::mouseClick(up);
        QtUiTest::waitForSignal(vbar, SIGNAL(valueChanged(int)));
        brect = brect_origin;
        brect.moveTopLeft(q->mapTo(sa, brect.topLeft()));
        if (++clicks > MAX_CLICKS) return false;
    }
    /* Handle down... */
    while (brect.bottom() > sa->height()) {
        if (!vbar->isVisible()) return false;
        QtUiTest::mouseClick(down);
        QtUiTest::waitForSignal(vbar, SIGNAL(valueChanged(int)));
        brect = brect_origin;
        brect.moveTopLeft(q->mapTo(sa, brect.topLeft()));
        if (++clicks > MAX_CLICKS) return false;
    }
    /* Handle left... */
    while (brect.left() < 0) {
        if (!hbar->isVisible()) return false;
        QtUiTest::mouseClick(left);
        QtUiTest::waitForSignal(hbar, SIGNAL(valueChanged(int)));
        brect = brect_origin;
        brect.moveTopLeft(q->mapTo(sa, brect.topLeft()));
        if (++clicks > MAX_CLICKS) return false;
    }
    /* Handle right... */
    while (brect.right() > sa->width()) {
        if (!hbar->isVisible()) return false;
        QtUiTest::mouseClick(right);
        QtUiTest::waitForSignal(hbar, SIGNAL(valueChanged(int)));
        brect = brect_origin;
        brect.moveTopLeft(q->mapTo(sa, brect.topLeft()));
        if (++clicks > MAX_CLICKS) return false;
    }
    return true;
}

const QObjectList &TestWidget::children() const
{ return q->children(); }

QObject* TestWidget::parent() const
{ return q->graphicsProxyWidget() ? q->graphicsProxyWidget() : q->parent(); }

QString TestWidget::windowTitle() const
{ return q->windowTitle(); }

QPoint TestWidget::mapToGlobal(QPoint const &local) const
{
    QGraphicsProxyWidget *pw;
    if ( (pw = q->graphicsProxyWidget()) ) {
        QtUiTest::Widget *w = qtuitest_cast<QtUiTest::Widget*>(pw);
        if (w) {
            return w->mapToGlobal(local);
        }
    }
    return q->mapToGlobal(local);
}

QPoint TestWidget::mapFromGlobal(QPoint const &global) const
{
    return q->mapFromGlobal(global);
}

bool TestWidget::hasFocus() const
{
    return q->hasFocus();
}

Qt::WindowFlags TestWidget::windowFlags() const
{
    return q->windowFlags();
}

bool TestWidget::canEnter(QVariant const&) const
{
    return false;
}

bool TestWidget::enter(QVariant const& item, bool noCommit)
{
    Q_UNUSED(noCommit);
    if (!hasFocus() && !setFocus()) return false;

    using namespace QtUiTest;

    /* If there's text currently in the field then erase it first */
    if (QtUiTest::mousePreferred()) {
        return false;
    }

    foreach (QChar const& c, item.toString()) {
        keyClick( asciiToKey(c.toLatin1()), asciiToModifiers(c.toLatin1()) );
    }
    return true;
}

void TestWidget::focusOutEvent()
{
}

#ifdef Q_WS_QWS
bool TestWidget::hasEditFocus() const
{ return q->hasEditFocus(); }
#endif

bool TestWidget::setEditFocus(bool enable)
{
    if (hasEditFocus() == enable) return true;

    if (!hasFocus()) {
        if (!setFocus()) return false;
    }

    // It is possible that giving us regular focus also gave us edit focus.
    if (hasEditFocus() == enable) return true;

#ifdef Q_WS_QWS
    if (!QtUiTest::keyClick(q, QtUiTest::Key_Activate)) return false;
    if (hasEditFocus() != enable && !QtUiTest::waitForEvent(q, enable ? QEvent::EnterEditFocus : QEvent::LeaveEditFocus)) {
        return false;
    }
    return (hasEditFocus() == enable);
#else
    return true;
#endif
}

QObject* TestWidget::focusProxy() const
{
    return q->focusProxy();
}

Qt::FocusPolicy TestWidget::focusPolicy () const
{
    return q->focusPolicy();
}

bool TestWidget::canWrap(QObject *o)
{ return qobject_cast<QWidget*>(o); }

QString TestWidget::printable(QString const& str)
{
    QString ret(str);

    ret.replace(QChar::Nbsp, " ");
    ret.remove(QChar(0x200E));
    ret.remove(QChar(0x200F));
    ret.remove(QChar(0x00AD));

    return ret;
}

bool TestWidget::grabPixmap(QPixmap &pixmap) const
{
    pixmap = QPixmap::grabWidget( q );
    return true;
}

QString TestWidget::labelText(const QString& text)
{
    return text.trimmed().replace(QRegExp("&(.)"), "\\1");
}

QVariant TestWidget::getProperty(const QString& name) const
{
    return q->property(name.toAscii());
}

bool TestWidget::setProperty(const QString& name, const QVariant& value)
{
    return q->setProperty(name.toAscii(), value);
}

QObject* TestWidget::inputProxy() const
{
    return q->inputContext();
}

}