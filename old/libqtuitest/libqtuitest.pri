INCLUDEPATH += $$SRCROOT/libqtuitest

symbian {
    LIBS+=-L$$OUT_PWD/ -lqtuitest
}

mac {
    CONFIG(debug,debug|release): LIBS += -L$$BUILDROOT/lib -lqtuitest_debug
    CONFIG(release,debug|release): LIBS += -L$$BUILDROOT/lib -lqtuitest
}

win32 {
    CONFIG(debug,debug|release):  LIBS+= -L$$BUILDROOT/libqtuitest -lqtuitestd
    CONFIG(release,debug|release):LIBS+= -L$$BUILDROOT/libqtuitest -lqtuitest
}

unix:!symbian {
    LIBS += -L$$BUILDROOT/lib -lqtuitest
}
