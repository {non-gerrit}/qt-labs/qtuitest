# This script converts the Test House BAT LaTeX documents into
# twiki markup which is suitable for QtUiTest.

sub printTestFunc {
    my $funcName = shift(@_);
    print "    $funcName: function()\n    {\n    prompt(twiki(\n";
    foreach (@_) {
        s/\n/\\n/g;
        s/\"/\\\"/g;
        print "\"$_\"+\n";
    }
    print "\"\"));\n    },\n";
}

$testprefix="QTBATMA_";
$instep = 0;
$indescr = 0;
$insubsection = 0;
$testcounter = 0;
@testlines = ();
@lines = <STDIN>;

print "testcase = {\n\n";

foreach (@lines)  {
    chomp;
    if ($insubsection) {
        push @subsectionlines, @templines;
    } else {
        push @testlines, @templines;
    }
    @templines = ();
    s/(``|'')/\"/g;
    s/\\url{(.*)}/%BLUE%$1%ENDCOLOR%/g;
    s/\t/   /g;

    if (/\\subsection{(.*)}/) {
        $insubsection = 1;
        @subsectionlines = ();
        push @templines, "---++ $1\n";
        next;
    }
    if (/\\subsubsection{(.*)}/) {
        if (defined $testname) {
            printTestFunc($testname, @testlines);
            @testlines = ();
        }
        if ($insubsection) {
            $insubsection = 0;
            push @templines, @subsectionlines;
            push @templines, "-------\n";
        }
        $testcounter++;
        if ($testcounter < 10) {
            $tcs = "0$testcounter";
        } else {
            $tcs = "$testcounter";
        }
        $testname = "$testprefix$tcs";
        push @templines, "---+++ $testname: $1\n";
        next;
    }
    if (/descr{(.*)}\s*(.*)/) {
        push @templines, "---++++ $1\n";
        if ($2 ne "") {
            push @templines, "$2\n";
        }
        $indescr = 1;
        next;
    }
    if (/\\begin{(?:short)?stepstable}/) {
        $step = 1;
        push @templines, "---++++ Steps:\n";
        push @templines, "| *No.* | *Description* | *Expected Result* |";
        next;
    }
    if (/\\end{(?:short)?stepstable}/) {
        $instep = 0;
        push @templines, "\n";
        next;
    }
    if (/\\step\s*(.*)/) {
        $instep = 1;
        $x = $1;
        $x =~ s/\s*(&|\\\\)\s*/ | /g;
        push @templines, "\n| $step $x";
        $step++;
        next;
    }
    if (/^\s*\\hline/ || /^\s*\\footnote/ || /^\s*\\begin/ || /^\s*\\end/) {
        next;
    }
    if ($instep) {
        s/\\item/%BB%/;
    } else {
        s/\\item/\   */;
    }
    if ($indescr) {
        push @templines, "$_\n";
        if (/^\s*$/) {
            $indescr=0;
        }
        next;
    }
    if ($instep) {
        $x = $_;
        $x =~ s/\s*(&|\\\\)\s*/ | /g;
        push @templines, "$x";
        next;
    }
}

printTestFunc($testname, @testlines);
print "\n}\n";
