#RESOURCES += scripts.qrc
DEFINES += QTUITESTRUNNER_TARGET
TEMPLATE=app
VPATH+=$$PWD
QT+=script network
CONFIG+=qtestlib
TARGET=qtuitestrunner

QTUITEST_SRC=$$PWD

INCLUDEPATH+=$$PWD
INCLUDEPATH+=$$QTUITEST_SRC
INCLUDEPATH+=$$QTUITEST_SRC/libqtuitest
INCLUDEPATH+=$$QTUITEST_SRC/libqsystemtest
INCLUDEPATH+=$$QTUITEST_SRC/botan/build
INCLUDEPATH+=$$QTUITEST_SRC/coreplugin

BOTAN_SRC=$$QTUITEST_SRC/botan/src
include($$BOTAN_SRC/botan.pri)

HEADERS +=\
    $$QTUITEST_SRC/libqtuitest/qtestprotocol_p.h\
    $$QTUITEST_SRC/libqtuitest/qtuitestelapsedtimer_p.h


SOURCES +=\
    $$QTUITEST_SRC/libqtuitest/qtestprotocol.cpp\
    $$QTUITEST_SRC/libqtuitest/qtuitestelapsedtimer.cpp\
    $$QTUITEST_SRC/interpreter/main.cpp

include(qtuitest-host.pri)

HEADERS +=\
        $$QTUITEST_SRC/coreplugin/ssh/sftpchannel.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftpchannel_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftpdefs.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftpincomingpacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftpoperation_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftpoutgoingpacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sftppacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshbotanconversions_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshcapabilities_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshchannel_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshchannelmanager_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshconnection.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshconnection_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshcryptofacility_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshdelayedsignal_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/ssherrors.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshexception_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshincomingpacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshkeyexchange_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshkeygenerator.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshoutgoingpacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshpacket_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshpacketparser_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshremoteprocess.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshremoteprocess_p.h \
        $$QTUITEST_SRC/coreplugin/ssh/sshsendfacility_p.h

SOURCES +=\
        $$QTUITEST_SRC/coreplugin/ssh/sftpchannel.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sftpdefs.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sftpincomingpacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sftpoperation.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sftpoutgoingpacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sftppacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshcapabilities.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshchannel.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshchannelmanager.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshconnection.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshcryptofacility.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshdelayedsignal.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshincomingpacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshkeyexchange.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshkeygenerator.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshoutgoingpacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshpacket.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshpacketparser.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshremoteprocess.cpp \
        $$QTUITEST_SRC/coreplugin/ssh/sshsendfacility.cpp

SOURCES += $$QTUITEST_SRC/libqsystemtest/qtestide.cpp

!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
#    DESTDIR=$$HOME/bin
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
}

win32 {
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
    !equals(QMAKE_CXX, "g++") {
        DEFINES+=strcasecmp=_stricmp
    }
}

mac {
    CONFIG-=app_bundle
}
