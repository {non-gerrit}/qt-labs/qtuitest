TEMPLATE = lib
TARGET = Core
DESTDIR = $$BUILDROOT/lib
target.path=$$[QT_INSTALL_LIBS]
INSTALLS+=target

QT += network
INCLUDEPATH += .. ../botan/build
LIBS+=-L$$BUILDROOT/lib -lBotan
DEFINES+=CORE_LIBRARY

HEADERS +=\
        ssh/sftpchannel.h \
        ssh/sftpchannel_p.h \
        ssh/sftpdefs.h \
        ssh/sftpincomingpacket_p.h \
        ssh/sftpoperation_p.h \
        ssh/sftpoutgoingpacket_p.h \
        ssh/sftppacket_p.h \
        ssh/sshbotanconversions_p.h \
        ssh/sshcapabilities_p.h \
        ssh/sshchannel_p.h \
        ssh/sshchannelmanager_p.h \
        ssh/sshconnection.h \
        ssh/sshconnection_p.h \
        ssh/sshcryptofacility_p.h \
        ssh/sshdelayedsignal_p.h \
        ssh/ssherrors.h \
        ssh/sshexception_p.h \
        ssh/sshincomingpacket_p.h \
        ssh/sshkeyexchange_p.h \
        ssh/sshkeygenerator.h \
        ssh/sshoutgoingpacket_p.h \
        ssh/sshpacket_p.h \
        ssh/sshpacketparser_p.h \
        ssh/sshremoteprocess.h \
        ssh/sshremoteprocess_p.h \
        ssh/sshsendfacility_p.h

SOURCES +=\
        ssh/sftpchannel.cpp \
        ssh/sftpdefs.cpp \
        ssh/sftpincomingpacket.cpp \
        ssh/sftpoperation.cpp \
        ssh/sftpoutgoingpacket.cpp \
        ssh/sftppacket.cpp \
        ssh/sshcapabilities.cpp \
        ssh/sshchannel.cpp \
        ssh/sshchannelmanager.cpp \
        ssh/sshconnection.cpp \
        ssh/sshcryptofacility.cpp \
        ssh/sshdelayedsignal.cpp \
        ssh/sshincomingpacket.cpp \
        ssh/sshkeyexchange.cpp \
        ssh/sshkeygenerator.cpp \
        ssh/sshoutgoingpacket.cpp \
        ssh/sshpacket.cpp \
        ssh/sshpacketparser.cpp \
        ssh/sshremoteprocess.cpp \
        ssh/sshsendfacility.cpp
