QTUITEST=../.. qdoc3 qtuitest.qdocconf
THISYEAR=`date +%Y`
for f in $( ls ../html/*.html ); do
    sed s/%THISYEAR%/$THISYEAR/g ../html/$f > ../html/$f.temp
    mv ../html/$f.temp ../html/$f
done 
