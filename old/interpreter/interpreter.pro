SOURCES +=\
        main.cpp                \
        qscriptsystemtest.cpp   \
        qtscript_bindings.cpp   \
        qtuitestengineagent.cpp \
        scriptpreprocessor.cpp

HEADERS +=\
        qscriptsystemtest.h   \
        scriptpreprocessor.h  \
        qtuitestengineagent.h \
        qtscript_bindings.h

RESOURCES += scripts.qrc
DEFINES += QTUITESTRUNNER_TARGET
TEMPLATE=app
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
INCLUDEPATH+=$$SRCROOT
INCLUDEPATH+=$$SRCROOT/libqsystemtest
QT+=script network
CONFIG+=qtestlib
TARGET=qtuitestrunner

!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
    DESTDIR=$$BUILDROOT/bin
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
}

#symbian {
#    TARGET.EPOCALLOWDLLDATA=1
#    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData
#    MOC_DIR=$$OUT_PWD/moc
#    OBJECTS_DIR=$$OUT_PWD/obj
#    LIBS += -L$$OUT_PWD -lqsystemtest
#}

win32 {
    CONFIG(debug,debug|release):  LIBS+=-L$$BUILDROOT/libqsystemtest/debug -lqsystemtestd 
    CONFIG(release,debug|release):LIBS+=-L$$BUILDROOT/libqsystemtest/release -lqsystemtest
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
    !equals(QMAKE_CXX, "g++") {
        DEFINES+=strcasecmp=_stricmp
    }
}

!win32:!symbian:!mac {
    LIBS += -L$$BUILDROOT/lib -lqsystemtest -lBotan -lCore
}

mac {
    CONFIG-=app_bundle
    CONFIG(debug,debug|release): LIBS += -L$$BUILDROOT/lib -lqsystemtest_debug
    CONFIG(release,debug|release): LIBS += -L$$BUILDROOT/lib -lqsystemtest
}

