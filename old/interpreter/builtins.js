/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*
    "Builtin" functions available to all tests.
    This file gets evaluated once at the beginning of every test.

    Documentation for these functions should be placed in qsystemtest.cpp.

    Note, if a function exists with the same name in both here and QSystemTest,
    the QtScript version takes precedence.  To force a call to the QSystemTest
    function, use ParentTestObject[signature], e.g. see expectMessageBox.

    Note that the exported function needs to be a public slot, AND have at least one
    parameter. See: runAsManualTest(void)
*/

/* called before every init() */
function init_global()
{
}

/* called after every cleanup() */
function cleanup_global()
{
}

/* called before every initTestCase() */
function initTestCase_global()
{
}

/* called after every cleanupTestCase() */
function cleanupTestCase_global()
{
    killApplication();
}

function verify() {
    if (arguments.length == 0) {
        fail( "You need to specify at least a boolean expression" );
        return false;
    }
    var msg = "";
    if (arguments.length > 1) { msg = arguments[1] };

    if (ParentTestObject['runAsManualTest(void)']()) {
        var description = "verify that MAGIC_DATA";
        if (msg.length > 0) description = "MAGIC_COMMAND:" + msg;
        ParentTestObject['manualTest(QString)'](description);
        return true;
    }

    return ParentTestObject['verify(bool,QString)'](arguments[0],msg);
}

function print() {
    var str = "";
    for (var i = 0; i < arguments.length; ++i) {
        if (arguments[i] != undefined) str = str + arguments[i];
    }
    ParentTestObject['print(QVariant)'](str);
}

/* Expect conditionFunction to return true after executing code, or fail with message */
function _expect(conditionFunction, message) {
    if (message == undefined) {
        message = "Expected condition did not become true";
    }
    this.__defineSetter__("code", function(code){
        try {
            code.apply(this);
        } catch (e) {
            if (this.onFail != undefined) {
                this.onFail.apply(this);
            }
            throw e;
        }
        if (!conditionFunction.call()) {
            fail(message);
        }
    });
}

function expect(a,b) {
    return new _expect(a,b);
}

function select( item, queryPath ) {
    if (item != undefined) {
        if (queryPath == undefined) queryPath = "";
        ParentTestObject['select(QString,QString)'](item, queryPath);
    }
    return true;
}

function selectIndex( idx, queryPath ) {
    if (idx instanceof Array) {
        ParentTestObject['selectIndex(QVariantList,QString)'](idx, queryPath);
    } else {
        ParentTestObject['selectIndex(QVariantList,QString)']([idx, 0], queryPath);
    }
}

function skip(text, mode) {
    ParentTestObject['skip(QString,QSystemTest::SkipMode)'](text, mode);
}

/*
    Wait for code to return true for up to timeout ms, with given number of intervals,
    and message on failure
*/
function _waitFor(timeout, intervals, message) {
    if (timeout == undefined) {
        timeout = 10000;
    }
    if (intervals == undefined) {
        intervals = 20;
    }
    if (message == undefined) {
        message = "Waited-for condition did not become true after " + timeout + " milliseconds";
    }
    this.__defineSetter__("code", function(code){
        ok = code.apply(this);
        i = 0;
        while (!ok && i < intervals) {
            wait(timeout/intervals);
            ok = code.apply(this);
            i = i + 1;
        }
        if (!ok) {
            fail(message);
        }
    });
}
function waitFor(a,b,c) {
    return new _waitFor(a,b,c);
}

Array.prototype.isEmpty = function()
{
    return (this.length <= 0);
}

Array.prototype.contains = function()
{
    obj = arguments[0];

    if (ParentTestObject['runAsManualTest(void)']()) {
        var description = " contains " + obj;
        ParentTestObject['manualTestData(QString)'](description);
        return true;
    }

    for (var i=0; i<this.length; i++) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}

// Ensure we don't get the 'contains' function when using for-in on arrays
setFlags(Array.prototype, "contains", QScriptValue.SkipInEnumeration);

Array.prototype.containsOne = function()
{
    obj = arguments[0];
    var count = 0;
    for (var i=0; i<this.length; i++) {
        if (this[i] == obj) {
            count++;
        }
    }
    return count == 1;
}
// Ensure we don't get the 'containsOne' function when using for-in on arrays
setFlags(Array.prototype, "containsOne", QScriptValue.SkipInEnumeration);

String.prototype.contains = function()
{
    var obj = arguments[0];
    if (arguments[1] != undefined) {
        obj = arguments[1];
    }

    if (obj instanceof Date) {
        var strs = [];
        strs.push(obj.toString("ddd, M/d/yy"));
        strs.push(obj.toString("dddd, MMMM d, yyyy"));
        strs.push(obj.toString("ddd, d MMM"));
        strs.push(obj.toString("dd/MM/yyyy"));

        if (ParentTestObject['runAsManualTest(void)']()) {
            var description = " contains date " + strs[i];
            ParentTestObject['manualTestData(QString)'](description);
            return true;
        }

        for (var i = 0; i < strs.length; ++i) {
            if (this.toString().contains(strs[i])) return true;
        }
        return false;
    } else if (obj instanceof RegExp) {
        if (ParentTestObject['runAsManualTest(void)']()) {
            var description = " contains " + obj;
            ParentTestObject['manualTestData(QString)'](description);
            return true;
        }

        return obj.test(this.toString());
    } else {
        if (ParentTestObject['runAsManualTest(void)']()) {
            var description = " contains " + obj;
            ParentTestObject['manualTestData(QString)'](description);
            return true;
        }

        var ref = this.toString();
        return (-1 != ref.indexOf(obj));
    }
}

String.prototype.toDate = function()
{
    var obj = this.toString();
    return toDate(obj);
}

String.prototype.startsWith = function()
{
    var obj = arguments[0];
    var ref = this.toString();
    return ref.indexOf( obj ) == 0;
}

String.prototype.left = function()
{
    var start = 0;
    var end = arguments[0];
    var ref = this.toString();
    return ref.slice( start, end );
}

String.prototype.right = function()
{
    var ref = this.toString();
    var start = ref.length - arguments[0];
    return ref.slice( start );
}

String.prototype.mid = function()
{
    var start = arguments[0];
    var end = arguments[1]+1;
    var ref = this.toString();
    return ref.slice( start, end );
}

String.prototype.toValue = function()
{
    var ret = 0;
    var ref = this.toString();
    for (var i=0; i<ref.length; i++) {
        var ch = ref.charAt(i);
        if (ch == '-') {
            ret = ret * -1;
        } else {
            ret = ret * 10;
            if (ch == '0') {
            } else if (ch == '1') {
                ret = ret + 1;
            } else if (ch == '2') {
                ret = ret + 2;
            } else if (ch == '3') {
                ret = ret + 3;
            } else if (ch == '4') {
                ret = ret + 4;
            } else if (ch == '5') {
                ret = ret + 5;
            } else if (ch == '6') {
                ret = ret + 6;
            } else if (ch == '7') {
                ret = ret + 7;
            } else if (ch == '8') {
                ret = ret + 8;
            } else if (ch == '9') {
                ret = ret + 9;
            } else {
                fail( "ERROR: Couldn't convert '" + ref + "' into a value." );
                return undefined;
            }
        }
    }
    return ret;
}

function anyDefined()
{
    for (var i = 0; i < arguments.length; ++i) {
        if (arguments[i] != undefined) return true;
    }
    return false;
}

function tabBar(index)
{
    if (index == undefined) {
        index = 0;
    }

    if (ParentTestObject['runAsManualTest(void)']()) {
        if (index == 0) {
            // In 99% of the cases there will be only one tabbar in an app
            // so we can get something like:
            // select 'foobar' from 'tabBar'
            return "tabBar";
        } else {
            // In the remaining 1% we get something like:
            // select 'foobar' from '2nd tabBar'
            return (index+1) + "nd tabBar";
        }
    }

    var tabBars = findByProperty( { inherits: "QTabBar" } );
    if (tabBars[index] == undefined) {
        fail( "ERROR: Tab widget not found." );
        return undefined;
    }
    return tabBars[index];
}

function menuBar(index)
{
    if (index == undefined) {
        index = 0;
    }

    if (ParentTestObject['runAsManualTest(void)']()) {
        if (index == 0) {
            // In 99% of the cases there will be only one menubar in an app
            // so we can get something like:
            // select 'foobar' from 'menuBar'
            return "menuBar";
        } else {
            // In the remaining 1% we get something like:
            // select 'foobar' from '2nd menuBar'
            return (index+1) + "nd menuBar";
        }
    }


    var menuBars = findByProperty( { inherits: "QMenuBar" } );
    if (menuBars[index] == undefined) {
        fail( "ERROR: MenuBar widget not found." );
        return undefined;
    }
    return menuBars[index];
}

// Convert twiki markup to HTML for display in prompts
function twiki(text) {
    var lines = text.split("\n");
    var result = [];
    var listType = [];
    var currLevel = 0;
    var rowNumber = 0;
    var inListItem = false;
    var inTable = false;
    var inVerbatim = false;
    var liRegExp = /((?:   )+)(\*|(?:[1AaIi]).)\s*(.*)/;
    var liContRegExp = /(?:   )\s*(.*)/;
    var tableRegExp = /\s*\|(\s*)(.*)(\s*)\|\s*$/;

    for (var i=0; i < lines.length; i++) {
        var line = lines[i];

        // Verbatim
        if (inVerbatim) {
            if (line.match(/\<\s*\/\s*verbatim\s*\>/)) {
                inVerbatim = false;
                result.push("</pre>");
            } else {
                result.push(line);
            }
            continue;
        } else if (line.match(/\<\s*verbatim\s*\>/)) {
            inVerbatim = true;
            result.push("<pre>");
            continue;
        }

        // Simple replacements
        // Headings
        line = line.replace(/---\+(?:!!)? +(.*)/, "<h1>$1</h1>");
        line = line.replace(/---\+\+(?:!!)? +(.*)/, "<h2>$1</h2>");
        line = line.replace(/---\+\+\+(?:!!)? +(.*)/, "<h3>$1</h3>");
        line = line.replace(/---\+\+\+\+(?:!!)? +(.*)/, "<h4>$1</h4>");
        line = line.replace(/---\+\+\+\+\+(?:!!)? +(.*)/, "<h5>$1</h5>");
        line = line.replace(/---\+\+\+\+\+\+(?:!!)? +(.*)/, "<h6>$1</h6>");

        // Misc
        line = line.replace(/---[-]*/g, "<hr />");
        line = line.replace(/^$/, "<p />");
        line = line.replace(/!([A-Z]\w*)/g, "$1");
        line = line.replace(/%TOC%/g, "");
        line = line.replace(/%BB%/g, "<br />&#8226;");

        // Links and anchors
        line = line.replace(/^\#([A-Z]\w*)/, "<a name=\"$1\"></a><p />");
        line = line.replace(/\[\[\#([A-Z]\w*)\]\[(.*)\]\]/g, "<a href=\"#$1\">$2</a>");
        line = line.replace(/\[\[\#([A-Z]\w*)\]\]/g, "<a href=\"#$1\">#$1</a>");

//        line = line.replace(/\[\[(.*?)\]\[(.*?)\]\]/g, "<a href=\"$1\">$2</a>");
//        line = line.replace(/\[\[(.*?)\]\]/g, "<a href=\"$1\">$1</a>");
        line = line.replace(/\[\[(.*?)\]\[(.*?)\]\]/g, "$2");
        line = line.replace(/\[\[(.*?)\]\]/g, "$1");

        // Text style
        line = line.replace(/^\*(\S.*?\S)\*(?!\w)/, "<b>$1</b>");
        line = line.replace(/([^\w])\*(\S.*?\S)\*(?!\w)/g, "$1<b>$2</b>");
        line = line.replace(/^__(.*?\S)__(?!\w)/g, "<b><i>$2</i></b>");
        line = line.replace(/([^\w])__(.*?\S)__(?!\w)/g, "$1<b><i>$2</i></b>");
        line = line.replace(/^_([^_]*?\S)_(?!\w)/, "<i>$1</i>");
        line = line.replace(/([^\w])_([^_]*?\S)_(?!\w)/g, "$1<i>$2</i>");
        line = line.replace(/^==(.*?\S)==(?!\w)/g, "<b><code>$1</code></b>");
        line = line.replace(/([^\w])==(.*?\S)==(?!\w)/g, "$1<b><code>$2</code></b>");
        line = line.replace(/^=(.*?\S)=(?!\w)/, "<code>$1</code>");
        line = line.replace(/([^\w])=(.*?\S)=(?!\w)/g, "$1<code>$2</code>");
        line = line.replace(/%(.*)%(.*)%ENDCOLOR%/g, "<font color=\"$1\">$2<\/font>");

        // Tables
        tableMatch = tableRegExp.exec(line);
        if (tableMatch) {
            if (inTable) {
                rowNumber++;
                if (rowNumber%2) {
                    result.push("</tr>\n<tr bgcolor=\"#f2f2f2\">");
                } else {
                    result.push("</tr>\n<tr bgcolor=\"#e0e0e0\">");
                }
            } else {
                inTable = true;
                rowNumber = 0;
                result.push("<table>\n<tr bgcolor=\"#e0e0e0\">");
            }

            var cellRegExp = /\|(\s*)(\<b\>)?(.*?)(\<\/b\>)?(\s*)(?=\|)/g;
            var trLine = "";
            while (cell = cellRegExp.exec(line)) {
                var left = cell[1].length;
                var right = cell[5].length;
                var align = "left";
                if (left > 1 && left == right) {
                    align = "center";
                } else if (left > right) {
                    align = "right";
                }
                if (cell[2] && cell[4]) {
                    trLine += "<th bgcolor=\"#404040\"><font color=\"white\">" + cell[3] + "</font></th>";
                } else {
                    trLine += "<td align=\"" + align + "\">" + cell[3] + "</td>";
                }
            }
            line = trLine;
            result.push(line);
            continue;
        } else {
            if (inTable) {
                result.push("</tr></table>");
                inTable = false;
            }
        }

        // List Items
        liMatch = liRegExp.exec(line);
        level = (liMatch ? liMatch[1].length/3 : 0);
        liType = (level ? (liMatch[2] == "*" ? "ul" : "ol") : null);

        // List item continuation
        if (inListItem) {
            if (!liMatch) {
                liContMatch = liContRegExp.exec(line);
                if (!liContMatch) {
                    inListItem = false;
                    result.push("</li>");
                } else {
                    line = liContMatch[1];
                    level = currLevel;
                }
            } else {
                inListItem = false;
                result.push("</li>");
            }
        }

        // Change of list indentation level
        if (level > currLevel) {
            for (var j = currLevel; j < level; j++) {
                result.push("<" + liType + (liType == "ol" ? " type=\"" + liMatch[2][0] + "\"" : "") + ">");
                listType.push(liType);
            }
        } else {
            for (var j = level; j < currLevel; j++) {
                prevLiType = listType.pop();
                result.push("</" + prevLiType + ">");
            }
            if (liMatch && listType.length && listType[listType.length - 1] != liType) {
                // Change of list item type (from unordered to ordered, or vice-versa)
                result.push("</" + listType.pop() + ">");
                result.push("<" + liType + (liType == "ol" ? " type=\"" + liMatch[2][0] + "\"" : "") + ">");
                listType.push(liType);
            }
        }
        currLevel = level;

        if (liMatch) {
            line = line.replace(liRegExp, "<li" + (liType == "ol" ? " type=\"" + liMatch[2][0] + "\"" : "") + ">" + liMatch[3]);
            inListItem = true;
        }

        result.push(line);
    }

    return(result.join("\n"));
}

// Support object-oriented syntax for widget interaction
String.prototype.focusWidget = function() { return focusWidget(this); }
String.prototype.getSelectedText = function() { return getSelectedText(this); }
String.prototype.getText = function() { return getText(this); }
String.prototype.getSelectedValue = function() { return getSelectedValue(this); }
String.prototype.getValue = function() { return getValue(this); }
String.prototype.getList = function() { return getList(this); }
String.prototype.getLabels = function() { return getLabels(this); }
String.prototype.currentTitle = function() { return currentTitle(this); }
String.prototype.getGeometry = function() { return getGeometry(this); }
String.prototype.setProperty = function(name, value) { setProperty(this, name, value); }
String.prototype.getProperty = function(name) { return getProperty(this, name); }
String.prototype.keyPress = function(key) { keyPress(key, this); }
String.prototype.keyRelease = function(key) { keyRelease(key, this); }
String.prototype.keyClick = function(key) { keyClick(key, this); }
String.prototype.keyClickHold = function(key, duration) { keyClickHold(key, duration, this); }
String.prototype.mouseClick = function() { mouseClick(this); }
String.prototype.mouseClickHold = function(duration) { mouseClickHold(this, duration); }
String.prototype.mousePress = function() { mousePress(this); }
String.prototype.mouseRelease = function() { mouseRelease(this); }
String.prototype.enter = function(value, mode) { enter(value, this, mode) }
String.prototype.select = function(item) { select(item, this); }
String.prototype.selectIndex = function(idx) { selectIndex(idx, this); }
String.prototype.activate = function() { activate(this); }
String.prototype.isVisible = function() { return isVisible(this); }
String.prototype.isEnabled = function() { return isEnabled(this); }
String.prototype.isChecked = function() { return isChecked(this); }
String.prototype.setChecked = function(doCheck) { setChecked(doCheck, this); }
String.prototype.checkState = function() { return checkState(this); }
String.prototype.setCheckState = function(state) { setCheckState(state, this); }
String.prototype.saveScreen = function(name) { saveScreen(name, this); }
