/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QSCRIPTSYSTEMTEST_H
#define QSCRIPTSYSTEMTEST_H

#include <qsystemtest.h>
#include <QScriptEngine>
#include <QScriptEngineAgent>
#include <QVariantMap>

class QTestMessage;

class QTUITESTRUNNER_EXPORT QScriptSystemTest
    : public QSystemTest
{
Q_OBJECT
public:
    QScriptSystemTest();
    virtual ~QScriptSystemTest();

    static QString loadInternalScript(QString const &name, QScriptEngine *engine, bool withParentObject = false);
    static void loadBuiltins(QScriptEngine *engine);
    static void importIntoGlobalNamespace(QScriptEngine*, QString const&);

    virtual QString testCaseName() const;
    void scriptPositionChange(qint64, int, int);
    void scriptContextChange(bool);

    virtual int runTest(const QString &fname, const QStringList &parameters,
                        const QStringList &environment);

public slots:
    virtual bool fail(QString const &message);
    virtual void expectFail( const QString &reason );
    virtual void skip(QString const &message, QSystemTest::SkipMode mode = QSystemTest::SkipSingle);
    virtual void verify(bool statement, QString const &message = QString());
    virtual void compare(const QString &actual, const QString &expected);
    virtual void compare(const QStringList &actual, const QStringList &expected);

    QVariantMap sendRaw(const QString&, const QScriptValue& = QScriptValue());
    void installMessageHandler(const QScriptValue&);

    void dumpEngine();

protected:
    virtual void initEngine(bool);
    virtual QString currentFile();
    virtual int currentLine();
    virtual void outputBacktrace();

    virtual bool setQueryError( const QTestMessage &message );
    virtual bool setQueryError( const QString &errString );

    virtual void printUsage() const;
    virtual void processCommandLine(QStringList&);    

    virtual bool isFailExpected();

    virtual void processMessage(const QTestMessage& message);

private:   
    QString filename;
    QScriptEngine *m_engine;
    QScriptEngineAgent *m_agent;
    int m_contextDepth;
    QList<QScriptValue> m_messageHandlers;
    QObject* testObject;
    QString expect_fail_function;
    QString expect_fail_datatag;
    QString expect_fail_reason;
    bool m_checkOnly;
    QStringList m_builtins;
};

#endif
