SRCDIR=$$replace(OUT_PWD, $$BUILDROOT, $$SRCROOT)
BUILDDIR=$$OUT_PWD

CONFIG += qt qtestlib console
INCLUDEPATH += $$SRCROOT/libqtuitest
INCLUDEPATH += $$SRCROOT/tests
!mac:!win32:LIBS += -L$$BUILDROOT/lib -lqtuitest
mac: {
    CONFIG(debug,debug|release): LIBS += -L$$BUILDROOT/lib -lqtuitest_debug
    CONFIG(release,debug|release): LIBS += -L$$BUILDROOT/lib -lqtuitest
    CONFIG -= app_bundle
}

LIBPATHVAR=LD_LIBRARY_PATH
mac:LIBPATHVAR=DYLD_LIBRARY_PATH

X11_SETUP=
X11_TEARDOWN=
unix:!mac:contains(QT,gui) {
    X11_SETUP=                                                                          \
        if ! which Xephyr > /dev/null 2>&1; then                                        \
            echo Xephyr is not available on this machine, will not run X11 tests. 1>&2; \
            exit 1;                                                                     \
        fi;                                                                             \
        XLOG=\$$(mktemp /tmp/maketest_x_log.XXXXXX);                                    \
        trap \'rm -f \$$XLOG\' EXIT;                                                    \
        Xephyr :322 -screen 1024x900 > \$$XLOG 2>&1 &                                   \
        export X11_PID=\$$!;                                                            \
        export DISPLAY=:322;                                                            \
        sleep 2;                                                                        \
        if ! ps | egrep -q \"^ *\$${X11_PID} \"; then                                   \
            echo It looks like Xephyr failed to run correctly, log follows. 1>&2;       \
            cat \$$XLOG 1>&2;                                                           \
            exit 1;                                                                     \
        fi;
    X11_TEARDOWN=kill \$${X11_PID} >/dev/null 2>&1;
}

test.depends = $(QMAKE_TARGET)
test.path = $$BUILDDIR

unix {
    test.commands = @\
        $$X11_SETUP \
        $$LIBPATHVAR=$$BUILDROOT/lib $$BUILDDIR/$(QMAKE_TARGET) $(ARGS);\
        ret=\$$?;\
        $$X11_TEARDOWN \
        exit \$$ret
}

win32 {
    CONFIG(debug,debug|release):  LIBS+=-L$$BUILDROOT/libqtuitest -lqtuitestd
    CONFIG(release,debug|release):LIBS+=-L$$BUILDROOT/libqtuitest -lqtuitest
    test.commands = @$$BUILDDIR/$$TARGET $(ARGS)
}

debug_and_release {
    debug:  test.depends += debug
    release:test.depends += release
}

QMAKE_EXTRA_TARGETS += test

unix {
    TESTRESULTS=$$OUT_PWD/testresults
    XSL=$$SRCROOT/scripts/qtestxml2junitxml.xsl

    junittest.depends = $(QMAKE_TARGET)
    junittest.path = $$BUILDDIR
    junittest.commands = @                                                                          \
        if ! which xsltproc 2>/dev/null 1>&2; then echo "xsltproc not found, aborting"; exit 1; fi; \
        set -e;                                                                                     \
        rm -rf $$TESTRESULTS;                                                                       \
        mkdir -p $$TESTRESULTS;                                                                     \
        make -s -j1 test ARGS=\"$(ARGS) -xml > $$TESTRESULTS/testlib.xml\";                         \
        csplit -s -z -f $$TESTRESULTS/testlibsplit $$TESTRESULTS/testlib.xml \'/<?xml/\' \'{*}\';   \
        rm $$TESTRESULTS/testlib.xml;                                                               \
        i=0;                                                                                        \
        for file in $$TESTRESULTS/testlibsplit*; do                                                 \
            xsltproc $$XSL \$${file} > $$TESTRESULTS/junit\$${i}.xml;                               \
            rm \$${file};                                                                           \
            i=\$$(expr \$$i + 1);                                                                   \
        done;                                                                                       \
        echo "JUnit-compatible test results written to $$TESTRESULTS"


    QMAKE_EXTRA_TARGETS += junittest
}

