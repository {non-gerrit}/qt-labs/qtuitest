TEMPLATE=lib
VPATH+=$$PWD
TARGET=qtslave
TARGET=$$qtLibraryTarget($$TARGET)
QT+=network
DEFINES+=QTSLAVE_TARGET
INCLUDEPATH+=$$PWD

INCLUDEPATH += $$SRCROOT/libqtuitest

include($$SRCROOT/libqtuitest/libqtuitest.pri)

# we need to set a MAEMO define since this is not yet in Qt
maemo*{
     DEFINES+=Q_OS_MAEMO
}

unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
    DESTDIR=$$BUILDROOT/lib
    target.path=$$[QT_INSTALL_LIBS]
    INSTALLS+=target
}

linux-g++-maemo*:{
   message(Maemo platform detected enabling define Q_OS_MAEMO)
   DEFINES += Q_OS_MAEMO
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData NetworkServices
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
    LIBS+=-lqtuitest.lib -lws32 -leuser -lcone
    CONFIG+=staticlib
}

win32 {
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
}

HEADERS += \
        qtestslave.h \
        qtestslaveglobal.h \
        qtestwidgets.h

SOURCES += \
        qtestslave.cpp \
        qtestwidgets.cpp

maemo5|maemo6 {
    target.path = $$[QT_INSTALL_LIBS]
    INSTALLS += target
}
