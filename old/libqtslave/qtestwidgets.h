/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTESTWIDGETS_H
#define QTESTWIDGETS_H

#include <QHash>
#include <QMetaType>
#include <QObject>
#include <QPointer>
#include <QString>
#include <QStringList>
#include <QVariantMap>
#include <QWidget>

#include <qtuitestnamespace.h>
#include <qtuitestglobal.h>

// Max timeout before we expect a visible clue on the device that the app is responding.
#define VISIBLE_RESPONSE_TIME 5000

#define USE_ADDRESS_FOR_SIGNATURE
class QTestWidget;

class QTSLAVE_EXPORT QActiveTestWidgetData : public QObject
{
public:
    QActiveTestWidgetData();
    ~QActiveTestWidgetData();

    void clear();
    void removeTestWidget( QObject* );
    void sort();
    void resolveLabels();
    bool scan( QObject* aw );
    void scanWindows();
    QStringList allLabels();
    QStringList getWindowTitles();
    QWidget *findWindow(const QString&);
    QObject* findWidgetByLabel( const QString &text, QString &error );
    QObjectList findWidgetsByLabel( const QString &text );
    bool findWidget( const QString &labelOrSignature, QObject *&widget, QString &error, int offset );

    QHash<QString, QObject*> testWidgetsBySignature;
    QHash< QPointer<QObject>, QPointer<QObject> > resolved_buddy_pairs;
    QObjectList visible_tw_buddies;
    QObjectList unresolved_tw_buddies;
    QObjectList visible_tw_labels;
    QObjectList unresolved_tw_labels;
    QObjectList app_windows;
};

class QTSLAVE_EXPORT QActiveTestWidget : public QObject
{
    Q_OBJECT
public:
    static QActiveTestWidget* instance();

    void clear();
    bool rescan( QString &error, int timeout = VISIBLE_RESPONSE_TIME );
    QString toString();
    QStringList allLabels();
    QStringList getWindowTitles();
    QWidget *findWindow(const QString&);
    bool findWidget( const QString &labelOrSignature, QObject *&widget, QString &error, int offset = 0 );
    QObjectList findObjectsByProperty( const QString&, const QVariant&, const QObjectList& = QObjectList() );
    QObjectList findObjectsByProperty( const QVariantMap& );
    QString friendlyName(QObject*);
    bool isList();
    QObject* testWidget( const QString &signature );

    void registerTestWidget( QObject *ao );

    static const QString NoActiveWidgetError;

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    bool findWidget_impl( const QString &labelOrSignature, QObject *&widget, QString &error, int offset );

protected slots:
    void onDestroyed(QObject*);

private:
    QActiveTestWidget();
    ~QActiveTestWidget();

    QPointer<QObject> active_widget;
    bool scan_busy;
    int scan_time;

    QActiveTestWidgetData *d;
};

//
// This is the class that manages the test widgets and handles queries about them.
//
class QTSLAVE_EXPORT QTestWidgets
{
public:
    static QObject* activeWidget();
    static QObjectList topLevelObjects();

    static QObject* testWidget( const QString signature );
    static QObject* locateBuddy( const QObject *testlabel, const QObjectList &search_list );
    static QString signature( const QObject* object );

    static bool widgetVisible( QObject *widget );
    static QObject* focusWidget();

    static QStringList objectListToSignatures(const QObjectList&);

    static QString labelText(const QString&);

    static bool lessThan(QObject *a, QObject *b) {
        QtUiTest::Widget *wa = qtuitest_cast<QtUiTest::Widget*>(a);
        QtUiTest::Widget *wb = qtuitest_cast<QtUiTest::Widget*>(b);
        Q_ASSERT(wa && wb);
        if (wa->y() == wb->y())
            return wa->x() < wb->x();
        else
            return wa->y() < wb->y();
    }

private:
    static QString uniqueName( const QObject *item );

};

#endif
