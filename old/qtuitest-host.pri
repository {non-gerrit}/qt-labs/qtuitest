#
#
# This project file is to be used to include (parts of QtUITest) into Creator.
# Basically we include the desktop side of QSystem test + associated classes
# and build them inside the Qt4Test plugin
#
#

DEFINES += QTUITESTRUNNER_TARGET QTUITEST
DEFINES += QSYSTEMTEST_TARGET
QT+=script network
CONFIG+=qtestlib
QTUITEST_VERSION=0.9.0

SOURCES +=\
        $$QTUITEST_SRC/interpreter/qscriptsystemtest.cpp   \
        $$QTUITEST_SRC/interpreter/qtscript_bindings.cpp   \
        $$QTUITEST_SRC/interpreter/qtuitestengineagent.cpp \
        $$QTUITEST_SRC/interpreter/scriptpreprocessor.cpp

HEADERS +=\
        $$QTUITEST_SRC/interpreter/qscriptsystemtest.h   \
        $$QTUITEST_SRC/interpreter/scriptpreprocessor.h  \
        $$QTUITEST_SRC/interpreter/qtuitestengineagent.h \
        $$QTUITEST_SRC/interpreter/qtscript_bindings.h

RESOURCES += $$QTUITEST_SRC/interpreter/scripts.qrc
INCLUDEPATH += $$QTUITEST_SRC/libqsystemtest $$QTUITEST_SRC/interpreter $$QTUITEST_SRC $$QTUITEST_SRC/libqtuitest

# stuff to include libqsystemtest
FORMS   +=\
        $$QTUITEST_SRC/libqsystemtest/manualverificationdlg.ui \
        $$QTUITEST_SRC/libqsystemtest/failuredlg.ui \
        $$QTUITEST_SRC/libqsystemtest/recorddlg.ui

SEMI_PRIVATE_HEADERS += \
        $$QTUITEST_SRC/libqsystemtest/qsystemtestmaster_p.h \
        $$QTUITEST_SRC/libqsystemtest/qtestremote_p.h \
        $$QTUITEST_SRC/libqsystemtest/qtestverifydlg_p.h

HEADERS +=\
        $$QTUITEST_SRC/libqsystemtest/qabstracttest.h \
        $$QTUITEST_SRC/libqsystemtest/qsystemtest.h \
        $$QTUITEST_SRC/libqsystemtest/qtestide.h \
        $$QTUITEST_SRC/libqsystemtest/testcontrol.h \
        $$QTUITEST_SRC/libqsystemtest/maemotestcontrol.h \
        $$QTUITEST_SRC/libqsystemtest/symbiantestcontrol.h \
        $$QTUITEST_SRC/libqsystemtest/desktoptestcontrol.h \
        $$QTUITEST_SRC/libqsystemtest/customtestcontrol.h \
        $$QTUITEST_SRC/libqsystemtest/meegotestcontrol.h

SOURCES +=\
        $$QTUITEST_SRC/libqsystemtest/qabstracttest.cpp \
        $$QTUITEST_SRC/libqsystemtest/qtestremote.cpp \
        $$QTUITEST_SRC/libqsystemtest/qtestverifydlg.cpp \
        $$QTUITEST_SRC/libqsystemtest/qsystemtest.cpp \
        $$QTUITEST_SRC/libqsystemtest/qsystemtest_p.cpp \
        $$QTUITEST_SRC/libqsystemtest/qsystemtestmaster.cpp \
        $$QTUITEST_SRC/libqsystemtest/testcontrol.cpp \
        $$QTUITEST_SRC/libqsystemtest/maemotestcontrol.cpp \
        $$QTUITEST_SRC/libqsystemtest/symbiantestcontrol.cpp \
        $$QTUITEST_SRC/libqsystemtest/desktoptestcontrol.cpp \
        $$QTUITEST_SRC/libqsystemtest/customtestcontrol.cpp \
        $$QTUITEST_SRC/libqsystemtest/meegotestcontrol.cpp

HEADERS*=$$SEMI_PRIVATE_HEADERS $$PRIVATE_HEADERS

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData
    LIBS+=-L$$OUT_PWD -lws32 -leuser -lcone
}

win32 {
    !equals(QMAKE_CXX, "g++") {
        DEFINES+=strcasecmp=_stricmp
    }
}

mac {
    DEFINES+=sighandler_t=sig_t
}
