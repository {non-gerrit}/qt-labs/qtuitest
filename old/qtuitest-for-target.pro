TEMPLATE=subdirs
SUBDIRS+=libqtuitest libqtslave plugins qtbindings
CONFIG+=ordered

!no_tests:SUBDIRS+=tests

include(qtuitest.pri)
