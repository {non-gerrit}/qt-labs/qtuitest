@rem = '--*-Perl-*--
@echo off
if "%OS%" == "Windows_NT" goto WinNT
perl -x -S "%0" %1 %2 %3 %4 %5 %6 %7 %8 %9
goto endofperl
:WinNT
perl -x -S %0 %*
if NOT "%COMSPEC%" == "%SystemRoot%\system32\cmd.exe" goto endofperl
if %errorlevel% == 9009 echo You do not have Perl in your PATH.
if errorlevel 1 goto script_failed_so_exit_with_non_zero_val 2>nul
goto endofperl
@rem ';
#!perl
#line 15
use SOAP::Lite;

$wsdl = $ENV{HTI_WSDL} or die("HTI_WSDL not set");
$htiSystem = SOAP::Lite->service("file:$wsdl/HtiSystem.wsdl");
$htiEcho = SOAP::Lite->service("file:$wsdl/HtiEcho.wsdl");
$htiSysInfo = SOAP::Lite->service("file:$wsdl/HtiSysInfo.wsdl");
$htiApplication = SOAP::Lite->service("file:$wsdl/HtiApplication.wsdl");

$proc = shift @ARGV or die "ERROR: Process not specified";

# Authenticate - doesn't matter what the token is but HTI won't work otherwise
print $htiSystem->authentication("hello");

# HTI connectivity check
print $htiEcho->echo("Connected to device...");

# Start process
#$result = $htiApplication->stopProcessById($proc);
$result = $htiApplication->stopApp($proc);

sleep(2);
__END__
:endofperl
