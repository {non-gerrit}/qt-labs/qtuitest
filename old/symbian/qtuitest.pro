# Use subdirs template to suppress generation of unnecessary files
TEMPLATE = subdirs

symbian: {
    load(data_caging_paths)

    SUBDIRS=
    TARGET = "QtUiTest"

    qtresources.sources = $${EPOCROOT}$$HW_ZDIR$$APP_RESOURCE_DIR/s60main.rsc 
    qtresources.path = $$APP_RESOURCE_DIR

    qtuitest.sources = qtwidgets.dll
    qtuitest.path = /sys/bin

    style_plugins.sources = qtuiteststyle.dll
    style_plugins.path = $$QT_PLUGINS_BASE_DIR/styles
 
    DEPLOYMENT += qtuitest style_plugins
}
