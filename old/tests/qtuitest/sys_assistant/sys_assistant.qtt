/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


testcase = {

    //TODO: In Progress. Not expected to pass (or even work correctly) yet.

    init: function() {
        startApplication("assistant");
        if (getLabels().contains("Updating search index")) {
            print("Waiting for search index update to complete...");
            waitFor(60000) {
                return (!getLabels().contains("Updating search index"));
            }
        }
    },

    check_registered_docs: function() {
        select("Edit/Preferences...", menuBar());
        select("Documentation", tabBar());
        setLabelOrientation(LabelAbove);
        var docs = getText("Registered Documentation:");
        if (!docs.contains("com.trolltech.assistant.") ||
            !docs.contains("com.trolltech.designer.") ||
            !docs.contains("com.trolltech.linguist.") ||
            !docs.contains("com.trolltech.qmake.") ||
            !docs.contains("com.trolltech.qt.")) {
            fail("Documentation has not been registered. Subsequent tests will fail.");
        }
    },

    browse_contents: function() {
        select("Contents", tabBar());
        var content = helpContent();

        select("Qt Linguist Manual/Manual", content);
        waitFor() { return(getText(tabBar(1)).contains("Qt Linguist Manual")); }

        select("Qt Reference Documentation/Overviews/An Introduction to Model\\/View Programming", content);
        waitFor() { return(getText(tabBar(1)).contains("An Introduction to Model/View Programming")); }

        select("Qt Reference Documentation/Classes/QList Class Reference/List of all members", content);
        waitFor() { return(getText(tabBar(1)).contains("List of All Members for QList")); }
    },

    browse_index_data: {
        countryToString: [ "countryToString", "QLocale Class Reference" ]
    },

    browse_index: function(item, pageTitle) {
        select("Index", tabBar());
        var indexWidget = helpIndex();

        select(item, indexWidget);
        select("Open Link", contextMenu());
        waitFor() { return(getText(tabBar(1)).contains(pageTitle)); }
    },

    create_bookmark_data: {
        one: [ "sys_assistant", "test_subfolder" ]
    },

    create_bookmark: function(topLevelFolder, subFolder) {
        select("Bookmarks", tabBar());
        var bookmarkTree = helpBookmarks();

        // If there is already a folder with the same name, remove it
        if (getList(bookmarkTree).contains(topLevelFolder)) {
            select(topLevelFolder, bookmarkTree);
            select("Bookmarks/Remove");
            select("Yes");
        }

        // Create a new bookmark
        select("Bookmarks/Add");
        var pageName = getText("Bookmark:").replace(/\//g, "\\\\/");;
        select("+");
        select("New Folder");
        select("Rename Folder", contextMenu());
        enter(topLevelFolder);
        select("New Folder");
        select("Rename Folder", contextMenu());
        enter(subFolder);
        select("OK");

        // Verify that it is in the list
        var bookmarkList = getList(bookmarkTree);
        verify(bookmarkList.contains(topLevelFolder));
        verify(bookmarkList.contains(topLevelFolder + "/" + subFolder));
        verify(bookmarkList.contains(topLevelFolder + "/" + subFolder + "/" + pageName));

        // Remove it
        select(topLevelFolder, bookmarkTree);
        select("Bookmarks/Remove");
        select("Yes");

        // Verify that it is no longer in the list
        var bookmarkList = getList(bookmarkTree);
        verify(!bookmarkList.contains(topLevelFolder));
        verify(!bookmarkList.contains(topLevelFolder + "/" + subFolder));
        verify(!bookmarkList.contains(topLevelFolder + "/" + subFolder + "/" + pageName));
    },

    search_data: {
        simpleText:      [ "Simple Text", "Simple Text Viewer Example" ],
        designerWidgets: [ "Designer Widgets", "Creating Custom Widgets for Qt Designer" ],
        includePath:     [ "INCLUDEPATH", "qmake Variable Reference" ]
    },

    search: function(query, expectedResult) {
        select("View/Search", menuBar());
        select("Search", tabBar(1));
        enter(query, "Search for:");
        select("Search");
        var results = searchResults();
        waitFor() { return(getText(results).contains(expectedResult)); }
        select(expectedResult, results);
        waitFor() { return(getText(tabBar(1)).contains(expectedResult)); }
        verify(getText(helpViewer()).contains(expectedResult));
    },

    follow_links_data: {
        one: [ "Qt Reference Documentation/Overviews/View Classes", "View Classes",
              [ ["delegates", "Delegate Classes"],
                ["Spin Box Delegate", "Spin Box Delegate Example"],
                ["QItemDelegate", "QItemDelegate Class Reference"],
                ["QSqlRelationalDelegate","QSqlRelationalDelegate Class Reference"] ] ]
    },

    follow_links: function(browseItem, firstTitle, links) {

        // Select browseItem from the Contents
        select("Contents", tabBar());
        var content = helpContent();
        select(browseItem, content);
        waitFor() { return(getText(tabBar(1)).contains(firstTitle)); }

        // Get a handle for the Help Viewer widget
        var viewer = helpViewer(); //findByProperty( { inherits: "QWebView" } );

        // Click on each link in turn
        for (var i=0; i<links.length; ++i) {
            select(links[i][0], viewer);
            waitFor() { return(getText(tabBar(1)).contains(links[i][1])); }
        }

        // Now navigate back...
        for (var i=links.length-1; i>=0; --i) {
            verify(getText(tabBar(1)).contains(links[i][1]));
            select("Back");
        }

        // ... and confirm that we're back to the first page
        verify(getText(tabBar(1)).contains(firstTitle));
    }

}

function contextMenu()
{
    keyClick(Qt.Key_Menu);
    var menu = findByProperty( { inherits: "QMenu" } );
    if (menu.isEmpty()) {
        fail("Context menu did not appear.");
    }
    return menu[0];
}

function helpViewer()
{
    return findByProperty( { className: "HelpViewer" } );
}

function helpContent()
{
    return findByProperty( { className: "QHelpContentWidget" } );
}

function helpIndex()
{
    return findByProperty( { className: "QHelpIndexWidget" } );
}

function helpBookmarks()
{
    return findByProperty( { child_of: "Bookmarks", inherits: "QTreeView" } );
}

function searchResults()
{
    return findByProperty( { className: "QCLuceneResultWidget" } );
}
