/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>

class TestWidget : public QTabWidget
{
Q_OBJECT
public:
    TestWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
private slots:
    void setupWidgets();

private:
    QLineEdit *testEdit;
};
#include "main.moc"

TestWidget::TestWidget(QWidget *parent, Qt::WindowFlags f)
 : QTabWidget(parent)
{
    setWindowFlags(f);
    setWindowTitle("testapp1");
    QTimer::singleShot(0, this, SLOT(setupWidgets()));
}

void TestWidget::setupWidgets() {
    {
        QWidget *page = new QWidget(this);
        QSignalMapper *sm = new QSignalMapper(page);

        QFormLayout *fl(new QFormLayout);

        QLineEdit *statusEdit = new QLineEdit;
        fl->addRow("Status", statusEdit);

        QLineEdit *le1   = new QLineEdit;
        QPushButton *pb1 = new QPushButton("Button1");
        QLineEdit *le2   = new QLineEdit;
        QPushButton *pb2 = new QPushButton("Button2");

        fl->addRow("LineEdit1", le1);
        fl->addRow(pb1);
        fl->addRow("LineEdit2", le2);
        fl->addRow(pb2);

        QComboBox *ecb    = new QComboBox;
        ecb->setEditable(true);
        ecb->addItem("Red");
        ecb->addItem("Green");
        ecb->addItem("Blue");
        ecb->addItem("Cyan");
        ecb->addItem("Magenta");
        ecb->addItem("Yellow");
        ecb->addItem("Black");
        ecb->addItem("White");
        fl->addRow("Colour", ecb);

        QMenu *menu                = new QMenu(page);
        QAction *action1           = menu->addAction("Action 1", sm, SLOT(map()));
        QAction *action2           = menu->addAction("Action 2", sm, SLOT(map()));
        QAction *actionWithSubmenu = menu->addAction("Submenu",  sm, SLOT(map()));
        for ( int i = 1; i <= 8; ++i ) {
            QString menuName = QString("Menu Item #%1").arg(i);
            menu->addAction(menuName);
        }

        QAction *actionLast        = menu->addAction("Last!", sm, SLOT(map()));

        QMenu *submenu             = new QMenu(page);
        actionWithSubmenu->setMenu(submenu);
        QAction *subaction1        = submenu->addAction( "Subaction 1", sm, SLOT(map()) );
        QAction *subaction2        = submenu->addAction( "Subaction 2", sm, SLOT(map()) );

        connect(le1, SIGNAL(textChanged(QString)), sm, SLOT(map()));
        connect(le2, SIGNAL(textChanged(QString)), sm, SLOT(map()));
        connect(pb1, SIGNAL(clicked()),            sm, SLOT(map()));
        connect(pb2, SIGNAL(clicked()),            sm, SLOT(map()));
        connect(ecb, SIGNAL(activated(int)),       sm, SLOT(map()));

        sm->setMapping(le1, "LineEdit1 text changed");
        sm->setMapping(le2, "LineEdit2 text changed");
        sm->setMapping(pb1, "Button1 clicked");
        sm->setMapping(pb2, "Button2 clicked");
        sm->setMapping(ecb, "Colour changed");
        sm->setMapping(action1, "Action 1 activated");
        sm->setMapping(action2, "Action 2 activated");
        sm->setMapping(actionWithSubmenu, "Submenu activated");
        sm->setMapping(actionLast, "Last activated");
        sm->setMapping(subaction1, "Subaction 1 activated");
        sm->setMapping(subaction2, "Subaction 2 activated");

        connect(sm, SIGNAL(mapped(QString)), statusEdit, SLOT(setText(QString)));

        page->setLayout(fl);

        addTab(page, "First Tab");
    }

    {
        QWidget *page = new QWidget;
        QSignalMapper *sm = new QSignalMapper(page);

        QFormLayout *fl(new QFormLayout);

        QLineEdit *statusEdit = new QLineEdit;
        fl->addRow("CowStatus", statusEdit);

        QComboBox *cb    = new QComboBox;
        cb->addItem("Neigh");
        cb->addItem("Woof");
        cb->addItem("Moo");
        cb->addItem("Meow");
        cb->addItem("Quack");
        cb->addItem("Honk");
        cb->addItem("Vrooooom");
        cb->addItem("\"Hi Frank\"");
        cb->addItem("Choo choo!");
        fl->addRow("Cow Goes?", cb);
        connect(cb, SIGNAL(activated(int)), sm, SLOT(map()));
        sm->setMapping(cb, "Cow Goes? changed");

        for (int i = 0; i < 20; ++i) {
            QString name( QString("CowButton%1").arg(i) );
            QPushButton *pb1 = new QPushButton(name);
            fl->addRow(pb1);
            connect(pb1, SIGNAL(clicked()), sm, SLOT(map()));
            sm->setMapping(pb1, name + " clicked");
        }

        connect(sm, SIGNAL(mapped(QString)), statusEdit, SLOT(setText(QString)));

        page->setLayout(fl);

        QScrollArea *sa = new QScrollArea(this);
        sa->setWidget(page);
        sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        sa->setFocusPolicy(Qt::NoFocus);
        sa->setFrameStyle(QFrame::NoFrame);
        sa->setWidgetResizable(true);
        addTab(sa, "Cow Tab");
    }

    {
        QWidget *page = new QWidget;
        QSignalMapper *sm = new QSignalMapper(page);

        QFormLayout *fl(new QFormLayout);

        QLineEdit *statusEdit = new QLineEdit;
        fl->addRow("Status", statusEdit);

        QCheckBox *cb    = new QCheckBox("Checkbox");
        cb->setChecked(false);
        fl->addRow(cb);

        QPushButton *pb  = new QPushButton("Clear");
        fl->addRow(pb);

        QTimeEdit *te    = new QTimeEdit;
        te->setTime(QTime(12, 30, 30));
        te->setDisplayFormat("hh:mm:ss");
        fl->addRow("Time", te);

        QDateEdit *de    = new QDateEdit;
        de->setDate(QDate(2001, 1, 1));
        fl->addRow("Date", de);

        QDateTimeEdit *dte = new QDateTimeEdit;
        dte->setDate(QDate(2010, 12, 1));
        dte->setTime(QTime(15, 0, 0));
        dte->setDisplayFormat("dd MMMM yyyy hh:mm a");
        fl->addRow("DateTime", dte);

        connect(cb, SIGNAL(clicked()),         sm, SLOT(map()));
        connect(pb, SIGNAL(clicked()),         sm, SLOT(map()));
        connect(te, SIGNAL(editingFinished()), sm, SLOT(map()));
        connect(de, SIGNAL(editingFinished()), sm, SLOT(map()));
        connect(dte, SIGNAL(editingFinished()), sm, SLOT(map()));
        sm->setMapping(cb, "'Checkbox' clicked");
        sm->setMapping(pb, "'Clear' clicked");
        sm->setMapping(te, "'Time' edited");
        sm->setMapping(de, "'Date' edited");
        sm->setMapping(dte, "'DateTime' edited");

        connect(sm, SIGNAL(mapped(QString)), statusEdit, SLOT(setText(QString)));

        page->setLayout(fl);

        addTab(page, "Awesome Tab");
    }

    for (int i = 0; i < 5; ++i) {
        QWidget* widget = new QWidget(this);
        addTab(widget, QString("Distant Tab %1").arg(i));
    }
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    TestWidget tw;
    tw.resize(640,480);
    tw.show();
    return app.exec();
}

