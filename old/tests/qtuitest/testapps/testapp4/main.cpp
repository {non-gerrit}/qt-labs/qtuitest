/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include <QtCore>

class TestWidget : public QWidget
{
Q_OBJECT
public:
    TestWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
private slots:
    void setupWidgets();
};
#include "main.moc"

TestWidget::TestWidget(QWidget *parent, Qt::WindowFlags f)
 : QWidget(parent,f)
{
    setWindowTitle("testapp4");
    QTimer::singleShot(0, this, SLOT(setupWidgets()));
}

void TestWidget::setupWidgets() {

    QTabWidget* tw = new QTabWidget;
    QPushButton* tab1 = new QPushButton("A Button", this);
    QPushButton* tab2 = new QPushButton("A Nother Button", this);

    tw->addTab(tab1, "Thirst");
    tw->addTab(tab2, "Sekond");

    QLineEdit* le = new QLineEdit;

    QFormLayout* form = new QFormLayout;
    form->addRow("Thing", le);
    form->addRow(tw);

    setLayout(form);

    le->setFocus();
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    TestWidget tw;
    tw.resize(640,480);
    tw.show();
    return app.exec();
}

