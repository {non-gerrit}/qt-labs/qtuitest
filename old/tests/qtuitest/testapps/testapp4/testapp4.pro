SOURCES*=main.cpp
TEMPLATE=app
unix:!maemo* {
    # avoid the need for make install on *nix
    DESTDIR=$$BUILDROOT/bin
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData NetworkServices
}

win32 {
    target.path=$$INSTALLROOT
    INSTALLS+=target
}

mac {
    CONFIG-=app_bundle
}

maemo5|maemo6 {
    target.path = /usr/local/lib
    INSTALLS += target
}
