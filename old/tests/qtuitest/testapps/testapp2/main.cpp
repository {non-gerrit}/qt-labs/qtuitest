/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>

class TestWidget : public QTabWidget
{
Q_OBJECT
public:
    TestWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
private slots:
    void setupWidgets();
    void raiseDialog();
    void doPopup();
    void doDialog();
    void setButtonText(const QString &text);
private:
    QLineEdit *popupEdit;
    QPushButton *menuButton;
};
#include "main.moc"

TestWidget::TestWidget(QWidget *parent, Qt::WindowFlags f)
 : QTabWidget(parent)
{
    setWindowFlags(f);
    setWindowTitle("testapp2");
    QTimer::singleShot(0, this, SLOT(setupWidgets()));
}

void TestWidget::setupWidgets() {
    {
        QWidget *page(new QWidget);
        QFormLayout *fl(new QFormLayout);
        QSignalMapper *sm(new QSignalMapper(page));

        QLineEdit *statusEdit = new QLineEdit;
        fl->addRow("Status", statusEdit);
        QTimer::singleShot(0, statusEdit, SLOT(setFocus()));

        QPushButton *nothing      = new QPushButton("Do Nothing");
        QPushButton *dialog       = new QPushButton("Show Modal Dialog");
        connect(dialog,      SIGNAL(pressed()), this, SLOT(raiseDialog()));

        QGroupBox *checkableGroupBox = new QGroupBox;
        checkableGroupBox->setCheckable(true);
        checkableGroupBox->setChecked(false);
        checkableGroupBox->setTitle("Checkable");
        QPushButton *buttonInGroupBox = new QPushButton("Groupie");
        {
            QFormLayout *subFl(new QFormLayout);
            subFl->addRow(buttonInGroupBox);
            checkableGroupBox->setLayout(subFl);
        }

        /*
            Test that a focusable widget located in a noncheckable group box
            which contains no other focusable widgets can be referred to using
            the group box label.
        */
        QGroupBox *lineEditGroupBox = new QGroupBox;
        {
            lineEditGroupBox->setCheckable(false);
            lineEditGroupBox->setTitle("Bug206084");
            QHBoxLayout* hbox = new QHBoxLayout;
            hbox->addWidget(new QLineEdit);
            lineEditGroupBox->setLayout(hbox);
        }

        fl->addRow(nothing);
        fl->addRow(dialog);
        fl->addRow(checkableGroupBox);
        fl->addRow(lineEditGroupBox);

        for (int i = 0; i < 5; ++i) {
            fl->addRow(new QPushButton(QString("Spacer %1").arg(i)));
        }

        QTimeEdit *te = new QTimeEdit;
        te->setTime(QTime(12, 30));
        te->setDisplayFormat("hh:mm");
        fl->addRow("Time", te);

        connect(nothing,           SIGNAL(clicked()),     sm, SLOT(map()));
        connect(checkableGroupBox, SIGNAL(clicked(bool)), sm, SLOT(map()));
        connect(buttonInGroupBox,  SIGNAL(clicked()),     sm, SLOT(map()));

        sm->setMapping(nothing,           "'Do Nothing' clicked");
        sm->setMapping(checkableGroupBox, "'Checkable' clicked");
        sm->setMapping(buttonInGroupBox,  "'Groupie' clicked");

        connect(sm, SIGNAL(mapped(QString)), statusEdit, SLOT(setText(QString)));

        page->setLayout(fl);
        QScrollArea *sa = new QScrollArea(this);
        sa->setWidget(page);
        sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        sa->setFocusPolicy(Qt::NoFocus);
        sa->setFrameStyle(QFrame::NoFrame);
        sa->setWidgetResizable(true);
        addTab(sa, "Tab One");
    }

    {
        QWidget *page(new QWidget);
        QFormLayout *fl(new QFormLayout);

        QPushButton *messageButton(new QPushButton("Popup"));
        fl->addRow(messageButton);
        connect(messageButton, SIGNAL(clicked()), this, SLOT(doPopup()));

        QPushButton *dialogButton(new QPushButton("Dialog"));
        fl->addRow(dialogButton);
        connect(dialogButton, SIGNAL(clicked()), this, SLOT(doDialog()), Qt::QueuedConnection);

        popupEdit = new QLineEdit;
        fl->addRow("Popup response", popupEdit);

        QCheckBox *cb     = new QCheckBox("Checkbox");

        QLineEdit *sillyLineEdit1 = new QLineEdit;
        QLineEdit *sillyLineEdit2 = new QLineEdit;
        QLabel *sillyLabel1       = new QLabel("Silly 1");
        QLabel *sillyLabel2       = new QLabel("Silly 2");

        sillyLineEdit1->setVisible(false);
        sillyLineEdit2->setVisible(false);
        sillyLabel1->setVisible(false);
        sillyLabel2->setVisible(false);

        connect(cb, SIGNAL(toggled(bool)), sillyLineEdit1, SLOT(setVisible(bool)));
        connect(cb, SIGNAL(toggled(bool)), sillyLineEdit2, SLOT(setVisible(bool)));
        connect(cb, SIGNAL(toggled(bool)), sillyLabel1,    SLOT(setVisible(bool)));
        connect(cb, SIGNAL(toggled(bool)), sillyLabel2,    SLOT(setVisible(bool)));

        fl->addRow(cb);
        fl->addRow(sillyLabel1, sillyLineEdit1);
        fl->addRow(sillyLabel2, sillyLineEdit2);

        QGroupBox *gb = new QGroupBox("CheckGroup");
        gb->setCheckable(true);
        gb->setChecked(false);
        {
            QFormLayout *subFl(new QFormLayout);
            subFl->addRow(new QPushButton("Do Nothing"));
            gb->setLayout(subFl);
        }
        fl->addRow(gb);

        page->setLayout(fl);
        addTab(page, "Tab Two");
    }

    {
        QWidget *page(new QWidget);
        QFormLayout *fl(new QFormLayout);
        QSignalMapper *sm(new QSignalMapper(page));

        menuButton = new QPushButton("Menu");
        QMenu *menu(new QMenu);
        for ( int i = 1; i <= 6; ++i ) {
            QString menuName = QString("Menu Item #%1").arg(i);
            QAction *a = menu->addAction(menuName, sm, SLOT(map()));
            sm->setMapping(a, a->text());
        }
        menuButton->setMenu(menu);
        fl->addRow("Menu", menuButton);

        connect(sm, SIGNAL(mapped(QString)), this, SLOT(setButtonText(QString)));

        QTimeEdit *te(new QTimeEdit);
        te->setDisplayFormat("hh:mm:ss");
        te->setTime(QTime(12, 34, 56));
        fl->addRow("Time", te);

        QGroupBox *gb(new QGroupBox("Groupbox"));
        QVBoxLayout *vb(new QVBoxLayout);
        vb->addWidget( new QPushButton("Child Button 1") );
        vb->addWidget( new QPushButton("Child Button 2") );
        gb->setLayout(vb);
        fl->addRow(gb);

        page->setLayout(fl);
        addTab(page, "Tab Free");
    }

}

void TestWidget::raiseDialog() {
    QDialog dlg;
    QPushButton pb("Close", &dlg);
    connect(&pb, SIGNAL(clicked()), &dlg, SLOT(accept()));
    dlg.exec();
}

void TestWidget::doPopup() {
    QString opt = (
     QMessageBox::warning(this, "A Message Box", "Please choose yes or no",
        QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) ? "Yes" : "No";
    popupEdit->setText(opt);
}

void TestWidget::doDialog() {
    QDialog dlg;
    dlg.setWindowTitle("New Dialog");

    QPushButton closeButton("Close", &dlg);
    connect(&closeButton, SIGNAL(clicked()), &dlg, SLOT(accept()));

    dlg.exec();
}

void TestWidget::setButtonText(const QString &text) {
    menuButton->setText(text);
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    TestWidget tw;
    tw.resize(640,480);
    tw.show();
    return app.exec();
}
