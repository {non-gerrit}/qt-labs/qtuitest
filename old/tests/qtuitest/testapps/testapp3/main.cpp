/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>

class TestWidget : public QTabWidget
{
Q_OBJECT
public:
    TestWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
private slots:
    void setupWidgets();
};
#include "main.moc"

TestWidget::TestWidget(QWidget *parent, Qt::WindowFlags f)
 : QTabWidget(parent)
{
    setWindowFlags(f);
    setWindowTitle("testapp3");
    QTimer::singleShot(0, this, SLOT(setupWidgets()));
}

void TestWidget::setupWidgets() {
    {
        QWidget *page(new QWidget);
        QFormLayout *fl(new QFormLayout);

        QComboBox *cb = new QComboBox;
        cb->addItems( QStringList() << "Off" << "Ask" << "On" );
        fl->addRow("Automatic", cb);

        cb = new QComboBox;
        cb->addItems( QStringList() << "New York" << "Los Angeles" );
        fl->addRow("Time Zone", cb);

        page->setLayout(fl);
        QScrollArea *sa = new QScrollArea(this);
        sa->setWidget(page);
        sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        sa->setFocusPolicy(Qt::NoFocus);
        sa->setFrameStyle(QFrame::NoFrame);
        sa->setWidgetResizable(true);
        addTab(sa, "Time");
    }

    {
        QWidget *page(new QWidget);
        QFormLayout *fl(new QFormLayout);

        QTextEdit *textEdit = new QTextEdit;
        fl->addRow("Text", textEdit);

        page->setLayout(fl);
        addTab(page, "Text");
    }

    {
        QWidget *page = new QWidget;
        QSignalMapper *sm = new QSignalMapper(page);

        QFormLayout *fl(new QFormLayout);

        QLineEdit *statusEdit = new QLineEdit;
        statusEdit->setFocusPolicy(Qt::NoFocus);
        fl->addRow("Status", statusEdit);

        QPushButton *button1 = new QPushButton("NoFocus1");
        button1->setFocusPolicy(Qt::NoFocus);
        button1->setShortcut(QKeySequence(Qt::Key_Select));
        connect(button1, SIGNAL(clicked()), sm, SLOT(map()));
        sm->setMapping(button1, "NoFocus1 clicked");
        fl->addRow(button1);

        QPushButton *button2 = new QPushButton("NoFocus2");
        button2->setFocusPolicy(Qt::NoFocus);
        button2->setShortcut(QKeySequence(Qt::Key_0));
        connect(button2, SIGNAL(clicked()), sm, SLOT(map()));
        sm->setMapping(button2, "NoFocus2 clicked");
        fl->addRow(button2);

        connect(sm, SIGNAL(mapped(QString)), statusEdit, SLOT(setText(QString)));
        page->setLayout(fl);

        QScrollArea *sa = new QScrollArea(this);
        sa->setWidget(page);
        sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        sa->setFocusPolicy(Qt::NoFocus);
        sa->setFrameStyle(QFrame::NoFrame);
        sa->setWidgetResizable(true);
        addTab(sa, "Shortcuts");
    }

}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    TestWidget tw;
    tw.resize(640,480);
    tw.show();
    return app.exec();
}

