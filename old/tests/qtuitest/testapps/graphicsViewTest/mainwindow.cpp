#include <QtGui>


#include "mainwindow.h"

MainWindow::MainWindow()
{
    const QString pixmapString = "qtlogo4.png";
    item = new QGraphicsPixmapItem(pixmapString);

    textItem = new QGraphicsTextItem("here is some text");
    textItem->setTextInteractionFlags(Qt::TextEditable);
    QGraphicsTextItem *textItem2 = new QGraphicsTextItem("and here is some more");
    textItem2->setTextInteractionFlags(Qt::TextEditable);
    textItem2->setPos(20, 30);
    scene = new QGraphicsScene();
//    scene->addItem(item);
    scene->addItem(textItem);
    scene->addItem(textItem2);
    scene->setBackgroundBrush(Qt::lightGray);

    view = new QGraphicsView(scene);
    view->rotate(-30);
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(view);
    QLabel *label = new QLabel("hello world");
    layout->addWidget(label);
    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    setCentralWidget(widget);
    setWindowTitle(tr("Diagramscene"));


}
