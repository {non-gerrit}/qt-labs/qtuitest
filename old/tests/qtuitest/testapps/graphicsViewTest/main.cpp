
#include <QtGui>

#include "mainwindow.h"

int main(int argv, char *args[])
{
    QApplication gearsofwartactics(argv, args);
    MainWindow mainWindow;
    mainWindow.setGeometry(100, 100, 1024, 768);
    mainWindow.show();

    return gearsofwartactics.exec();
}

