TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
unix:!maemo* {
    # avoid the need for make install on *nix
    DESTDIR=$$BUILDROOT/bin
}

HEADERS += mainwindow.h
SOURCES += main.cpp mainwindow.cpp
mac:DESTDIR=$$BUILDROOT/bin


target.path += \
    /usr/local/bin

INSTALLS += \
    target