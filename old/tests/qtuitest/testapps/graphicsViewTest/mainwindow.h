#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QGraphicsView;
class QGraphicsScene;
class QGraphicsItem;
class QGraphicsTextItem;
class QGraphicsPixmapItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
private:

    QGraphicsView *view;
    QGraphicsScene *scene;
    QGraphicsItem *item;
    QGraphicsTextItem *textItem;
    QGraphicsPixmapItem *gowtPixmapItem;
};

#endif
