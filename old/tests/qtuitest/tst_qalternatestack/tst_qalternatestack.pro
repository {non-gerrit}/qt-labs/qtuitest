TEMPLATE=app
CONFIG+=unittest
QT = core
TARGET=tst_qalternatestack
CONFIG += qtestlib

CONFIG-=debug_and_release_target

SOURCES+=                   \
    tst_qalternatestack.cpp

include($$SRCROOT/libqtuitest/libqtuitest.pri)


target.path += \
    /usr/local/bin

INSTALLS += \
    target