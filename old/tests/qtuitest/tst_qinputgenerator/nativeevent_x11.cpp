#include "nativeevent.h"

#include <X11/Xlib.h>

template <typename T>
QString hexstr(T const& thing)
{ return QString("0x%1").arg(thing, 8, 16, QLatin1Char('0')); }

NativeEvent::NativeEvent(void* event)
{
    XEvent* e = static_cast<XEvent*>(event);

#define GET_COMMON_THINGS(event) \
    things << qMakePair(QString("serial"),     hexstr(event->serial));                  \
    things << qMakePair(QString("send_event"), QString::number(event->send_event));     \
    things << qMakePair(QString("x"),          QString::number(event->x));              \
    things << qMakePair(QString("y"),          QString::number(event->y));              \
    things << qMakePair(QString("x_root"),     QString::number(event->x_root));         \
    things << qMakePair(QString("y_root"),     QString::number(event->y_root));         \
    things << qMakePair(QString("state"),      hexstr(event->state));                   \
    things << qMakePair(QString("same_screen"),QString("%1").arg(event->same_screen));

    switch (e->type) {
        case KeyPress:
        case KeyRelease: {
            if (e->type == KeyPress)   type = "XKeyPressedEvent";
            if (e->type == KeyRelease) type = "XKeyReleasedEvent";
            XKeyEvent* xe = static_cast<XKeyEvent*>(event);
            things << qMakePair(QString("keycode"), hexstr(xe->keycode));
            GET_COMMON_THINGS(xe); }
            break;

        case ButtonPress:
        case ButtonRelease: {
            if (e->type == ButtonPress)   type = "XButtonPressedEvent";
            if (e->type == ButtonRelease) type = "XButtonReleasedEvent";
            XButtonEvent* xe = static_cast<XButtonEvent*>(event);
            things << qMakePair(QString("button"), hexstr(xe->button));
            GET_COMMON_THINGS(xe); }
            break;

        case MotionNotify: {
            type = "XMotionEvent";
            XMotionEvent* xe = static_cast<XMotionEvent*>(event);
            GET_COMMON_THINGS(xe); }
            break;

        case EnterNotify:
        case LeaveNotify: {
            if (e->type == EnterNotify) type = "XEnterWindowEvent";
            if (e->type == LeaveNotify) type = "XLeaveWindowEvent";
            XCrossingEvent* xe = static_cast<XCrossingEvent*>(event);
            things << qMakePair(QString("focus"), QString("%1").arg(xe->focus));
            GET_COMMON_THINGS(xe); }
            break;

        default:
            type = QString("XEvent(type=%1)").arg(e->type);
            break;
    }
}

/* Returns true if the event is related to key or mouse inputs */
bool NativeEvent::isInteresting(void* event)
{
    if (!event) return false;

    XEvent* e = static_cast<XEvent*>(event);
    switch (e->type) {
        case KeyPress:
        case KeyRelease:
        case ButtonPress:
        case ButtonRelease:
        case MotionNotify:
        case EnterNotify:
        case LeaveNotify:
            return true;
        default:
            break;
    }

    return false;
}


