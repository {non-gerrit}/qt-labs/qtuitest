#ifndef NATIVEEVENT_H
#define NATIVEEVENT_H

#include <QList>
#include <QPair>
#include <QString>

struct NativeEvent
{
    NativeEvent(void*);

    static bool isInteresting(void*);

    QString                        type;
    QList<QPair<QString,QString> > things;
};

#endif

