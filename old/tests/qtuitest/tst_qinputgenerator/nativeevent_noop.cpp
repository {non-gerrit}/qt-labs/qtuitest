#include "nativeevent.h"

NativeEvent::NativeEvent(void*)
{}

bool NativeEvent::isInteresting(void*)
{
    static bool warned = false;
    if (!warned) {
        warned = true;
        qWarning("Don't understand native events, only logging Qt events.");
    }

    return false;
}


