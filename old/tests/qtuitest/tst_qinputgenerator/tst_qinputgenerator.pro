TEMPLATE=app
CONFIG+=unittest
QT = core gui
TARGET=tst_qinputgenerator
CONFIG+=qtestlib

CONFIG-=debug_and_release_target

SOURCES+=                   \
    tst_qinputgenerator.cpp

unix:!mac:!embedded:!symbian {
    SOURCES+=nativeevent_x11.cpp
}
else {
    SOURCES+=nativeevent_noop.cpp
}

include($$SRCROOT/libqtuitest/libqtuitest.pri)



target.path += \
    /usr/local/bin

INSTALLS += \
    target