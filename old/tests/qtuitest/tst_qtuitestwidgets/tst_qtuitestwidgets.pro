TEMPLATE=app
CONFIG+=unittest
CONFIG+=qtestlib
QT = core gui

TARGET=tst_qtuitestwidgets

CONFIG-=debug_and_release_target

SOURCES+=                   \
    tst_qtuitestwidgets.cpp

DEFINES+=QT_STATICPLUGIN

include($$SRCROOT/libqtuitest/libqtuitest.pri)

symbian {
    LIBS+=-L$$OUT_PWD -lqtwidgets
}



target.path += \
    /usr/local/bin

INSTALLS += \
    target