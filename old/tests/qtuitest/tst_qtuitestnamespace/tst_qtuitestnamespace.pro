TEMPLATE=app
CONFIG+=unittest
QT = core gui
TARGET=tst_qtuitestnamespace
CONFIG+=qtestlib

CONFIG-=debug_and_release_target

include($$SRCROOT/libqtuitest/libqtuitest.pri)

SOURCES+=                   \
    tst_qtuitestnamespace.cpp



target.path += \
    /usr/local/bin

INSTALLS += \
    target
