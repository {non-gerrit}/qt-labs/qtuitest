<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>Numbers</name>
    <message>
        <source>one</source>
        <translation>un</translation>
    </message>
    <message>
        <source>two</source>
        <translation>deux</translation>
    </message>
    <message>
        <source>three</source>
        <translation>trois</translation>
    </message>
    <message>
        <source>four</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>five</source>
        <translation>cinq</translation>
    </message>
    <message>
        <source>six</source>
        <translation>six</translation>
    </message>
    <message>
        <source>seven</source>
        <translation>sept</translation>
    </message>
    <message>
        <source>eight</source>
        <translation>huit</translation>
    </message>
    <message>
        <source>nine</source>
        <translation>neuf</translation>
    </message>
    <message>
        <source>ten</source>
        <translation>dix</translation>
    </message>
</context>
<context>
    <name>Ordinals</name>
    <message>
        <source>first</source>
        <translation>premier</translation>
    </message>
    <message>
        <source>second</source>
        <translation>deuxième</translation>
    </message>
    <message>
        <source>third</source>
        <translation>troisième</translation>
    </message>
    <message>
        <source>fourth</source>
        <translation>quatrième</translation>
    </message>
    <message>
        <source>fifth</source>
        <translation>cinquième</translation>
    </message>
    <message>
        <source>sixth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>seventh</source>
        <translation>septième</translation>
    </message>
    <message>
        <source>eighth</source>
        <translation>huitième</translation>
    </message>
    <message>
        <source>ninth</source>
        <translation>neuvième</translation>
    </message>
    <message>
        <source>tenth</source>
        <translation>dixième.</translation>
    </message>
</context>
<context>
    <name>Colours</name>
    <message>
        <source>red</source>
        <translation>rouge</translation>
    </message>
    <message>
        <source>orange</source>
        <translation>orange</translation>
    </message>
    <message>
        <source>yellow</source>
        <translation>jaune</translation>
    </message>
    <message>
        <source>green</source>
        <translation>vert</translation>
    </message>
    <message>
        <source>blue</source>
        <translation>bleu</translation>
    </message>
    <message>
        <source>a mauvy shade of pinky russet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Placemarkers</name>
    <message>
        <source>Read: %1</source>
        <translation>Lecture : %1</translation>
    </message>
    <message>
        <source>Write: %1</source>
        <translation>Écriture : %1</translation>
    </message>
    <message>
        <source>Delete: %1</source>
        <translation>Supprimer: %1</translation>
    </message>
    <message>
        <source>Read error reading from %1: %2</source>
        <translation>Erreur de lecture sur %1</translation>
    </message>
    <message>
        <source>Write error writing to %1: %2</source>
        <translation>Erreur d&apos;écriture sur %1: %2</translation>
    </message>
</context>
<context>
    <name>Accelerators</name>
    <message>
        <source>&amp;hello world</source>
        <translation>bonjour tout le monde</translation>
    </message>
    <message>
        <source>&amp;goodbye</source>
        <translation>au &amp;revoir</translation>
    </message>
</context>
<context>
    <name>Other</name>
    <message>
        <source>all for one, one for all.</source>
        <translation>tout pour un, un pour tout</translation>
    </message>
</context>
</TS>
