TEMPLATE=subdirs
SUBDIRS+=       \
    testapps    \
    sys_input   \
    sys_assistant \
    sys_designer \
    sys_graphicsView \
    sys_linguist \
    tst_qalternatestack \
    tst_qelapsedtimer \
    tst_qinputgenerator \
    tst_qtuitestnamespace

# Tests which should run in Pulse (currently, those which do not require X server)
PULSE_TESTS = \
    tst_qalternatestack \
    tst_qelapsedtimer   \
    tst_qtestprotocol   \

!win32 {
    SUBDIRS+=tst_qtestprotocol tst_qtuitestwidgets
}

symbian {
    SUBDIRS-=tst_qalternatestack tst_qtestprotocol tst_qtuitestwidgets
}

pulsetest.CONFIG = recursive
pulsetest.recurse = $$PULSE_TESTS
pulsetest.target = pulsetest
pulsetest.recurse_target = junittest
QMAKE_EXTRA_TARGETS += pulsetest

