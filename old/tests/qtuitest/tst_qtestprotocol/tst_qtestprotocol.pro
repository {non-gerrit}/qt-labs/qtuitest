TEMPLATE=app
CONFIG+=unittest
QT = core network
TARGET=tst_qtestprotocol
CONFIG+=qtestlib

include($$SRCROOT/libqtuitest/libqtuitest.pri)

CONFIG-=debug_and_release_target

SOURCES+=                   \
    tst_qtestprotocol.cpp   \
    testprotocol.cpp        \
    testprotocolserver.cpp

HEADERS+=                   \
    testprotocol.h          \
    testprotocolserver.h


target.path += \
    /usr/local/bin

INSTALLS += \
    target