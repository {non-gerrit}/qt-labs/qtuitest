/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QTest>

// for m_map
#define protected public
# include "qtestprotocol_p.h"
#undef protected

#include "testprotocol.h"
#include "testprotocolserver.h"

//TESTED_COMPONENT=QA: Testing Framework (18707)

class tst_QTestProtocol : public QObject
{
    Q_OBJECT

private slots:
    void postMessage();
    void postMessage_data();
};

QTEST_MAIN(tst_QTestProtocol)

/*
    Comparison for QTestMessage.
*/
namespace QTest {
template<>
inline bool qCompare
    (QTestMessage const &m1, QTestMessage const &m2,
     const char* actual, const char* expected, const char* file, int line)
{
#define COMPARE_MEMBER(a,b,args...)                                      \
    qCompare(a.args, b.args,                            \
        qPrintable(QString("%1.%2").arg(actual).arg(#args)),   \
        qPrintable(QString("%1.%2").arg(expected).arg(#args)), \
            file, line)

    return COMPARE_MEMBER(m1,m2,event())
        && COMPARE_MEMBER(m1,m2,toString())
        && COMPARE_MEMBER(m1,m2,m_map);

#undef COMPARE_MEMBER
}
}

/*
    \req QTOPIA-78

    \groups
    Simply tests that messages can be sent correctly without becoming corrupt.
*/
void tst_QTestProtocol::postMessage_data()
{
    QTest::addColumn<QTestMessage>("message");

    QTest::newRow("empty")
        << QTestMessage();

    QTest::newRow("simple, no map")
        << QTestMessage("foo");

    {
        QVariantMap vm;
        vm["foo"] = 1;
        vm["bar"] = "Hello.";

        QTest::newRow("simple, small map")
            << QTestMessage("foobar", vm);
    }

    {
        QByteArray ba;
        while (ba.size() < 1024*1024*10)
            ba.append("zoop");

        QVariantMap vm;
        vm["aaa"] = "A";
        vm["bbb"] = ba;
        vm["ccc"] = "C";

        QTest::newRow("10 Mb")
            << QTestMessage("big_test", vm);
    }
}

// Will try to wait for the condition while allowing event processing
#define QTRY_COMPARE(__expr, __expected) \
    do { \
        const int __step = 50; \
        const int __timeout = 5000; \
        if ((__expr) != (__expected)) { \
                        QTest::qWait(0); \
                    } \
        for (int __i = 0; __i < __timeout && ((__expr) != (__expected)); __i+=__step) { \
                        QTest::qWait(__step); \
                    } \
        QCOMPARE(__expr, __expected); \
    } while(0)

void tst_QTestProtocol::postMessage()
{
    // Start a server to listen for connections.
    TestProtocolServer server;
    QVERIFY(server.listen());

    // Create a test protocol which connects to the server.
    TestProtocol* clientP = new TestProtocol;
    clientP->setParent(&server);
    clientP->connect( "127.0.0.1", server.serverPort() );

    QVERIFY( clientP->waitForConnected(1000) );
    QVERIFY( server.hasPendingConnections() );

    // Get the protocol which should have been created on the server side
    TestProtocol* serverP = server.nextPendingConnection();
    QVERIFY( serverP );
    // Should be no further connections
    QVERIFY( !server.hasPendingConnections() );

    // OK, now we have two peers.  They should both be connected by now.
    QVERIFY( clientP->isConnected() );
    QVERIFY( serverP->isConnected() );

    QList<QTestMessage>& clientMessages = clientP->messages();
    QList<QTestMessage>& serverMessages = serverP->messages();

    QFETCH(QTestMessage, message);

    // Try sending the message from client to server.
    uint post_id = clientP->postMessage(message);
    QVERIFY(post_id);
    QCOMPARE(serverMessages.count(), 0);
    QTRY_COMPARE(serverMessages.count(), 1);

    // Verify the message is OK.
    QCOMPARE(serverMessages.takeFirst(), message);


    // Try sending the message from server to client.
    post_id = serverP->postMessage(message);
    QVERIFY(post_id);
    QCOMPARE(clientMessages.count(), 0);
    QTRY_COMPARE(clientMessages.count(), 1);

    // Verify the message is OK.
    QCOMPARE(clientMessages.takeFirst(), message);
}

#include "tst_qtestprotocol.moc"

