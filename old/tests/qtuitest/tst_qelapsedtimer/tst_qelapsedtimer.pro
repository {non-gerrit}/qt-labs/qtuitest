TEMPLATE=app
CONFIG+=unittest
QT = core
CONFIG+=qtestlib

TARGET=tst_qelapsedtimer

CONFIG-=debug_and_release_target

SOURCES+=                   \
    tst_qelapsedtimer.cpp

include($$SRCROOT/libqtuitest/libqtuitest.pri)



target.path += \
    /usr/local/bin

INSTALLS += \
    target