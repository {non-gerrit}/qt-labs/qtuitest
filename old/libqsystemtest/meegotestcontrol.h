#ifndef MEEGOTESTCONTROL_H
#define MEEGOTESTCONTROL_H

#include "testcontrol.h"

namespace Qt4Test {
class MeeGoTestControl : public TestControl
{
    Q_OBJECT
public:
    MeeGoTestControl();
    virtual ~MeeGoTestControl();

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );
};
}

#endif // MEEGOTESTCONTROL_H
