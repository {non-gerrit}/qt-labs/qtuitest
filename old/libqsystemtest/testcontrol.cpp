/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of Qt Creator.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testcontrol.h"
#include "desktoptestcontrol.h"
#include "customtestcontrol.h"
#include "maemotestcontrol.h"
#include "meegotestcontrol.h"
#include "symbiantestcontrol.h"

#include <QDebug>
#include <QRegExp>
#include <QTime>
#include <QApplication>
#include <QTimer>

namespace Core {
    class SshConnection;
    class SshRemoteProcess;
} // namespace Core

using namespace Core;

namespace Qt4Test {

TestControl::TestControl()
{

}

TestControl::~TestControl()
{

}

TestController::TestController( TestDeviceType type, Core::SshConnectionParameters *sshParam )
{
    if (sshParam) ssh_param = *sshParam;
    test_device_type = type;
}

TestController::~TestController()
{
    killApplications();

    while (device_controls.count() > 0)
        delete device_controls.takeFirst();
}

TestControl* TestController::controlFactory()
{
    if (test_device_type == Maemo)
        return new MaemoTestControl(ssh_param);
//        return MaemoTestControl::instance(ssh_param);
    else if (test_device_type == Desktop)
//        return new DesktopTestControl();
        return DesktopTestControl::instance();    
    else if (test_device_type == MeeGo)
        return new MeeGoTestControl();
//        return MeeGoTestControl::instance();
    else if (test_device_type == Custom)
        return new CustomTestControl();
//        return CustomTestControl::instance();
    else if (test_device_type == Symbian)
        return new SymbianTestControl();
//        return SymbianTestControl::instance();    
    return 0;
}

bool TestController::deviceConfiguration( QString &reply )
{
    TestControl *ctrl = controlFactory();
    bool ok = (ctrl && ctrl->deviceConfiguration(reply));
    delete ctrl;
    return ok;
}

bool TestController::startApplication( const QString &application, const QStringList &arguments,
                                         bool styleQtUITest, const QStringList &environment, QString &reply )
{
    TestControl *ctrl = controlFactory();
    if (ctrl) {
//        device_controls.append(ctrl);
        if (ctrl->startApplication(application, arguments, styleQtUITest, environment, reply)) {
            started_applications += application;
            return true;
        }
    }
    return false;
}

bool TestController::killApplication( const QString &application, QString &reply )
{
    TestControl *ctrl = controlFactory();
    if (ctrl) {
//        device_controls.append(ctrl);
        return ctrl->killApplication(application, reply);
    }
    return false;
}

void TestController::killApplications()
{
    if (started_applications.count() == 0)
        return;

    QString reply;
    foreach (QString app, started_applications) {
        killApplication(app, reply);
    }
    started_applications.clear();
}

} // namespace Qt4Test
