#ifndef DESKTOPTESTCONTROL_H
#define DESKTOPTESTCONTROL_H

#include "testcontrol.h"
#include "qsystemtest.h"

#include <QProcess>

namespace Qt4Test {

class TestProcess : public QProcess
{
    Q_OBJECT
    public:
        TestProcess(QObject *parent = 0)
            : QProcess(parent)
            , env()
            , test(0)
        {
            connect(this,SIGNAL(readyReadStandardError()),this,SLOT(error()));
            connect(this,SIGNAL(readyReadStandardOutput()),this,SLOT(output()));
            connect(this,SIGNAL(finished(int,QProcess::ExitStatus)),
                    this,SLOT(deleteLater()));
        }

        QStringList env;
        QSystemTest* test;

    private slots:
        void output()
        {
            QByteArray text = readAllStandardOutput();
            if (!test) return;

            while (text.endsWith("\n")) text.chop(1);
            QList<QByteArray> lines = text.split('\n');
            test->applicationStandardOutput(lines);
        }

        void error()
        {
            QByteArray text = readAllStandardError();
            if (!test) return;

            while (text.endsWith("\n")) text.chop(1);
            QList<QByteArray> lines = text.split('\n');
            test->applicationStandardError(lines);
        }

    protected:
        virtual void setupChildProcess()
        {
            foreach (QString const& e, env) {
                int equals = e.indexOf('=');
                if (equals == -1) continue;
                QString key = e.left(equals);
                QString value = e.mid(equals+1);
                prependToEnv(key.toLocal8Bit(), value.toLocal8Bit());
            }
        }

    private:
        void prependToEnv(QByteArray const& key, QByteArray const& value)
        {
            // Environment variables which are a colon-separated list (like PATH)
            static const QList<QByteArray> pathlike = QList<QByteArray>()
                << "LD_LIBRARY_PATH"
            ;

            // Environment variables which can be silently clobbered
            static const QList<QByteArray> clobber = QList<QByteArray>()
                << "DISPLAY"
            ;

            QByteArray current = qgetenv(key.constData());
            QByteArray set = value;
            if (!current.isEmpty()) {
                if (pathlike.contains(key)) {
                    set = set + ":" + current;
                } else if (clobber.contains(key)) {
                } else {
                    // Cannot use qWarning because we are in a child process and we will pass
                    // through message handlers twice
                    fprintf(stderr, "Environment variable %s is already set (to \"%s\"); "
                            "qtuitestrunner will clobber it (with \"%s\") when starting test "
                            "process!"
                        ,key.constData()
                        ,current.constData()
                        ,set.constData()
                    );
                }
            }
#ifndef Q_OS_WIN
            setenv(key.constData(), set.constData(), 1);
#endif
        }
};


class DesktopTestControl : public TestControl
{
    Q_OBJECT
public:
    static DesktopTestControl* instance();
    DesktopTestControl();
    virtual ~DesktopTestControl();

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );

private:
    TestProcess* proc;
};

}

#endif // DESKTOPTESTCONTROL_H
