/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QABSTRACTTEST_H
#define QABSTRACTTEST_H

#if ! defined Q_QDOC

#include <QObject>
#include <QTest>
#include "qtuitestglobal.h"
#include <qdebug.h>

class QSYSTEMTEST_EXPORT Autotest_QLog {
public:
    static inline bool enabled() { return m_enabled; }
    static QDebug log(const char* category);
private:
    static bool m_enabled;
    friend class QAbstractTest;
};

class QAbstractTestPrivate;
class FatalTimeoutThread;
class Timer;

class QSYSTEMTEST_EXPORT QTimedTest : public QObject
{
#ifndef Q_MOC_RUN
    Q_OBJECT
#endif
public:
    QTimedTest(QObject *parent=0);
    virtual ~QTimedTest();

    int real_qt_metacall(QMetaObject::Call, int, void **);

protected:
    bool pass_through;
    bool print_times;
    bool have_init;
    bool have_cleanup;
    FatalTimeoutThread *timeout_thread;
    Timer *realTimer;
    Timer *cpuTimer;
};

#ifdef Q_QDOC
namespace QAbstractTest
#else
class QSYSTEMTEST_EXPORT QAbstractTest : public QTimedTest
#endif
{
    Q_OBJECT

public:
    enum LearnMode
    {
        LearnNone,
        LearnNew,
        LearnAll
    };

#ifndef Q_QDOC
    QAbstractTest(QString const &srcdir =
#ifdef QTUITEST_SRCDIR
            QTUITEST_SRCDIR
#else
            QString()
#endif
            , QObject *parent = 0);
    virtual ~QAbstractTest();

# ifndef QTCREATOR_QTEST
    int exec( int argc, char* argv[], char* filename = 0 );
# endif
#endif

public slots:
    LearnMode learnMode() const;
    void setLearnMode(LearnMode mode);
    bool failEmptyTest() const;

    QString currentDataPath() const;
    QString baseDataPath() const;
    void setBaseDataPath(QString const &path);
#ifdef TESTS_SHARED_DIR
    QString sharedDataPath() const;
#endif
    QString currentDataTag() const;
    virtual QString testCaseName() const;
    QString currentTestFunction( bool fullName = false ) const;

#ifndef Q_QDOC
protected:
    virtual int runTest(const QString &fname, const QStringList &parameters,
                        const QStringList &environment);
    virtual void printUsage() const;
    virtual void processCommandLine(QStringList&);    

    virtual void setupTestDataPath(const char *filename);
#endif

private:
    QAbstractTestPrivate *d;
};

#endif

#endif
