/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef RECORDEVENT_P_H
#define RECORDEVENT_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtUiTest API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QString>
#include <QVariant>

struct RecordEvent {
    enum Type {
        GotFocus,
        Entered,
        Selected,
        Activated,
        CheckStateChanged,
        TitleChanged,
        MessageBoxShown
    };
    Type     type;
    QString  widget;
    QString  focusWidget;
    QVariant data;
};

#define Q_DECLARE_METATYPE_STREAM(TYPE)                                 \
    QT_BEGIN_NAMESPACE                                                  \
    template <>                                                         \
    struct QMetaTypeId< TYPE >                                          \
    {                                                                   \
        enum { Defined = 1 };                                           \
        static int qt_metatype_id()                                     \
            {                                                           \
                static QBasicAtomicInt metatype_id = Q_BASIC_ATOMIC_INITIALIZER(0); \
                if (!metatype_id) {                                     \
                    metatype_id = qRegisterMetaType< TYPE >(#TYPE,      \
                               reinterpret_cast< TYPE *>(quintptr(-1))); \
                    qRegisterMetaTypeStreamOperators< TYPE >(#TYPE);    \
                }                                                       \
                return metatype_id;                                     \
            }                                                           \
    };                                                                  \
    QT_END_NAMESPACE

Q_DECLARE_METATYPE_STREAM(RecordEvent);
Q_DECLARE_METATYPE_STREAM(QList<RecordEvent>);

inline bool operator==(RecordEvent const& a, RecordEvent const& b)
{ return a.type == b.type && a.widget == b.widget && a.focusWidget == b.focusWidget && a.data == b.data; }

inline bool operator!=(RecordEvent const& a, RecordEvent const& b)
{ return !(a == b); }

inline QDataStream &operator<<(QDataStream &out, const RecordEvent &re)
{ return (out << static_cast<int>(re.type) << re.widget << re.focusWidget << re.data); }

inline QDataStream &operator>>(QDataStream &in, RecordEvent &re)
{
    int reType;
    QDataStream &ret = (in >> reType >> re.widget >> re.focusWidget >> re.data);
    re.type = static_cast<RecordEvent::Type>(reType);
    return ret;
}

inline QDataStream &operator<<(QDataStream &out, const QList<RecordEvent> &l)
{
    out << l.count();
    foreach (RecordEvent re, l) {
        out << re;
    }
    return out;
}
inline QDataStream &operator>>(QDataStream &in, QList<RecordEvent> &l)
{
    int count = 0;
    in >> count;
    RecordEvent re;
    for (int i = 0; i < count; ++i) {
        in >> re;
        l << re;
    }
    return in;
}

#endif

