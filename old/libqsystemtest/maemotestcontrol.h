#ifndef MAEMOTESTCONTROL_H
#define MAEMOTESTCONTROL_H

#include "testcontrol.h"
#include <coreplugin/ssh/sshconnection.h>
#include <coreplugin/ssh/sshremoteprocess.h>

namespace Qt4Test {
class MaemoTestControl : public TestControl
{
    Q_OBJECT
public:
    MaemoTestControl( Core::SshConnectionParameters &sshParam );
    virtual ~MaemoTestControl();

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );

protected:
    bool runCustomCommand( const QString &cmd, int timeout, QString &reply );
    bool runCustomBlockingCommand( const QString &cmd, int timeout, QString &reply );

private slots:
    void cleanupConnection();

    void processSshOutput(const QByteArray &output);
    void handleConnected();
    void handleConnectionError();
    void handleProcessFinished(int exitStatus);

private:
    bool initConnection();
    QString parseOutput();
    void waitForCommandToFinish(int timeout);

    enum MaemoTestControlMode {Idle, ConfigCheck, CustomCommand, CustomBlockingCommand};
    MaemoTestControlMode control_mode;
    void runBlockingCommand( MaemoTestControlMode mode, const QString &cmd, int timeout);

    QSharedPointer<Core::SshConnection> m_connection;
    QSharedPointer<Core::SshRemoteProcess> m_testProcess;
    Core::SshConnectionParameters ssh_param;
    QString m_deviceTestOutput;
    bool m_qtVersionOk;
    bool reply_ok;
    QString reply_txt;
    QString m_cmd;
};
}

#endif // MAEMOTESTCONTROL_H
