/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QSYSTEMTEST_H
#define QSYSTEMTEST_H

#include "qabstracttest.h"
#include "qtestprotocol_p.h"
#include "recordevent_p.h"
#include <testcontrol.h>

#include <QStringList>
#include <QDir>
#include <QLocale>
#include <QPointer>

class QProcess;
class QSystemTestMail;
class QSystemTestMaster;
class QTextEdit;

#define DEFAULT_QUERY_TIMEOUT 120000

class QTestMessage;

#ifdef Q_QDOC
namespace QSystemTest
#else
class QSYSTEMTEST_EXPORT QSystemTest : public QAbstractTest
#endif
{
Q_OBJECT
Q_ENUMS(SkipMode)
Q_ENUMS(EnterMode)
Q_ENUMS(LabelOrientation)
Q_ENUMS(StartApplicationFlag)
Q_FLAGS(StartApplicationFlags)
Q_ENUMS(Role)
Q_ENUMS(StateFlag)
Q_FLAGS(State)

public:
    QSystemTest();
    virtual ~QSystemTest();

    enum SkipMode
    {
        SkipSingle                   = 0x01,
        SkipAll                      = 0x02
    };

    enum EnterMode
    {
        Commit                       = 0x00,
        NoCommit                     = 0x01
    };

    enum LabelOrientation
    {
        LabelLeft                    = 0x01,
        LabelRight                   = 0x02,
        LabelAbove                   = 0x03,
        LabelBelow                   = 0x04
    };

    enum StartApplicationFlag
    {
        NoFlag                       = 0x00,
        WaitForFocus                 = 0x01,
        BackgroundCurrentApplication = 0x02
    };
    Q_DECLARE_FLAGS(StartApplicationFlags, StartApplicationFlag)

    // This should match QAccessible::Role
    enum Role {
        NoRole         = 0x00000000,
        TitleBar       = 0x00000001,
        MenuBar        = 0x00000002,
        ScrollBar      = 0x00000003,
        Grip           = 0x00000004,
        Sound          = 0x00000005,
        Cursor         = 0x00000006,
        Caret          = 0x00000007,
        AlertMessage   = 0x00000008,
        Window         = 0x00000009,
        Client         = 0x0000000A,
        PopupMenu      = 0x0000000B,
        MenuItem       = 0x0000000C,
        ToolTip        = 0x0000000D,
        Application    = 0x0000000E,
        Document       = 0x0000000F,
        Pane           = 0x00000010,
        Chart          = 0x00000011,
        Dialog         = 0x00000012,
        Border         = 0x00000013,
        Grouping       = 0x00000014,
        Separator      = 0x00000015,
        ToolBar        = 0x00000016,
        StatusBar      = 0x00000017,
        Table          = 0x00000018,
        ColumnHeader   = 0x00000019,
        RowHeader      = 0x0000001A,
        Column         = 0x0000001B,
        Row            = 0x0000001C,
        Cell           = 0x0000001D,
        Link           = 0x0000001E,
        HelpBalloon    = 0x0000001F,
        Assistant      = 0x00000020,
        List           = 0x00000021,
        ListItem       = 0x00000022,
        Tree           = 0x00000023,
        TreeItem       = 0x00000024,
        PageTab        = 0x00000025,
        PropertyPage   = 0x00000026,
        Indicator      = 0x00000027,
        Graphic        = 0x00000028,
        StaticText     = 0x00000029,
        EditableText   = 0x0000002A,
        PushButton     = 0x0000002B,
        CheckBox       = 0x0000002C,
        RadioButton    = 0x0000002D,
        ComboBox       = 0x0000002E,
        ProgressBar    = 0x00000030,
        Dial           = 0x00000031,
        HotkeyField    = 0x00000032,
        Slider         = 0x00000033,
        SpinBox        = 0x00000034,
        Canvas         = 0x00000035,
        Animation      = 0x00000036,
        Equation       = 0x00000037,
        ButtonDropDown = 0x00000038,
        ButtonMenu     = 0x00000039,
        ButtonDropGrid = 0x0000003A,
        Whitespace     = 0x0000003B,
        PageTabList    = 0x0000003C,
        Clock          = 0x0000003D,
        Splitter       = 0x0000003E,
        LayeredPane    = 0x0000003F,
        UserRole       = 0x0000ffff
    };

    // This should match QAccessible::StateFlag
    enum StateFlag {
        Normal          = 0x00000000,
        Unavailable     = 0x00000001,
        Selected        = 0x00000002,
        Focused         = 0x00000004,
        Pressed         = 0x00000008,
        Checked         = 0x00000010,
        Mixed           = 0x00000020,
        ReadOnly        = 0x00000040,
        HotTracked      = 0x00000080,
        DefaultButton   = 0x00000100,
        Expanded        = 0x00000200,
        Collapsed       = 0x00000400,
        Busy            = 0x00000800,
        Marqueed        = 0x00002000,
        Animated        = 0x00004000,
        Invisible       = 0x00008000,
        Offscreen       = 0x00010000,
        Sizeable        = 0x00020000,
        Movable         = 0x00040000,
        SelfVoicing     = 0x00080000,
        Focusable       = 0x00100000,
        Selectable      = 0x00200000,
        Linked          = 0x00400000,
        Traversed       = 0x00800000,
        MultiSelectable = 0x01000000,
        ExtSelectable   = 0x02000000,
        Protected       = 0x20000000,
        HasPopup        = 0x40000000,
        Modal           = 0x80000000
    };
    Q_DECLARE_FLAGS(State, StateFlag)

public slots:
    virtual bool runAsManualTest(void);
    virtual void manualTest( const QString &description );
    virtual void manualTestData( const QString &description, bool isAlternative = false );

    void prompt(const QString& = QString());

    // commands to control test execution
    void strict( bool = true );
    void setDemoMode( bool );
    static QString userName();
    bool shouldFilterMessage(char const*);
    QMap<QString, int> filteredMessages() const;
    void clearFilteredMessages();
    bool runsOnDevice();

    virtual void skip(const QString&, SkipMode);
    virtual void expectFail(const QString&);

    QString signature(const QString&, int = 0);

    // widget checks
    bool isVisible(const QString&);
    bool isEnabled(const QString&);

    // checkBox manipulators
    bool isChecked(const QString&);
    bool isChecked(const QString&, const QString&);
    void setChecked(bool,const QString& = QString());
    void setChecked(bool,const QString&,const QString&);
    int checkState(const QString&);
    void setCheckState(int,const QString& = QString());

    // data verification functions
    QString currentApplication();
    QString applicationVersion();
    QString qtVersion();
    QString currentTitle(const QString& = QString());
    QStringList getWindowTitles();
    void activateWindow(const QString&);
    QString activeWindow();
    QString focusWidget();
    QString getSelectedText(const QString& = QString() );
    QString getText(const QString& = QString() );
    QVariant getSelectedValue(const QString& = QString() );
    QVariant getValue(const QString& = QString() );
    QStringList getList(const QString& = QString() );
    QPoint getCenter(const QString&, const QString&);
    QStringList getLabels(const QString& = QString() );

    // clipboard
    QString getClipboardText();
    void setClipboardText(const QString&);

    // time management functions
    QDateTime getDateTime();

    int visibleResponseTime();
    void setVisibleResponseTime(int);

    // application management functions
    virtual void startApplication(const QString&, const QStringList& = QStringList(), int timeout = 5000,
                                  QSystemTest::StartApplicationFlags = QSystemTest::WaitForFocus);
    virtual void killApplication();
    void expectApplicationClose(bool);

    // low level simulators
    void keyClick(Qt::Key,const QString& = QString());
    void keyClickHold(Qt::Key,int,const QString& = QString());
    void keyPress(Qt::Key,const QString& = QString());
    void keyRelease(Qt::Key,const QString& = QString());

    void mouseClick(int,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseClick(const QPoint&,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseClick(const QString&,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseClickHold(int,int,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseClickHold(const QPoint&,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseClickHold(const QString&,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mousePress(int,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mousePress(const QPoint&,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mousePress(const QString&,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseRelease(int,int,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseRelease(const QPoint&,QFlags<Qt::MouseButton> = Qt::LeftButton);
    void mouseRelease(const QString&,QFlags<Qt::MouseButton> = Qt::LeftButton);

    bool mousePreferred();
    void setMousePreferred(bool);

    LabelOrientation labelOrientation();
    void setLabelOrientation(LabelOrientation);

    // input and selection functions
    void enter(const QVariant&,const QString& = QString(),EnterMode = Commit);
    void select(const QString&,const QString& = QString());
    void selectIndex(const QVariantList&,const QString& = QString());
    QVariantList getSelectedIndex(const QString& = QString());
    void activate(const QString&);
    void ensureVisible(const QString&,const QString& = QString());

    // verification mechanisms
    void verifyImage(const QString&,const QString& = QString(),
                        const QString& = QString(),const QStringList& = QStringList());
    bool compareImage( const QString&, const QString&, const QStringList& = QStringList());
    bool recordEvents(const QString&, bool = true);
    QString stopRecordingEvents();

    // testcase synchronisation
    void wait(int);

    // graphical manipulation
    void saveScreen(const QString&, const QString& = "");
    QImage grabImage(const QString& = QString(),const QStringList& = QStringList());

    // environment manipulation functions
    QString runProcess(const QString&,const QStringList&,const QString&);
    QString getenv(const QString&);
    bool checkOS(const QString&);
    QString targetIdentifier();
    void setTargetIdentifier(const QString &);
    void putFile(const QString&,const QString&,QFile::Permissions=0);
    void putData(const QByteArray&,const QString&,QFile::Permissions=0);
    void getFile(const QString&,const QString&);
    QString readLocalFile(const QString&);
    QString getData(const QString&);
    void deletePath(const QString&);
    QStringList getDirectoryEntries(const QString&,QDir::Filters = QDir::NoFilter);

    QSize getImageSize(const QString&);
    QRect getGeometry(const QString& = QString());

    void setProperty(const QString&,const QString&,const QVariant&);
    QVariant getProperty(const QString&,const QString&);
    QStringList findByProperty(const QString&, const QVariant&);
    QStringList findByProperty(const QVariantMap&);
    QStringList findWidget(const QString&, const QVariant&);
    QStringList findWidget(const QVariantMap&);

    bool invokeMethod(const QString&,const QString&,
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant());

    bool invokeMethod(const QString&,const QString&,Qt::ConnectionType,
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant(),const QVariant& = QVariant(),const QVariant& = QVariant(),
                        const QVariant& = QVariant());

    QVariant getSetting(const QString&,const QString&,const QString&);
    QVariant getSetting(const QString&,const QString&,const QString&,const QString&);
    void setSetting(const QString&,const QString&,const QString&,const QVariant&);
    void setSetting(const QString&,const QString&,const QString&,const QString&,const QVariant&);
    QString translate(const QString&, const QString&, const QString& = QString(), int = 0);
    QLocale getLocale();

    // debug helper functions
    QString activeWidgetInfo();

    void print(QVariant const&);

    void qtuitest_pre_test_function();
    void qtuitest_post_test_function();

    // These public slots come from QSystemTestPrivate
    QTestMessage query( const QTestMessage &message, const QString &queryPath = QString(),
                       int timeout = DEFAULT_QUERY_TIMEOUT );
    bool queryPassed( const QStringList &passResult, const QStringList &failResult,
                       const QTestMessage &message, const QString &queryPath = QString(),
                       QTestMessage *reply = 0, int timeout = DEFAULT_QUERY_TIMEOUT );
    bool queryPassed( const QString &passResult, const QString &failResult,
                       const QTestMessage &message, const QString &queryPath = QString(),
                       QTestMessage *reply = 0, int timeout = DEFAULT_QUERY_TIMEOUT );
    void recordPrompt();
    bool recentEventRecorded();
    void resetEventTimer();

public:
    void setConnectionParameters( Qt4Test::TestController::TestDeviceType &type,
                                 Core::SshConnectionParameters param );
    virtual void applicationStandardOutput(QList<QByteArray> const&);
    virtual void applicationStandardError(QList<QByteArray> const&);

protected slots:
    void abortPrompt();
    void abortTest();

protected:
    virtual void printUsage() const;
    virtual int runTest(const QString &fname, const QStringList &parameters,
                        const QStringList &environment);
    virtual void processCommandLine(QStringList&);    

    bool connectToAut(int timeout = 10000);
    void disconnectFromAut();
    bool isConnected() const;
    bool demoMode() const;
    QString autHost() const;
    void configTarget();

    int autPort() const;

    virtual QString currentFile();
    virtual int currentLine();

    QString configurationIdentifier() const;
    void setConfigurationIdentifier(QString const&);

    bool verbose() const;

#ifndef Q_QDOC
    virtual bool fail(QString const&);
    bool doQuery(const QTestMessage& message, const QString& queryPath = QString(), QTestMessage* reply = 0, int timeout = DEFAULT_QUERY_TIMEOUT, const QStringList& pass = QStringList("OK"), const QStringList& fail = QStringList());
    inline bool doQuery(const QTestMessage& message, const QString& queryPath, QTestMessage* reply, int timeout, const QString& pass, const QString& fail = QString())
    { return doQuery(message,queryPath,reply,timeout,QStringList(pass),QStringList(fail)); }

    template <typename T> inline
    T queryWithReturn(T const& ret, QString const& msg, QString const& queryPath)
    {
        QTestMessage reply;
        QTestMessage testMessage(msg);
        if (!doQuery(testMessage, queryPath, &reply)) return ret;
        if (!reply[msg].isValid()) {
            reply["status"] = QString("ERROR: no data in reply to %1; status: %2").arg(msg).arg(reply["status"].toString());
            setQueryError(reply);
            return ret;
        }
        return reply[msg].value<T>();
    }
#endif

    virtual bool setQueryError(const QTestMessage&);
    virtual bool setQueryError(const QString&);

    virtual bool queryFailed(QTestMessage* = 0,QTestMessage* = 0);
    virtual void enableQueryWarnings(bool,const QString& = QString(),int = -1);
    virtual void setLocation(const QString& = QString(),int = -1);

    virtual void processMessage(const QTestMessage&);
    
    bool abort_prompt;

public:
// public functions imported from QSystemTestPrivate
    void recordedEvent( const QTestMessage &msg );
    void recordEvents( QList<RecordEvent> const& );
    void parseKeyEventToCode( const QTestMessage &msg);

    QVariant getSetting(const QString&,const QString&,const QString&,const QString&,const QString&);
    void     setSetting(const QString&,const QString&,const QString&,const QString&,const QString&,const QVariant&);

    bool imagesAreEqual( const QImage &actual, const QImage &expected, bool strict = false );
    bool learnImage( const QImage &actual, const QImage &expected, const QString &comment = QString());

    QString processEnvironment( QString const& ) const;
    QStringList processEnvironment( QStringList const& ) const;
    QString PATH();
    QString which( const QString &appName );

signals:
    void appGainedFocus(QString const &appName);
    void appBecameIdle(QString const &appName);

protected:
    QStringList m_env;

private:
    friend class QSystemTestMaster;
    friend class QSystemTestPrivate;
    friend class QSystemTestMail;
    friend class TestProcess;

    QMap<QString, int> m_filteredMessages;
    QTime *event_timer;
    QTime key_hold_started;
    QSystemTestMaster *m_test_app;
    QTestMessage m_error_msg;
    QTestMessage m_error_msg_sent;
    QTestMessage m_last_msg_sent;

    QTimer *key_enter_timer;

    QPointer<QTextEdit> m_recorded_events_edit;

    bool recorded_events_as_code;
    bool record_prompt;
    bool query_failed;
    bool query_warning_mode;
    int fatal_timeouts;
    int timeouts;

    Qt::Key m_keyclickhold_key;
    QString m_keyclickhold_path;
    QString current_application;
    QString current_app_version;

    QString m_loc_fname;
    int m_loc_line;

    class ExpectedMessageBox {
    public:
        QString test_function;
        QString data_tag;
        QString title;
        QString text;
        QString option;
    };
    QList<ExpectedMessageBox*> expected_msg_boxes;

    bool m_auto_mode;
    bool m_run_as_manual_test;
    QString m_manual_commands;
    QStringList m_manual_command_data;
    QStringList alternative_command_data;
    void showPromptDialog(const QString& = QString());
    bool isConnected();

    // the following parameters are used to start an Application_Under_Test
    Qt4Test::TestController *device_controller;
    Core::SshConnectionParameters ssh_param;
    quint16 m_aut_port;
    bool m_keep_aut;
    bool m_silent_aut;
    bool m_no_aut;
    bool m_demo_mode;
    bool m_verbose_perf;
    bool m_verbose;
    QString m_targetID;
    QMap<QString, QString> appNameToBinary;
    bool m_strict_mode;
    int m_visible_response_time;
    QList<RecordEvent> recorded_events;
    QString m_recorded_code;
    int display_id;
    QString device;
    bool m_mousePreferred;
    QRect screenGeometry;
    QString theme;
    QString m_config_id;
    bool m_recording_events;
    bool m_expect_app_close;
    int m_query_count;
    bool m_skip_current_function;

#ifdef Q_QDOC
/* Functions where implementation is in QtScript */
public:
    void waitFor(Number = 10000,Number = 20,String = null);
    void expectMessageBox(String,String,String,Number);
    void compare(Variant,Variant);
    void verify(Boolean,String = null);
    void fail(String);
    void tabBar(int = 0);
#endif
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QSystemTest::StartApplicationFlags)

#ifdef Q_QDOC
typedef Nothing String;
typedef Nothing StringArray;
typedef Nothing QVariantArray;
typedef Nothing Boolean;
typedef Nothing Number;
typedef Nothing Array;
typedef Nothing Function;
#endif

#endif
