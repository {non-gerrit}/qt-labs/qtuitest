DEFINES += QSYSTEMTEST_TARGET

FORMS   +=\
        manualverificationdlg.ui \
        failuredlg.ui \
        recorddlg.ui

SEMI_PRIVATE_HEADERS += \
        qsystemtestmaster_p.h \
        qtestremote_p.h \
        qtestprotocol_p.h \
        qtestverifydlg_p.h

HEADERS +=\
        qabstracttest.h \
        qsystemtest.h \
        qtestide.h \
        testcontrol.h \
        maemotestcontrol.h \
        meegotestcontrol.h \
        symbiantestcontrol.h \
        desktoptestcontrol.h \
        customtestcontrol.h

SOURCES +=\
        qabstracttest.cpp \
        qtestide.cpp \
        qtestremote.cpp \
        qtestverifydlg.cpp \
        qsystemtest.cpp \
        qsystemtest_p.cpp \
        qsystemtestmaster.cpp \
        qtestprotocol.cpp \
        testcontrol.cpp \
        maemotestcontrol.cpp \
        meegotestcontrol.cpp \
        symbiantestcontrol.cpp \
        desktoptestcontrol.cpp \
        customtestcontrol.cpp

VPATH+=$$PWD
INCLUDEPATH+=$$PWD
INCLUDEPATH+=$$SRCROOT
INCLUDEPATH+=$$SRCROOT/botan/build
INCLUDEPATH+=$$SRCROOT/coreplugin
DEPENDPATH+=$$SRCROOT

TEMPLATE=lib
TARGET=qsystemtest
TARGET=$$qtLibraryTarget($$TARGET)
HEADERS*=$$SEMI_PRIVATE_HEADERS $$PRIVATE_HEADERS
CONFIG+=qtestlib
QT+=network script
LIBS+=-L$$BUILDROOT/lib -lBotan -lCore

unix:!symbian {
    DESTDIR=$$BUILDROOT/lib
    target.path=$$[QT_INSTALL_LIBS]
    INSTALLS+=target
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData
    LIBS+=-L$$OUT_PWD -lws32 -leuser -lcone
}

win32 {
    target.path=$$[QT_INSTALL_BINS]
    INSTALLS+=target
    !equals(QMAKE_CXX, "g++") {
        DEFINES+=strcasecmp=_stricmp
    }
}

mac {
    DEFINES+=sighandler_t=sig_t
}

