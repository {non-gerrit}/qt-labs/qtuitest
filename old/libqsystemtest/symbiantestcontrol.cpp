#include "symbiantestcontrol.h"

using namespace Qt4Test;

SymbianTestControl::SymbianTestControl()
    : TestControl()
{

}

SymbianTestControl::~SymbianTestControl()
{

}

bool SymbianTestControl::deviceConfiguration( QString &reply )
{
    return false;
}

bool SymbianTestControl::startApplication( const QString &application, const QStringList &arguments,
                        bool styleQtUITest, const QStringList &environment, QString &reply )
{
    return false;
}

bool SymbianTestControl::killApplication( const QString &application, QString &reply )
{
    return false;
}

