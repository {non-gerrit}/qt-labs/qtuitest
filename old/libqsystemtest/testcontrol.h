/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of Qt Creator.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTCONTROL_H
#define TESTCONTROL_H

#include <coreplugin/ssh/sshconnection.h>

#include <QtCore/QSharedPointer>
#include <QHash>
#include <QStringList>

namespace Qt4Test {

class TestControl : public QObject
{
    Q_OBJECT
public:
    TestControl();
    virtual ~TestControl();

    virtual bool deviceConfiguration( QString &reply ) = 0;
    virtual bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply ) = 0;
    virtual bool killApplication( const QString &application, QString &reply ) = 0;
};

/**
 * A class that can communicate with a device, using a secure connection.
 */
class TestController : public QObject
{
    Q_OBJECT
public:
    enum TestDeviceType {Desktop, Maemo, MeeGo, Custom, Symbian};
    TestController( TestDeviceType type, Core::SshConnectionParameters *sshParam = 0 );
    virtual ~TestController();

    enum TestControlMode {Idle, ConfigCheck, CustomCommand, CustomBlockingCommand};

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                          bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );
    void killApplications();

protected:
    Core::SshConnectionParameters ssh_param;
    TestDeviceType test_device_type;

    TestControl* controlFactory();

private:
    QList<TestControl*> device_controls;
    QStringList started_applications;
};


} // namespace Qt4Test

#endif // TESTCONTROL_H
