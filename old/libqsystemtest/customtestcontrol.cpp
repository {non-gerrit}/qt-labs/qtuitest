#include "customtestcontrol.h"

#include <QDebug>
#include <QProcess>
#include <QApplication>

using namespace Qt4Test;

CustomTestControl::CustomTestControl()
    : TestControl()
{

}

CustomTestControl::~CustomTestControl()
{

}

bool CustomTestControl::deviceConfiguration( QString &reply )
{
    return false;
}

bool CustomTestControl::startApplication( const QString &application, const QStringList &arguments,
                        bool styleQtUITest, const QStringList &environment, QString &reply )
{
    QProcess proc;
    QStringList args = arguments;
    args.prepend(application);
    static QByteArray startProcess = qgetenv("QTUITEST_START_PROCESS");
    if (startProcess.isEmpty()) {
        qDebug() << QString("Could not start remote process '%1', QTUITEST_START_PROCESS not defined.").arg(application);
        return false;
    }

    proc.start(startProcess, args);
    if (!proc.waitForFinished(10000)) {
        proc.kill();
        if (!proc.waitForFinished(5000))
            return false;
    }

    return true;
}

bool CustomTestControl::killApplication( const QString &application, QString &reply )
{
    // ensure that we stop any running remote applications
    QProcess proc;
    QStringList args;
    args << application;
    args << QString("-style=qtuitest");

    static QByteArray stopProcess = qgetenv("QTUITEST_STOP_PROCESS");
    if (stopProcess.isEmpty()) {
        qDebug() << QString("Could not stop remote process '%1', QTUITEST_STOP_PROCESS not defined.").arg(application);
        return false;
    }

    proc.start(stopProcess, args);
    if (!proc.waitForFinished(10000)) {
        proc.kill();
        if (!proc.waitForFinished(5000))
            return false;
    }
    return true;
}
