/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtestremote_p.h"

#include <stdlib.h> // used for exit

QTestRemote::QTestRemote()
{
    must_stop_event_recording = false;
    event_recording_aborted = false;
}

QTestRemote::~QTestRemote()
{
}

void QTestRemote::processMessage( QTestMessage *msg )
{
    if (msg->event() == "stopEventRecording") {
        must_stop_event_recording = true;
    } else if (msg->event() == "eventRecordingAborted") {
        must_stop_event_recording = true;
        event_recording_aborted = true;
    } else if (msg->event() == "abort") {
        emit abort();
    }
}

/*!
    \internal
    Opens a socket connection to a remote test tool and uses the connection to
    communicate with the connected tool.
*/
void QTestRemote::openRemote( const QString &ip, int port )
{
    connect( ip, port );
    if (!waitForConnected(5000)) {
        // it really doesn't make sense to continue testing if we cannot communicate with the remote tool so we abort immediately.
        exit( REMOTE_CONNECT_ERROR );
    }
}

