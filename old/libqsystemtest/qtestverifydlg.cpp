/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtestverifydlg_p.h"

#include <QLabel>
#include <QPushButton>
#include <QDesktopWidget>
#include <QLayout>
#include <QPixmap>
#include <QTabWidget>


#define WIDTH 364
#define HEIGHT 310

QTestVerifyDlg::QTestVerifyDlg( QWidget* parent )
    : QDialog( parent )
{

//    resize( WIDTH, HEIGHT );

    QVBoxLayout *mainLayout = new QVBoxLayout( this );
    mainLayout->setMargin(11);
    mainLayout->setSpacing(6);

    commentLabel = new QLabel();
    commentLabel->setAlignment( Qt::AlignCenter );
    mainLayout->addWidget( commentLabel );

    QHBoxLayout *pixLayout = new QHBoxLayout;
    pixLayout->setMargin(0);
    pixLayout->setSpacing(6);

    tabWidget = new QTabWidget();
    tabWidget->setTabPosition( QTabWidget::South );

    pixLayout->addItem( new QSpacerItem( 12, 12, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
    pixLayout->addWidget( tabWidget );
    pixLayout->addItem( new QSpacerItem( 12, 12, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
    mainLayout->addLayout( pixLayout );
    mainLayout->addItem( new QSpacerItem( 16, 16, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->setMargin(0);
    buttonLayout->setSpacing(6);
    okButton = new QPushButton();
    buttonLayout->addWidget( okButton );
    cancelButton = new QPushButton();
    buttonLayout->addWidget( cancelButton );
    buttonLayout->addItem( new QSpacerItem( 16, 16, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
    mainLayout->addLayout( buttonLayout );

    // signals and slots connections
    connect( okButton, SIGNAL(clicked()), this, SLOT(accept()), Qt::DirectConnection );
    connect( cancelButton, SIGNAL(clicked()), this, SLOT(reject()), Qt::DirectConnection );

    okButton->setText( "Yes" );
    cancelButton->setText( "No" );
    setWindowTitle( "Verify Pixmap" );
//    questionText = "Does the 'Actual' tab show the correct output?\n ";
//    questionLabel->setText(questionText);
}

/*
    Destroys the object and frees any allocated resources
*/
QTestVerifyDlg::~QTestVerifyDlg()
{
    //hide();
}

void QTestVerifyDlg::setComment( const QString &text )
{
    commentLabel->setText(text);
}

void QTestVerifyDlg::addTab( const QString &name, const QPixmap &img )
{
    QLabel *label = new QLabel();
    label->setObjectName( name + "Label" );

    QSizePolicy sizePolicy = QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());

    label->setSizePolicy( sizePolicy );
    label->setFrameShape( QLabel::Box );
    label->setFrameShadow( QLabel::Sunken );
    label->setScaledContents( false );
    label->setAlignment( Qt::AlignCenter );

    if (!img.isNull()) {
        label->setPixmap(img);
    } else {
        label->setText("No image available");
    }

    tabWidget->addTab( label, name );
    updateGeometry();
}
/*
void QTestVerifyDlg::setData( const QPixmap &actual, const QPixmap &expected,
                                const QPixmap &diff, const QString &comment)
{
    questionLabel->setText(questionText + comment);
    if (!actual.isNull())
        actualLabel->setPixmap(actual);

    bool expectedValid = true;
    if (!expected.isNull()) {
        expectedLabel->setPixmap(expected);
        if (!diff.isNull()) {
            diffLabel->setPixmap(diff);
        }
    }
    else {
        QString S = "No expected pixmap available";
        expectedLabel->setText( S );
        QSize s1 = actualLabel->sizeHint();
        QSize s2 = expectedLabel->sizeHint();
        while (s1.width() > s2.width()) {
            S = " " + S + " ";
            expectedLabel->setText( S );
            s1 = actualLabel->sizeHint();
            s2 = expectedLabel->sizeHint();
        }
        S+="\n\nIf you click on 'Yes' the LEFT pixmap\nwill become the expected pixmap\nand will be used for future comparisons.";
        expectedLabel->setText( S );

        expectedValid = false;
    }

    updateGeometry();
}
*/