/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtestide.h"
#include "qsystemtest.h"
#include <QScriptContext>
#include <QScriptContextInfo>
#include <QScriptValueIterator>
#include <QPixmap>

QTestIDE* QTestIDE::instance()
{
    static QTestIDE instance;
    return &instance;
}

QTestIDE::QTestIDE()
    : m_sysTest(0)
{
    must_stop_event_recording = false;
    event_recording_aborted = false;
}

QTestIDE::~QTestIDE()
{
}

void QTestIDE::setSystemTest(QSystemTest *sysTest)
{
    m_sysTest = sysTest;
}

/*!
    \internal
    Opens a socket connection to a remote test tool and uses the connection to
    communicate with the connected tool.
*/
void QTestIDE::openRemote( const QString &ip, int port )
{
    m_remote.openRemote(ip, port);
}

bool QTestIDE::isConnected()
{
    return m_remote.isConnected();
}

void QTestIDE::scriptSyntaxError(const QString& errorMsg, const QString& file, int line)
{
    if (errorMsg.isEmpty()) {
        qWarning(QString("Syntax Error in %1, near line %2").arg(file).arg(line).toLatin1());
    } else {
        qWarning(QString("Syntax Error in %1, near line %2: %3").arg(file).arg(line).arg(errorMsg).toLatin1());
    }
}

bool QTestIDE::queryBreakpoint(const QString& file, int line, const QString& function, int depth)
{
    if (m_remote.isConnected()) {
      QTestMessage ide_msg("queryBreakpoint");
      ide_msg["file"] = file;
      ide_msg["line"] = line;
      ide_msg["function"] = function;
      ide_msg["depth"] = depth;
      QTestMessage reply;
      m_remote.sendMessage( ide_msg, reply, -1 );
      return (reply["status"] == "sendContext");
    }
    return false;
}

void QTestIDE::breakpointContext(const QScriptContext* scriptContext)
{
    QTestMessage ide_msg("context");
    int i=0;
    for (const QScriptContext *c = scriptContext;
            c && c->parentContext();
            c = c->parentContext(), ++i) {
        QVariantMap contextMap;
        contextToMap(c, contextMap);
        ide_msg[QString("context_%1").arg(i)] = contextMap;
    }
    ide_msg["context_count"] = i;
    QTestMessage reply;
    m_remote.sendMessage( ide_msg, reply, -1 );
}

void QTestIDE::eventRecordingStarted(const QString& file, int line, const QString& steps)
{
    if (m_remote.isConnected()) {
        QTestMessage message("EVENT_RECORDING_STARTED");
        message["file"] = file;
        message["line"] = line;
        message["steps"] = steps;
        m_remote.postMessage( message );

        m_remote.must_stop_event_recording = false;
        m_remote.event_recording_aborted = false;
    }
}

void QTestIDE::recordedCode(const QString &code)
{
    if (m_remote.isConnected()) {
        QTestMessage ide_msg("recordedCode");
        ide_msg["code"] = code;
        m_remote.postMessage( ide_msg );
    }
}

bool QTestIDE::mustStopEventRecording() {
    return m_remote.must_stop_event_recording;
}

bool QTestIDE::eventRecordingAborted() {
    return m_remote.event_recording_aborted;
}

void QTestIDE::failureScreenshot(const QString &screenshot, const QString &file, int line, const QString &function)
{
    if (m_remote.isConnected()) {
        QTestMessage msg("failure_screenshot");
        msg["screenshot"]   = screenshot;
        msg["filename"]     = file;
        msg["line"]         = line;
        msg["testfunction"] = function;
        m_remote.postMessage(msg);
    }
}

void QTestIDE::newTestData(const QString &file)
{
    if (m_remote.isConnected()) {
        QTestMessage msg("NEW_TESTDATA");
        msg["filename"] = file;
        m_remote.postMessage(msg);
    }
}

bool QTestIDE::verifyImage(const QPixmap &actual, const QPixmap &expected, const QString &comment)
{
    QTestMessage returnValue;
    QTestMessage msg("VERIFY_IMAGE");
    msg["actual"] = actual;
    msg["expected"] = expected;
    msg["comment"] = comment;

    if (m_remote.sendMessage( msg, returnValue, -1 ) && returnValue["status"] == "OK") {
        return returnValue["accepted"].toBool();
    } else {
        return false;
//        return fail( "Verification of pixmap not possible: remote test tool did not correctly respond." );
    }
}

void QTestIDE::contextToMap(const QScriptContext* scriptContext, QVariantMap &contextMap)
{
    QVariantMap vars;
    QScriptContextInfo ctxInfo(scriptContext);
    
    QScriptValue locals = scriptContext->activationObject();
    if (!locals.isValid()) return;
    QScriptValueIterator it(locals);
    while (it.hasNext()) {
        it.next();
        if (!it.value().toVariant().isValid()) {
          continue;
        }
        vars[it.name()] = it.value().toVariant();
    }

    contextMap["vars"] = vars;
    contextMap["file"] = ctxInfo.fileName();                
    contextMap["line"] = ctxInfo.lineNumber();

    if (ctxInfo.functionName().isEmpty()) {
        contextMap["function"] = m_sysTest ? m_sysTest->currentTestFunction() : QString("unknown");
    } else {
        contextMap["function"] = ctxInfo.functionName();
        contextMap["funcStart"] = ctxInfo.functionStartLineNumber();
        contextMap["funcEnd"] = ctxInfo.functionEndLineNumber();
    }
}