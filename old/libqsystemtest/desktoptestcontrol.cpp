#include "desktoptestcontrol.h"

#include <QDebug>
#include <QProcess>


using namespace Qt4Test;

DesktopTestControl* DesktopTestControl::instance()
{
    static DesktopTestControl instance;
    return &instance;
}

DesktopTestControl::DesktopTestControl()
    : TestControl()
{
    proc = 0;
}

DesktopTestControl::~DesktopTestControl()
{

}

bool DesktopTestControl::deviceConfiguration( QString &reply )
{
    return false;
}

bool DesktopTestControl::startApplication( const QString &application, const QStringList &arguments,
                        bool styleQtUITest, const QStringList &environment, QString &reply )
{
    proc = new Qt4Test::TestProcess(this);

    proc->setEnvironment(environment);
// FIXME    proc->test = this;
    QStringList args = arguments;
    if (styleQtUITest) {
        args << "-style";
        args << "qtuitest";
    }

    QByteArray defArgs = qgetenv("QTUITEST_DEFAULT_AUT_ARGS");
    if (defArgs.length()) {
        QList<QByteArray> defaultArgs = defArgs.split(' ');
        foreach (QByteArray arg, defaultArgs) {
            args << arg;
        }
    }

    proc->start(application, args);
    if (!proc->waitForStarted()) {
        reply = QString("Failed to start process '%1': %2").arg(application).arg(proc->errorString());
        delete proc;
        return false;
    }
    return true;
}

bool DesktopTestControl::killApplication( const QString &application, QString &reply )
{
    if (!proc)
        return false;

    proc->terminate();
    if (!proc->waitForFinished(5000)) {
        proc->kill();
        proc->waitForFinished(5000);
    }
    return true;
}

