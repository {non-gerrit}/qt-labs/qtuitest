#include "meegotestcontrol.h"

using namespace Qt4Test;

MeeGoTestControl::MeeGoTestControl()
    : TestControl()
{

}

MeeGoTestControl::~MeeGoTestControl()
{

}

bool MeeGoTestControl::deviceConfiguration( QString &reply )
{
    return false;
}

bool MeeGoTestControl::startApplication( const QString &application, const QStringList &arguments,
                        bool styleQtUITest, const QStringList &environment, QString &reply )
{
    return false;
}

bool MeeGoTestControl::killApplication( const QString &application, QString &reply )
{
    return false;
}

