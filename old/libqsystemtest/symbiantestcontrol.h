#ifndef SYMBIANTESTCONTROL_H
#define SYMBIANTESTCONTROL_H

#include "testcontrol.h"

namespace Qt4Test {
class SymbianTestControl : public TestControl
{
    Q_OBJECT
public:
    SymbianTestControl();
    virtual ~SymbianTestControl();

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );
};
}

#endif // SYMBIANTESTCONTROL_H
