/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qsystemtestmaster_p.h"

#include <qsystemtest.h>

#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QIODevice>

// **************************************************************************************
// **************************************************************************************

QSystemTestMaster::QSystemTestMaster( QSystemTest *testCase )
    : QTestProtocol()
    , app_name()
    , app_version()
    , qt_version()
    , test_case(testCase)
{
}

QSystemTestMaster::~QSystemTestMaster()
{
}

void QSystemTestMaster::queryName()
{
    QTestMessage reply;
    sendMessage(QTestMessage("appName"), reply, 40000);
    app_name = reply["appName"].toString();
    app_version = reply["appVersion"].toString();
    qt_version = reply["qtVersion"].toString();
}

void QSystemTestMaster::processMessage( QTestMessage *msg )
{
    if (msg->event() == "APP_NAME") {
        app_name = (*msg)["appName"].toString();
        app_version = (*msg)["appVersion"].toString();
        qt_version = (*msg)["qtVersion"].toString();
    } else {
        // Leave all processing to the testcase
        if (test_case != 0) {
            test_case->processMessage( *msg );
        }
    }
}

QString QSystemTestMaster::appName()
{
    return app_name;
}

QString QSystemTestMaster::appVersion()
{
    return app_version;
}

QString QSystemTestMaster::qtVersion()
{
    return qt_version;
}

void QSystemTestMaster::onReplyReceived( QTestMessage * /*reply*/ )
{
}

