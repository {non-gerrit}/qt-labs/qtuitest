/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTESTIDE_H
#define QTESTIDE_H

#include "qtestremote_p.h"
#include <QVariantMap>

class QSystemTest;
class QString;
class QScriptContext;

class QSYSTEMTEST_EXPORT QTestIDE : public QObject
{
    Q_OBJECT

public:
    static QTestIDE* instance();
    
    QTestIDE();
    virtual ~QTestIDE();
    void setSystemTest( QSystemTest* );
    
    void openRemote( const QString &ip, int port );
    bool isConnected();
//    virtual void processMessage( QTestMessage *msg );

    void scriptSyntaxError(const QString&, const QString&, int);
    bool queryBreakpoint(const QString&, int, const QString&, int);
    void breakpointContext(const QScriptContext*);
    
    void eventRecordingStarted(const QString&, int, const QString&);
    void recordedCode(const QString&);
    bool mustStopEventRecording();
    bool eventRecordingAborted();
    
    void failureScreenshot(const QString&, const QString&, int, const QString&);
    void newTestData(const QString&);
    bool verifyImage(const QPixmap &, const QPixmap&, const QString&);

signals:
    void abort();
    void syntaxError(const QString&, const QString&, int);
    
private:
    void contextToMap(const QScriptContext* scriptContext, QVariantMap&);
    bool must_stop_event_recording;
    bool event_recording_aborted;
    QTestRemote m_remote;
    QSystemTest *m_sysTest;

};

#endif
