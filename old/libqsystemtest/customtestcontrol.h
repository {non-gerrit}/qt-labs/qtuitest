#ifndef CUSTOMTESTCONTROL_H
#define CUSTOMTESTCONTROL_H

#include "testcontrol.h"

namespace Qt4Test {
class CustomTestControl : public TestControl
{
    Q_OBJECT
public:
    CustomTestControl();
    virtual ~CustomTestControl();

    bool deviceConfiguration( QString &reply );

    bool startApplication( const QString &application, const QStringList &arguments,
                            bool styleQtUITest, const QStringList &environment, QString &reply );
    bool killApplication( const QString &application, QString &reply );
};

}

#endif // CUSTOMTESTCONTROL_H
