/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include <QtScript/QScriptExtensionPlugin>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>
#include <QtCore/QDebug>

#include <qtextboundaryfinder.h>
#include <qxmlstream.h>
#include <qbytearraymatcher.h>
#include <qnamespace.h>
#include <qpoint.h>
#include <qdatastream.h>
#include <qrect.h>
#include <qsize.h>
#include <qrunnable.h>
#include <qmutex.h>
#include <qxmlstream.h>
#include <qxmlstream.h>
#include <qlibraryinfo.h>
#include <qpoint.h>
#include <qurl.h>
#include <qtextcodec.h>
#include <qstringmatcher.h>
#include <qxmlstream.h>
#include <qcoreevent.h>
#include <qdiriterator.h>
#include <qxmlstream.h>
#include <qabstractitemmodel.h>
#include <qobject.h>
#include <qwaitcondition.h>
#include <qxmlstream.h>
#include <qsystemsemaphore.h>
#include <qdatetime.h>
#include <qlocale.h>
#include <qrect.h>
#include <qxmlstream.h>
#include <qbasictimer.h>
#include <qtextcodec.h>
#include <qcryptographichash.h>
#include <qabstractitemmodel.h>
#include <quuid.h>
#include <qdir.h>
#include <qbitarray.h>
#include <qtextcodec.h>
#include <qsize.h>
#include <qtextstream.h>
#include <qbytearray.h>
#include <qsemaphore.h>
#include <qfileinfo.h>
#include <qxmlstream.h>
#include <qsocketnotifier.h>
#include <qcoreapplication.h>
#include <qeventloop.h>
#include <qcoreevent.h>
#include <qsettings.h>
#include <qcoreevent.h>
#include <qiodevice.h>
#include <qabstractitemmodel.h>
#include <qtimer.h>
#include <qsignalmapper.h>
#include <qmimedata.h>
#include <qcoreevent.h>
#include <qtextcodecplugin.h>
#include <qthreadpool.h>
#include <qtranslator.h>
#include <qfilesystemwatcher.h>
#include <qtimeline.h>
#include <qabstractitemmodel.h>
#include <qprocess.h>
#include <qbuffer.h>
#include <qfile.h>
#include <qabstractitemmodel.h>
#include <qtemporaryfile.h>

QScriptValue qtscript_create_QTextBoundaryFinder_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamEntityDeclaration_class(QScriptEngine *engine);
QScriptValue qtscript_create_QByteArrayMatcher_class(QScriptEngine *engine);
QScriptValue qtscript_create_Qt_class(QScriptEngine *engine);
QScriptValue qtscript_create_QPointF_class(QScriptEngine *engine);
QScriptValue qtscript_create_QDataStream_class(QScriptEngine *engine);
QScriptValue qtscript_create_QRectF_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSize_class(QScriptEngine *engine);
QScriptValue qtscript_create_QRunnable_class(QScriptEngine *engine);
QScriptValue qtscript_create_QMutex_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamReader_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamWriter_class(QScriptEngine *engine);
QScriptValue qtscript_create_QLibraryInfo_class(QScriptEngine *engine);
QScriptValue qtscript_create_QPoint_class(QScriptEngine *engine);
QScriptValue qtscript_create_QUrl_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTextDecoder_class(QScriptEngine *engine);
QScriptValue qtscript_create_QStringMatcher_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamNamespaceDeclaration_class(QScriptEngine *engine);
QScriptValue qtscript_create_QEvent_class(QScriptEngine *engine);
QScriptValue qtscript_create_QDirIterator_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamEntityResolver_class(QScriptEngine *engine);
QScriptValue qtscript_create_QPersistentModelIndex_class(QScriptEngine *engine);
QScriptValue qtscript_create_QObject_class(QScriptEngine *engine);
QScriptValue qtscript_create_QWaitCondition_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamNotationDeclaration_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSystemSemaphore_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTime_class(QScriptEngine *engine);
QScriptValue qtscript_create_QLocale_class(QScriptEngine *engine);
QScriptValue qtscript_create_QRect_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamAttribute_class(QScriptEngine *engine);
QScriptValue qtscript_create_QBasicTimer_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTextCodec_class(QScriptEngine *engine);
QScriptValue qtscript_create_QCryptographicHash_class(QScriptEngine *engine);
QScriptValue qtscript_create_QModelIndex_class(QScriptEngine *engine);
QScriptValue qtscript_create_QUuid_class(QScriptEngine *engine);
QScriptValue qtscript_create_QDir_class(QScriptEngine *engine);
QScriptValue qtscript_create_QBitArray_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTextEncoder_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSizeF_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTextStream_class(QScriptEngine *engine);
QScriptValue qtscript_create_QByteArray_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSemaphore_class(QScriptEngine *engine);
QScriptValue qtscript_create_QFileInfo_class(QScriptEngine *engine);
QScriptValue qtscript_create_QXmlStreamAttributes_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSocketNotifier_class(QScriptEngine *engine);
QScriptValue qtscript_create_QCoreApplication_class(QScriptEngine *engine);
QScriptValue qtscript_create_QEventLoop_class(QScriptEngine *engine);
QScriptValue qtscript_create_QChildEvent_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSettings_class(QScriptEngine *engine);
QScriptValue qtscript_create_QDynamicPropertyChangeEvent_class(QScriptEngine *engine);
QScriptValue qtscript_create_QIODevice_class(QScriptEngine *engine);
QScriptValue qtscript_create_QAbstractItemModel_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTimer_class(QScriptEngine *engine);
QScriptValue qtscript_create_QSignalMapper_class(QScriptEngine *engine);
QScriptValue qtscript_create_QMimeData_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTimerEvent_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTextCodecPlugin_class(QScriptEngine *engine);
QScriptValue qtscript_create_QThreadPool_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTranslator_class(QScriptEngine *engine);
QScriptValue qtscript_create_QFileSystemWatcher_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTimeLine_class(QScriptEngine *engine);
QScriptValue qtscript_create_QAbstractTableModel_class(QScriptEngine *engine);
QScriptValue qtscript_create_QProcess_class(QScriptEngine *engine);
QScriptValue qtscript_create_QBuffer_class(QScriptEngine *engine);
QScriptValue qtscript_create_QFile_class(QScriptEngine *engine);
QScriptValue qtscript_create_QAbstractListModel_class(QScriptEngine *engine);
QScriptValue qtscript_create_QTemporaryFile_class(QScriptEngine *engine);

static const char * const qtscript_com_nokia_qt_core_class_names[] = {
    "QTextBoundaryFinder"
    , "QXmlStreamEntityDeclaration"
    , "QByteArrayMatcher"
    , "Qt"
    , "QPointF"
    , "QDataStream"
    , "QRectF"
    , "QSize"
    , "QRunnable"
    , "QMutex"
    , "QXmlStreamReader"
    , "QXmlStreamWriter"
    , "QLibraryInfo"
    , "QPoint"
    , "QUrl"
    , "QTextDecoder"
    , "QStringMatcher"
    , "QXmlStreamNamespaceDeclaration"
    , "QEvent"
    , "QDirIterator"
    , "QXmlStreamEntityResolver"
    , "QPersistentModelIndex"
    , "QObject"
    , "QWaitCondition"
    , "QXmlStreamNotationDeclaration"
    , "QSystemSemaphore"
    , "QTime"
    , "QLocale"
    , "QRect"
    , "QXmlStreamAttribute"
    , "QBasicTimer"
    , "QTextCodec"
    , "QCryptographicHash"
    , "QModelIndex"
    , "QUuid"
    , "QDir"
    , "QBitArray"
    , "QTextEncoder"
    , "QSizeF"
    , "QTextStream"
    , "QByteArray"
    , "QSemaphore"
    , "QFileInfo"
    , "QXmlStreamAttributes"
    , "QSocketNotifier"
    , "QCoreApplication"
    , "QEventLoop"
    , "QChildEvent"
    , "QSettings"
    , "QDynamicPropertyChangeEvent"
    , "QIODevice"
    , "QAbstractItemModel"
    , "QTimer"
    , "QSignalMapper"
    , "QMimeData"
    , "QTimerEvent"
    , "QTextCodecPlugin"
    , "QThreadPool"
    , "QTranslator"
    , "QFileSystemWatcher"
    , "QTimeLine"
    , "QAbstractTableModel"
    , "QProcess"
    , "QBuffer"
    , "QFile"
    , "QAbstractListModel"
    , "QTemporaryFile"

};

typedef QScriptValue (*QtBindingCreator)(QScriptEngine *engine);
static const QtBindingCreator qtscript_com_nokia_qt_core_class_functions[] = {
    qtscript_create_QTextBoundaryFinder_class
    , qtscript_create_QXmlStreamEntityDeclaration_class
    , qtscript_create_QByteArrayMatcher_class
    , qtscript_create_Qt_class
    , qtscript_create_QPointF_class
    , qtscript_create_QDataStream_class
    , qtscript_create_QRectF_class
    , qtscript_create_QSize_class
    , qtscript_create_QRunnable_class
    , qtscript_create_QMutex_class
    , qtscript_create_QXmlStreamReader_class
    , qtscript_create_QXmlStreamWriter_class
    , qtscript_create_QLibraryInfo_class
    , qtscript_create_QPoint_class
    , qtscript_create_QUrl_class
    , qtscript_create_QTextDecoder_class
    , qtscript_create_QStringMatcher_class
    , qtscript_create_QXmlStreamNamespaceDeclaration_class
    , qtscript_create_QEvent_class
    , qtscript_create_QDirIterator_class
    , qtscript_create_QXmlStreamEntityResolver_class
    , qtscript_create_QPersistentModelIndex_class
    , qtscript_create_QObject_class
    , qtscript_create_QWaitCondition_class
    , qtscript_create_QXmlStreamNotationDeclaration_class
    , qtscript_create_QSystemSemaphore_class
    , qtscript_create_QTime_class
    , qtscript_create_QLocale_class
    , qtscript_create_QRect_class
    , qtscript_create_QXmlStreamAttribute_class
    , qtscript_create_QBasicTimer_class
    , qtscript_create_QTextCodec_class
    , qtscript_create_QCryptographicHash_class
    , qtscript_create_QModelIndex_class
    , qtscript_create_QUuid_class
    , qtscript_create_QDir_class
    , qtscript_create_QBitArray_class
    , qtscript_create_QTextEncoder_class
    , qtscript_create_QSizeF_class
    , qtscript_create_QTextStream_class
    , qtscript_create_QByteArray_class
    , qtscript_create_QSemaphore_class
    , qtscript_create_QFileInfo_class
    , qtscript_create_QXmlStreamAttributes_class
    , qtscript_create_QSocketNotifier_class
    , qtscript_create_QCoreApplication_class
    , qtscript_create_QEventLoop_class
    , qtscript_create_QChildEvent_class
    , qtscript_create_QSettings_class
    , qtscript_create_QDynamicPropertyChangeEvent_class
    , qtscript_create_QIODevice_class
    , qtscript_create_QAbstractItemModel_class
    , qtscript_create_QTimer_class
    , qtscript_create_QSignalMapper_class
    , qtscript_create_QMimeData_class
    , qtscript_create_QTimerEvent_class
    , qtscript_create_QTextCodecPlugin_class
    , qtscript_create_QThreadPool_class
    , qtscript_create_QTranslator_class
    , qtscript_create_QFileSystemWatcher_class
    , qtscript_create_QTimeLine_class
    , qtscript_create_QAbstractTableModel_class
    , qtscript_create_QProcess_class
    , qtscript_create_QBuffer_class
    , qtscript_create_QFile_class
    , qtscript_create_QAbstractListModel_class
    , qtscript_create_QTemporaryFile_class
};

class com_nokia_qt_core_ScriptPlugin : public QScriptExtensionPlugin
{
public:
    QStringList keys() const;
    void initialize(const QString &key, QScriptEngine *engine);
};

QStringList com_nokia_qt_core_ScriptPlugin::keys() const
{
    QStringList list;
    list << QLatin1String("qt");
    list << QLatin1String("qt.core");
    return list;
}

void com_nokia_qt_core_ScriptPlugin::initialize(const QString &key, QScriptEngine *engine)
{
    if (key == QLatin1String("qt")) {
    } else if (key == QLatin1String("qt.core")) {
        QScriptValue extensionObject = engine->globalObject();
        for (int i = 0; i < 67; ++i) {
            extensionObject.setProperty(qtscript_com_nokia_qt_core_class_names[i],
                qtscript_com_nokia_qt_core_class_functions[i](engine),
                QScriptValue::SkipInEnumeration);
        }
    } else {
        Q_ASSERT_X(false, "com_nokia_qt_core::initialize", qPrintable(key));
    }
}
Q_EXPORT_STATIC_PLUGIN(com_nokia_qt_core_ScriptPlugin)
Q_EXPORT_PLUGIN2(qtscript_com_nokia_qt_core, com_nokia_qt_core_ScriptPlugin)

