/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testabstractbutton.h"
#include "testwidgetslog.h"

#include <QAbstractButton>
#include <QTimer>
#include <QTime>
#include <QPointer>
#include <QStyleOptionButton>

namespace QtUiTest {

TestAbstractButton::TestAbstractButton(QObject *_q)
    : TestGenericTextWidget(_q), q(qobject_cast<QAbstractButton*>(_q)),
      m_pressed(false)
{
    if (!QtUiTest::connectFirst(q, SIGNAL(pressed()), this, SIGNAL(activated())))
        Q_ASSERT(false);
    if (!connect(q, SIGNAL(toggled(bool)), this, SLOT(on_toggled(bool))))
        Q_ASSERT(false);
}

void TestAbstractButton::on_toggled(bool state)
{ emit stateChanged(state); }

Qt::CheckState TestAbstractButton::checkState() const
{ return q->isChecked() ? Qt::Checked : Qt::Unchecked; }

bool TestAbstractButton::setCheckState(Qt::CheckState state)
{
    if (state == checkState()) return true;
    if (!q->isCheckable()) {
        QtUiTest::setErrorString("This abstract button is not checkable.");
        return false;
    }
    bool ret = activate(QtUiTest::NoOptions);
    TestWidgetsLog() << "activated:" << ret;
    if (ret && (state != checkState()) && !QtUiTest::waitForSignal(q, SIGNAL(toggled(bool)))) {
        QtUiTest::setErrorString("Successfully activated button, but check state did not change "
                "to expected value.");
        return false;
    }
    TestWidgetsLog() << "state:" << ret;
    return ret;
}

bool TestAbstractButton::activate()
{
    return activate((QtUiTest::InputOption)0);
}

bool TestAbstractButton::activate(QtUiTest::InputOption opt)
{
    if (QtUiTest::mousePreferred()) {
        int t;
        m_pressed = false;

        QPoint pos;
        if (q->isCheckable()) {
            QStyle const* style = q->style();
            QStyleOptionButton opt;
            opt.initFrom(q);
            QRect rect = style->subElementRect( QStyle::SE_CheckBoxIndicator, &opt, q);
            pos = rect.center();
        } else {
            pos = q->rect().center();
        }

        QRegion region = QRegion(pos.x()-1, pos.y()-1, 3, 3);
        for (t = 0; t < 2; ++t) {
            ensureVisibleRegion(region);
            QtUiTest::mousePress(mapToGlobal(pos), Qt::LeftButton, opt);
            QtUiTest::waitForSignal(q, SIGNAL(pressed()));
            if (q->isDown())
                break;
        }
        if (!q->isDown()) {
            qWarning("Button did not react to mouse press.");
            QtUiTest::mouseRelease(mapToGlobal(pos), Qt::LeftButton, opt);
            return false;
        }
        if (t > 0)
            qWarning("Button did not react to mouse press on first try.");

        QtUiTest::mouseRelease(mapToGlobal(pos), Qt::LeftButton, opt);
        return true;
    } else {
        Qt::Key key = Qt::Key(QtUiTest::Key_ActivateButton);
        Qt::KeyboardModifiers mod = 0;

        if (q->focusPolicy() == Qt::NoFocus) {
            /* Handle buttons which need to be activated by a shortcut */
            QKeySequence ks = q->shortcut();
            TestWidgetsLog() << "Can't give focus to button; need to use shortcut"
                    << ks.toString();
            if (ks.isEmpty()) {
                QtUiTest::setErrorString("Button has NoFocus policy set and does not appear "
                        "to have any shortcut set.  Therefore, it is impossible to activate "
                        "using only the keyboard.");
                return false;
            }

            int key_and_mod = ks[0];
            mod = QFlag(key_and_mod & (0xfe000000));
            key = Qt::Key(key_and_mod & (0x01ffffff));
        }
        else {
            if (!hasFocus()) setFocus();
            if (!hasFocus()) return false;
        }

        QPointer<QObject> safeQ(q);

        if (!QtUiTest::keyClick(q, SIGNAL(pressed()), key, mod, opt))
            return false;

        if (safeQ && q->isDown() && !QtUiTest::waitForSignal(q, SIGNAL(released()))) {
            QtUiTest::setErrorString("Button did not become released after key click");
            return false;
        }
        return true;
    }
    return false;
}

QString TestAbstractButton::labelText() const
{
    QString ret = text();
    if (ret.isEmpty())
        return ret;

    QObject *w = parent();
    while (w) {
        QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
        QtUiTest::LabelWidget *lw = qtuitest_cast<QtUiTest::LabelWidget*>(w);
        if (lw) {
            if (!lw->labelText().isEmpty()) {
                ret.prepend(lw->labelText() + "/");
                break;
            }
        }
        w = qw->parent();
    }
    return ret;
}

QObject* TestAbstractButton::buddy() const
{
    return q;
}

bool TestAbstractButton::canWrap(QObject *o)
{ return qobject_cast<QAbstractButton*>(o); }

}