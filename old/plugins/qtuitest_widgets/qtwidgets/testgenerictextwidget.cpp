/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testgenerictextwidget.h"
#include "testwidgetslog.h"

#include <QWidget>
#include <QVariant>

namespace QtUiTest {

QList<QByteArray> TestGenericTextWidget::textProperties = QList<QByteArray>()
    << "text"
    << "plainText"
    << "value"
    << "title"
;
QList<QByteArray> TestGenericTextWidget::selectedTextProperties = QList<QByteArray>()
    << "selectedText"
;

TestGenericTextWidget::TestGenericTextWidget(QObject* _q)
    : TestWidget(_q), q(qobject_cast<QWidget*>(_q))
{
    TestWidgetsLog() << _q;
    foreach (QByteArray property, textProperties) {
        if (-1 != q->metaObject()->indexOfProperty(property)) {
            textProperty = property;
            break;
        }
    }
    foreach (QByteArray property, selectedTextProperties) {
        if (-1 != q->metaObject()->indexOfProperty(property)) {
            selectedTextProperty = property;
            break;
        }
    }
}

QString TestGenericTextWidget::text() const
{ TestWidgetsLog(); return q->property(textProperty).toString(); }

QString TestGenericTextWidget::selectedText() const
{
    TestWidgetsLog();
    QString ret;
    if (!selectedTextProperty.isEmpty())
        ret = q->property(selectedTextProperty).toString();
    if (ret.isEmpty())
        ret = text();
    return ret;
}

bool TestGenericTextWidget::canWrap(QObject *o)
{
    QWidget *w;
    if (!(w = qobject_cast<QWidget*>(o)))
        return false;

    foreach (QByteArray property, textProperties)
        if (-1 != o->metaObject()->indexOfProperty(property))
            return true;

    return false;
}

}