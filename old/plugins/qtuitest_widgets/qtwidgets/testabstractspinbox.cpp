/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testabstractspinbox.h"
#include "testwidgetslog.h"

#include <QPointer>
#include <QSpinBox>
#include <QValidator>
#include <QVariant>

namespace QtUiTest {

TestAbstractSpinBox::TestAbstractSpinBox(QObject *_q)
    : TestGenericTextWidget(_q), q(qobject_cast<QAbstractSpinBox*>(_q))
{
//TestWidgetsLog();
}

bool TestAbstractSpinBox::canEnter(QVariant const& item) const
{
    if (!item.canConvert<QString>()) return false;

    int dontcare = 0;
    QString text = item.toString();
    return (QValidator::Acceptable==q->validate(text, dontcare));
}

bool TestAbstractSpinBox::enter(QVariant const& item, bool noCommit)
{
    if (!canEnter(item)) return false;
    if (!hasFocus() && !setFocus()) return false;

    using namespace QtUiTest;

    QPointer<QObject> safeThis = this;
    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    InputWidget* iw = qtuitest_cast<InputWidget*>(inputProxy());
    TestWidgetsLog() << iw;

    //FIXME: This shouldn't be necessary
    bool kt = q->keyboardTracking();
    q->setKeyboardTracking(true);

    //FIXME: Doesn't use mouse events...
    bool ret = true;

    /* Use input method if available... */
    if (iw) {
        ret = iw->canEnter(item) && iw->enter(item, noCommit);
    } else {
        //FIXME: Should clear existing text using keyClicks, not API
        q->clear();
        /* ...otherwise, generate key clicks ourself */
        foreach (QChar c, item.toString()) {
            TestWidgetsLog() << asciiToModifiers(c.toLatin1());
            keyClick( asciiToKey(c.toLatin1()), asciiToModifiers(c.toLatin1()) );
        }
    }

    waitForSignal(q, SIGNAL(valueChanged(int)));
    q->setKeyboardTracking(kt);

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    if (mousePreferred()) {
        return ret;
    }

    if (!noCommit) {
        keyClick(QtUiTest::Key_Activate);
    }

    return ret;
}

bool TestAbstractSpinBox::canWrap(QObject *o)
{ return qobject_cast<QAbstractSpinBox*>(o); }

}