/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testlistview.h"
#include "testwidgetslog.h"

#include <QListView>

#include <qtuitestnamespace.h>

namespace QtUiTest {

TestListView::TestListView(QObject *_q)
    : TestAbstractItemView(_q), q(qobject_cast<QListView*>(_q))
{}

QStringList TestListView::list() const
{
    QStringList list;

    QAbstractItemModel *model = q->model();
    QModelIndex index = q->rootIndex();
    int count = 0;

    for (int i = 0, max_i = model->rowCount(index); i < max_i && count <= QTUITEST_MAX_LIST; ++i) {
        if (model->hasIndex(i, q->modelColumn(), index)) {
            list << model->data(model->index(i, q->modelColumn(), index)).toString();
        }
    }

    return list;
}

QModelIndex TestListView::indexForItem(QString const &item) const
{
    QModelIndex ret;

    QStringList list;

    QAbstractItemModel *model = q->model();
    QModelIndex index = q->rootIndex();
    int count = 0;

    for (int i = 0, max_i = model->rowCount(index); i < max_i && count <= QTUITEST_MAX_LIST; ++i) {
        int j = q->modelColumn();
        QModelIndex child;
        if (model->hasIndex(i, j, index) && model->data(model->index(i, j, index)).toString() == item) {
            ret = model->index(i, j, index);
        }
    }

    return ret;
}

bool TestListView::canWrap(QObject* o)
{ return qobject_cast<QListView*>(o); }

}