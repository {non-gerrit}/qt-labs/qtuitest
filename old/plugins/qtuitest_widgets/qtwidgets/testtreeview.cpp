/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtreeview.h"
#include "testwidgetslog.h"

#include <QTreeView>

#include <qtuitestnamespace.h>

namespace QtUiTest {

TestTreeView::TestTreeView(QObject *_q)
    : TestAbstractItemView(_q), q(static_cast<QTreeView*>(_q))
{
}

QString TestTreeView::selectedText() const
{
    TestWidgetsLog();

    if (q->allColumnsShowFocus()) {
        return selectedValue().toStringList().join(",");
    } 

    return TestAbstractItemView::selectedText();
}

QVariant TestTreeView::selectedValue() const
{
    TestWidgetsLog();

    if (q->allColumnsShowFocus()) {
        QStringList columnText;
        QModelIndex selectedIndex = q->currentIndex();

        for (int i = 0, max_i = q->model()->columnCount(selectedIndex.parent()); i < max_i; ++i) {
            if (q->isColumnHidden(i)) continue;
            QModelIndex sibling(selectedIndex.sibling(selectedIndex.row(), i));
            columnText << TestWidget::printable(sibling.data().toString());
        }

        return columnText;
    } 

    return TestAbstractItemView::selectedText();
}

QStringList TestTreeView::list() const
{
    class QModelListGetter : public QModelViewIterator<QTreeView>
    {
    public:
        QModelListGetter(QTreeView *view)
         : QModelViewIterator<QTreeView>(view) {};

        QStringList getList() {
            list.clear();
            iterate(view()->rootIndex(), view()->itemsExpandable());
            return list;
        }
    protected:
        virtual void visit(QModelIndex const &index)
        {
            if (index == view()->rootIndex() && !view()->rootIsDecorated()) {
                return;
            } else if (view()->isColumnHidden(index.column())) {
                return;
            }
            list << itemForIndex(index);
        }

        virtual bool isRowHidden(int row, const QModelIndex& index)
        {
            return view()->isRowHidden(row, index);
        }

        virtual QString itemForIndex(QModelIndex const& index) const
        {
            QString ret = TestWidget::printable(index.data().toString());
            ret.replace("/","\\\\/");

            QModelIndex rootIndex = view()->rootIndex();
            if (index.parent() != rootIndex) {
                ret = itemForIndex(index.parent()) + "/" + ret;
            }
            return ret;
        }
        QStringList list;
    };

    return QModelListGetter(q).getList();
}

QModelIndex TestTreeView::indexForItem(QString const &item) const
{
    return indexForItem(q->rootIndex(), item);
}

QModelIndex TestTreeView::indexForItem(QModelIndex const &index, QString const &item) const
{
    QModelIndex ret;
    static QRegExp slashRe("[^\\\\]/");

    int ind;
    if (0 != (ind = item.indexOf(slashRe)+1)) {
        return indexForItem(indexForItem(index, item.left(ind)), item.mid(ind+1));
    }

    QString itemUnescaped(item);
    itemUnescaped.replace("\\/", "/");

    QAbstractItemModel *model = q->model();
    int count = 0;

    for (int i = 0, max_i = model->rowCount(index); i < max_i && count <= QTUITEST_MAX_LIST; ++i) {
        if (q->isRowHidden(i, index)) continue;
        for (int j = 0, max_j = model->columnCount(index); j < max_j && count <= QTUITEST_MAX_LIST; ++j) {
            if (q->isColumnHidden(j)) continue;
            if (model->hasIndex(i, j, index) && model->data(model->index(i, j, index)).toString() == itemUnescaped) {
                if (ret.isValid()) {
                    qWarning("QtUitest: more than one item matches '%s' in item view", qPrintable(item));
                    return ret;
                } else {
                    ret = model->index(i, j, index);
                }
            }
        }
    }

    return ret;
}

bool TestTreeView::ensureVisible(QModelIndex const &index)
{
    if (!index.isValid()) return false;

    if (q->itemsExpandable() && !q->isExpanded(index.parent())) {
        ensureVisible(index.parent());
        //FIXME: Should expand using mouse/key clicks
        q->setExpanded(index.parent(), true);
    }

    return TestAbstractItemView::ensureVisible(index);
}

bool TestTreeView::canSelect(QString const &item) const
{
    return indexForItem(item).isValid();
}

bool TestTreeView::canWrap(QObject* o)
{ return qobject_cast<QTreeView*>(o); }

}
