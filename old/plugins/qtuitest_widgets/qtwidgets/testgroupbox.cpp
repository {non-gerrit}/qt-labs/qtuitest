/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testgroupbox.h"
#include "testwidgetslog.h"

#include <QGroupBox>
#include <QStyleOptionGroupBox>

namespace QtUiTest {

TestGroupBox::TestGroupBox(QObject *_q)
    : TestGenericTextWidget(_q), q(qobject_cast<QGroupBox*>(_q))
{
    connect(q, SIGNAL(toggled(bool)), this, SLOT(on_toggled(bool)));
}

void TestGroupBox::on_toggled(bool state)
{ emit stateChanged(state); }

Qt::CheckState TestGroupBox::checkState() const
{ return q->isChecked() ? Qt::Checked : Qt::Unchecked; }

bool TestGroupBox::setCheckState(Qt::CheckState state)
{
    TestWidgetsLog() << state << checkState() << q->isCheckable();
    if (state == checkState()) return true;
    if (!q->isCheckable()) return false;

    if (QtUiTest::mousePreferred()) {
        QStyle const* style = q->style();
        QStyleOptionGroupBox opt;
        opt.initFrom(q);
        QRect rect = style->subControlRect(QStyle::CC_GroupBox, &opt, QStyle::SC_GroupBoxCheckBox, q);
        QPoint p = rect.center();
        if (!ensureVisiblePoint(p)) return false;
        if (!QtUiTest::mouseClick(q, SIGNAL(toggled(bool)), mapToGlobal( p ) )) return false;
    } else {
        if (!setFocus()) {
            QtUiTest::setErrorString("Couldn't toggle group box check state: couldn't give "
                    "focus to group box.");
            return false;
        }
        if (!QtUiTest::keyClick(q, SIGNAL(toggled(bool)), QtUiTest::Key_ActivateButton )) return false;
    }

    return state == checkState();
}

QString TestGroupBox::labelText() const
{
    QString ret = text();
    if (ret.isEmpty())
        return ret;

    QObject *w = parent();
    while (w) {
        QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
        QtUiTest::LabelWidget *lw = qtuitest_cast<QtUiTest::LabelWidget*>(w);
        if (lw) {
            if (!lw->labelText().isEmpty()) {
                ret.prepend(lw->labelText() + "/");
                break;
            }
        }
        w = qw->parent();
    }
    return ret;
}

QObject* TestGroupBox::buddy() const
{
    QObject *buddy = q;

    if (!q->isCheckable()) {
        // If the widget is a QGroupBox, and it has exactly one focusable child widget,
        // consider that child widget the buddy
        QList<QWidget*> focusableWidgets;
        foreach (QWidget* fw, q->findChildren<QWidget*>()) {
            if (fw->focusPolicy() != Qt::NoFocus)
                focusableWidgets << fw;
        }
        if (focusableWidgets.count() == 1) {
            buddy = focusableWidgets[0];
        }
    }

    return buddy;
}

bool TestGroupBox::canWrap(QObject *o)
{ return qobject_cast<QGroupBox*>(o); }

}