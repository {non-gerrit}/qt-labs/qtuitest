/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdockwidget.h"
#include "testwidgetslog.h"

#include <QDockWidget>

namespace QtUiTest {

TestDockWidget::TestDockWidget(QObject* _q)
    : TestGenericTextWidget(_q), q(qobject_cast<QDockWidget*>(_q))
{ TestWidgetsLog(); }

QString TestDockWidget::text() const
{
    TestWidgetsLog();
    return windowTitle();
}

QString TestDockWidget::labelText() const
{
    QString ret = text();
    if (ret.isEmpty())
        return ret;

    QObject *w = parent();
    while (w) {
        QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
        QtUiTest::LabelWidget *lw = qtuitest_cast<QtUiTest::LabelWidget*>(w);
        if (lw) {
            if (!lw->labelText().isEmpty()) {
                ret.prepend(lw->labelText() + "/");
                break;
            }
        }
        w = qw->parent();
    }
    return ret;
}

QObject* TestDockWidget::buddy() const
{
    return q->widget();
}

bool TestDockWidget::canWrap(QObject *o)
{ return qobject_cast<QDockWidget*>(o); }

QString TestDockWidget::convertToPlainText(QString const &richText)
{
    static QRegExp rxBr("<br */?>");
    static QRegExp rxHtml("<[^>]+>");

    QString ret(richText);
    ret.replace(rxBr, "\n");
    ret.replace(rxHtml, QString());
    return ret;
}

}