/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testwebview.h"
#include "testwidgetslog.h"

#include <QWebView>
#include <QWebPage>
#include <QWebFrame>
#include <QWebElementCollection>

namespace QtUiTest {

TestWebView::TestWebView(QObject *_q)
    : TestWidget(_q), m_progress(0.0), q(qobject_cast<QWebView*>(_q))
{
    connect(q->page(), SIGNAL(loadProgress(int)), this, SLOT(on_progress(int)));
}

QString TestWebView::text() const
{
    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        return page->mainFrame()->toPlainText();
    }
    return QString();
}

QString TestWebView::selectedText() const
{
    return q->selectedText();
}

QStringList TestWebView::list() const
{
    QStringList ret;

    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        QWebElementCollection links = page->mainFrame()->findAllElements("a[href]");
        foreach (QWebElement el, links) {
            QString str = TestWidget::printable(el.toPlainText());
            if (!str.isEmpty()) ret << str;
        }
    }
    return ret;
}

QWebElement TestWebView::elementForItem(QString const &item) const
{
    QWebElement ret;
    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        QWebElementCollection links = page->mainFrame()->findAllElements("a[href]");
        foreach (QWebElement el, links) {
            if (TestWidget::printable(el.toPlainText()) == item) {
                ret = el;
                break;
            }
        }
    }
    return ret;
}

QRect TestWebView::visualRect(QString const &item) const
{
    return visualRect(elementForItem(item));
}

QRect TestWebView::visualRect(QWebElement const &el) const
{
    QRect ret;
    if (!el.isNull()) {
        ret = el.geometry();
        ret.translate(el.webFrame()->geometry().topLeft()-el.webFrame()->scrollPosition());
    }
    return ret;
}

bool TestWebView::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestWebView::select(QString const &item)
{
    QWebElement el(elementForItem(item));
    if (el.isNull()) return false;

    QRect rect = visualRect(el);
    QPoint pos = rect.center();
    QRect geometry = q->visibleRegion().boundingRect(); //q->geometry();
    QWebFrame *frame = el.webFrame();
    QRect horizScroll = frame->scrollBarGeometry(Qt::Horizontal);
    QRect vertScroll = frame->scrollBarGeometry(Qt::Vertical);
    if (!horizScroll.isEmpty()) {
        geometry.setBottom(horizScroll.top()-1);
    }
    if (!vertScroll.isEmpty()) {
        geometry.setRight(vertScroll.left()-1);
    }

    if (!geometry.contains(pos, true)) {
        //TODO: Is there a way to get the scrollbar up/down buttons? The scrollbar is
        //      not a QScrollBar. For now, use the API to scroll.

        // Scroll vertically
        if (geometry.bottom() < rect.bottom()) {
            frame->scroll(0, rect.bottom() - geometry.bottom() + geometry.height()/2);
        } else if (geometry.top() > rect.top()) {
            frame->scroll(0, rect.top() - geometry.top() - geometry.height()/2);
        }

        // Scroll horizontally
        if (geometry.right() < pos.x()) {
            frame->scroll(rect.right() - geometry.right(), 0);
        } else if (geometry.left() > pos.x()) {
            frame->scroll(rect.left() - geometry.left(), 0);
        }

        pos = visualRect(el).center();
    }

    QtUiTest::mouseClick(q->mapToGlobal(pos), Qt::LeftButton);
    waitForSignal(q, SIGNAL(linkClicked(QUrl)));
    return true;
}

void TestWebView::on_progress(int p)
{
    if (m_progress == p/100.0)
        return;
    m_progress = p/100.0;
}

QVariant TestWebView::getProperty(const QString& name) const
{
    QVariant ret;

    if (name.toUpper() == "PROGRESS") {
        ret = m_progress;
    } else {
        ret = q->property(name.toLatin1());
    }

    return ret;
}

bool TestWebView::canWrap(QObject *o)
{ return qobject_cast<QWebView*>(o); }

}