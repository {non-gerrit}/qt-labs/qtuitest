/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtext.h"
#include "testwidgetslog.h"

#include <QPointer>
#include <QVariant>

using namespace QtUiTest;

bool TestText::enterText(QObject* q, QString const& item, QString const& expectedText, bool commit)
{
    QByteArray textChanged = textChangedSignal(q);
    TextWidget* tw = qtuitest_cast<TextWidget*>(q);

    foreach (QChar const& c, item) {
        TestWidgetsLog() << asciiToModifiers(c.toLatin1());
        if (!keyClick(q, textChanged, asciiToKey(c.toLatin1()), asciiToModifiers(c.toLatin1()) ))
            return false;
    }

    QString newText = tw->text();
    if (newText != expectedText) {
        setErrorString(QString("After entering text, expected text of:\n%1\nBut text was:\n%2")
            .arg(expectedText)
            .arg(newText));
        return false;
    }

    if (commit) {
#ifdef Q_OS_SYMBIAN
        keyClick(Qt::Key_Select);
#else
        QtUiTest::Widget* w = qtuitest_cast<QtUiTest::Widget*>(q);
        if (w) return w->setEditFocus(false);
#endif
    }

    return true;
}

bool TestText::enterTextByProxy(QtUiTest::InputWidget* proxy, QObject* q, QString const& item, QString const& expectedText, bool commit)
{
    TextWidget* tw = qtuitest_cast<TextWidget*>(q);
    QByteArray textChanged = textChangedSignal(q);
    QPointer<QObject> safeQ(q);

    if (!proxy->enter(item, !commit)) return false;

    QString newText;

    for (int i = expectedText.length(); i != 0; --i) {
        if (!safeQ) {
            setErrorString("Widget was destroyed while entering text.");
            return false;
        }
        newText = tw->text();
        if (newText == expectedText) {
            break;
        }
        if (!waitForSignal(q, textChanged)) {
            setErrorString("Text did not change on focused widget due to key clicks");
            return false;
        }
    }

    if (newText != expectedText) newText = tw->text();
    if (newText != expectedText) {
        setErrorString(QString("After entering text, expected text of:\n%1\nBut text was:\n%2")
            .arg(expectedText)
            .arg(newText));
        return false;
    }

    return true;
}

