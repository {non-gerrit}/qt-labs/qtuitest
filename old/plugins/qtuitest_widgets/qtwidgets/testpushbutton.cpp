/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testpushbutton.h"
#include "testwidgetslog.h"

#include <QMenu>
#include <QPushButton>

namespace QtUiTest {

TestPushButton::TestPushButton(QObject *_q)
    : TestAbstractButton(_q), q(qobject_cast<QPushButton*>(_q))
{}

QStringList TestPushButton::list() const
{
    QStringList ret;

    QtUiTest::ListWidget *menu
        = qtuitest_cast<QtUiTest::ListWidget*>(q->menu());

    if (menu)
        ret = menu->list();

    return ret;
}

QRect TestPushButton::visualRect(QString const &item) const
{
    TestWidgetsLog();
    QRect ret;
    QtUiTest::ListWidget *lMenu
        = qtuitest_cast<QtUiTest::ListWidget*>(q->menu());

    if (lMenu)
        ret = lMenu->visualRect(item);
    return ret;
}

bool TestPushButton::ensureVisible(QString const& item)
{
    bool ret = false;

    QtUiTest::ListWidget *menu
        = qtuitest_cast<QtUiTest::ListWidget*>(q->menu());

    if (menu)
        ret = menu->ensureVisible(item);

    return ret;
}

bool TestPushButton::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestPushButton::select(QString const &item)
{
    bool ret = false;

    QtUiTest::SelectWidget *menu
        = qtuitest_cast<QtUiTest::SelectWidget*>(q->menu());

    if (menu)
    {
        activate();
        ret = menu->select(item);
    }

    return ret;
}

bool TestPushButton::canWrap(QObject *o)
{ return qobject_cast<QPushButton*>(o); }

}