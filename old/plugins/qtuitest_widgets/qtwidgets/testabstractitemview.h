/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTABSTRACTITEMVIEW_H
#define TESTABSTRACTITEMVIEW_H

#include <testwidget.h>
#include <QAbstractItemView>
#include <QTime>

const int QTUITEST_MAX_LIST=15000;

namespace QtUiTest {

class TestAbstractItemView : public TestWidget,
    public QtUiTest::TextWidget,
    public QtUiTest::ListWidget,
    public QtUiTest::SelectWidget,
    public QtUiTest::IndexedWidget
{
    Q_OBJECT
    Q_INTERFACES(
            QtUiTest::TextWidget
            QtUiTest::ListWidget
            QtUiTest::SelectWidget
            QtUiTest::IndexedWidget)

public:
    TestAbstractItemView(QObject*);

    virtual QString text() const;
    virtual QString selectedText() const;

    virtual QStringList list() const;
    virtual QRect visualRect(QString const&) const;
    virtual bool ensureVisible(QString const&);

    virtual bool canEnter(QVariant const&) const;
    virtual bool enter(QVariant const&,bool);

    virtual bool isMultiSelection() const;
    virtual bool canSelect(QString const&) const;
    virtual bool canSelectMulti(QStringList const&) const;
    virtual bool select(QString const&);
    virtual bool selectMulti(QStringList const&);

    virtual bool selectIndex(QVariantList const&);
    virtual QVariantList selectedIndex() const;

    static bool canWrap(QObject*);

protected:
    virtual QModelIndex indexForItem(QString const&) const;
    virtual QModelIndex indexFromList(QVariantList const&) const;
    virtual QVariantList listFromIndex(QModelIndex const&) const;
    virtual bool ensureVisible(QModelIndex const&);
    virtual bool select(QModelIndex const&);

signals:
    void selected(const QString&);

private slots:
    void on_activated(QModelIndex const&);

private:
    QAbstractItemView *q;
    QTime m_lastActivatedTimer;
};

template <typename T>
class QModelViewIterator
{
public:
    QModelViewIterator(T* view)
        : m_view(view), count(0)
    {}

    virtual ~QModelViewIterator()
    {}

    virtual void iterate(QModelIndex rootIndex = QModelIndex(), bool recurse=true)
    {
        QModelIndexList seen;
        iterate(rootIndex, &seen, recurse, true);
    }

protected:
    T* view() const
    { return m_view; }
    QAbstractItemModel* model() const
    { return m_view->model(); }

    virtual void visit(QModelIndex const&) = 0;
    virtual void overFlow()
    {
        QtUiTest::setErrorString("Maximum number of items exceeded");
    }
    virtual bool isRowHidden(int, const QModelIndex&)
    {
        return false;
    }
    virtual QString itemForIndex(QModelIndex const& idx) const
    {
        return TestWidget::printable(idx.data().toString());
    }

private:
    void iterate(QModelIndex const &index, QModelIndexList *seen, bool recurse, bool isRoot=false)
    {
        if (++count > QTUITEST_MAX_LIST) {
            overFlow();
            return;
        }

        if (index.isValid()) {
            visit(index);
            (*seen) << index;
        }

        for (int i = 0, max_i = model()->rowCount(index); i < max_i && count <= QTUITEST_MAX_LIST; ++i) {
            if (isRowHidden(i, index)) {
                continue;
            }
            for (int j = 0, max_j = model()->columnCount(index);
                    j < max_j && count <= QTUITEST_MAX_LIST; ++j) {
                QModelIndex child;
                if (model()->hasIndex(i, j, index)) {
                    child = model()->index(i, j, index);
                }
                if (child.isValid() && !seen->contains(child) && (isRoot || recurse)) {
#if 0
                    /* Very verbose! */
                    qDebug() << "child at (" << i << "," << j << ")" << child.data();
#endif
                    iterate(child, seen, recurse);
                }
            }
        }
    }

    T *m_view;
    int count;
};

}
#endif

