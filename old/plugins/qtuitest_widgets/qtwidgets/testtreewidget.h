/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTTREEWIDGET_H
#define TESTTREEWIDGET_H

#include "testtreeview.h"

class QTreeWidget;
class QTreeWidgetItem;

namespace QtUiTest {

class TestTreeWidget : public TestTreeView
{
    Q_OBJECT

public:
    TestTreeWidget(QObject*);

    virtual QRect visualRect(QString const&) const;
    virtual QStringList list() const;
    virtual bool canSelect(QString const&) const;
    virtual bool select(QString const&);
    virtual bool ensureVisible(QString const&);
    static bool canWrap(QObject*);

protected:
    QString itemName(QTreeWidgetItem *item, int column) const;
    QTreeWidgetItem *itemLookup(QTreeWidgetItem *parent, QString const &name) const;
    QWidget *itemWidgetLookup(QString const &name, QTreeWidgetItem *parent, QString &remainder) const;

private:
    QTreeWidget *q;
};

}

#endif

