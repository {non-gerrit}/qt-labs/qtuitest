/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtreewidget.h"
#include "testwidgetslog.h"

#include <QTreeWidget>

#include <qtuitestnamespace.h>

namespace QtUiTest {

TestTreeWidget::TestTreeWidget(QObject *_q)
    : TestTreeView(_q), q(static_cast<QTreeWidget*>(_q))
{
}

QStringList TestTreeWidget::list() const
{
    QStringList ret;
    QTreeWidgetItemIterator it(q);
    while (*it) {
        for (int i=0; i<q->columnCount(); ++i) {
            QtUiTest::ListWidget *lw = qtuitest_cast<QtUiTest::ListWidget*>(q->itemWidget(*it, i));
            if (lw) {
                QStringList widgetList = lw->list();
                QString parent = itemName((*it)->parent(), i);
                foreach (QString s, widgetList) {
                    ret << parent + "/" + s;
                }
            } else {
                  ret << itemName((*it), i);
            }
        }
        ++it;
    }

    return ret;
}

QString TestTreeWidget::itemName(QTreeWidgetItem *item, int column) const
{
    QString ret;
    if (!item) return ret;
    ret = item->text(column);
    if (item->parent() && item->parent() != q->invisibleRootItem()) {
        return itemName(item->parent(), 0) + "/" + ret;
    }

    return ret;
}

QTreeWidgetItem *TestTreeWidget::itemLookup(QTreeWidgetItem *parent, QString const &name) const
{
    QTreeWidgetItem *ret = 0;

    for (int i=0, childCount = parent->childCount(); i < childCount; ++i) {
        QTreeWidgetItem *item = parent->child(i);
        for (int j=0, colCount = q->columnCount(); j < colCount; ++j) {
            if (item->text(j) == name) {
                return item;
            }
        }
    }

    return ret;
}

QWidget *TestTreeWidget::itemWidgetLookup(QString const &name, QTreeWidgetItem *parent, QString &remainder) const
{
    static QRegExp slashRe("[^\\\\]/");
    int ind;
    QString top = name;
    if ((ind = name.indexOf(slashRe)+1)) {
        top = name.left(ind);
    }

    QTreeWidgetItem *widgetItem = itemLookup(parent, top);
    if (widgetItem == 0) {
        remainder = name;
        for (int i=0, colCount = q->columnCount(); i < colCount; ++i) {
            QWidget *iw = q->itemWidget(parent->child(i), 0);
            if (iw) {
                QtUiTest::ListWidget *lw = qtuitest_cast<QtUiTest::ListWidget*>(iw);
                QStringList widgetList = lw->list();
                if (widgetList.contains(remainder)) {
                    return iw;
                }
            }
        }
        return 0;
    } else {
        if (!widgetItem->isExpanded()) {
            widgetItem->setExpanded(true);
        }
        return itemWidgetLookup(name.mid(ind+1), widgetItem, remainder);
    }
}

bool TestTreeWidget::select(QString const &item)
{
    if (!canSelect(item)) {
        TestWidgetsLog() << "can't select" << item;
        return false;
    }

    QModelIndex index = indexForItem(item);
    if (index.isValid()) {
        return TestAbstractItemView::select(index);
    }

    QString remainder;
    QWidget *w = itemWidgetLookup(item, q->invisibleRootItem(), remainder);
    QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
    QtUiTest::ListWidget *lw = qtuitest_cast<QtUiTest::ListWidget*>(w);
    qw->ensureVisibleRegion(lw->visualRect(remainder));
    QtUiTest::SelectWidget *sw = qtuitest_cast<QtUiTest::SelectWidget*>(w);
    if (sw) {
        return sw->select(remainder);
    }

    return false;
}

bool TestTreeWidget::ensureVisible(QString const &item)
{
    if (!canSelect(item)) {
        TestWidgetsLog() << "can't select" << item;
        return false;
    }

    QModelIndex index = indexForItem(item);
    if (index.isValid()) {
        return TestAbstractItemView::ensureVisible(index);
    }

    QString remainder;
    QWidget *w = itemWidgetLookup(item, q->invisibleRootItem(), remainder);
    QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
    QtUiTest::ListWidget *lw = qtuitest_cast<QtUiTest::ListWidget*>(w);
    return qw->ensureVisibleRegion(lw->visualRect(remainder));
}

QRect TestTreeWidget::visualRect(QString const &item) const
{
    QRect ret;

    QModelIndex index = indexForItem(item);

    if (index.isValid()) {
        return q->visualRect(index);
    } else {
        QString remainder;
        QWidget *w = itemWidgetLookup(item, q->invisibleRootItem(), remainder);
        QtUiTest::ListWidget *lw = qtuitest_cast<QtUiTest::ListWidget*>(w);
        if (lw) {
            QRect rect = lw->visualRect(remainder);
            rect.moveTopLeft( q->mapFromGlobal( w->mapToGlobal( rect.topLeft() ) ) );
            return rect;
        }
    }

    return ret;
}

bool TestTreeWidget::canSelect(QString const &item) const
{
    return list().contains(item);
}

bool TestTreeWidget::canWrap(QObject* o)
{ return qobject_cast<QTreeWidget*>(o); }

}
