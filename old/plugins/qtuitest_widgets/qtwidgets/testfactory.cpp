/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testfactory.h"

#include "testabstractbutton.h"
#include "testabstractitemview.h"
#include "testabstractspinbox.h"
#include "testcalendarwidget.h"
#include "testcheckbox.h"
#include "testcombobox.h"
#include "testdateedit.h"
#include "testdatetimeedit.h"
#include "testdockwidget.h"
#include "testgenericcheckwidget.h"
#include "testgenerictextwidget.h"
#include "testgroupbox.h"
#include "testheaderview.h"
#include "testignore.h"
#include "testlabel.h"
#include "testlineedit.h"
#include "testlistview.h"
#include "testmenu.h"
#include "testmenubar.h"
#include "testpushbutton.h"
#include "testtabbar.h"
#include "testtextedit.h"
#include "testtimeedit.h"
#include "testtoolbar.h"
#include "testtreeview.h"
#include "testtreewidget.h"
#include "testgenericinputmethod.h"
 
#if !defined(QT_NO_WEBKIT)
#  include "testwebview.h"
#endif

#include <testwidget.h>
#include "testwidgetslog.h"
#include <QApplication>

namespace QtUiTest {

TestWidgetsFactory::TestWidgetsFactory()
{
}

QObject* TestWidgetsFactory::create(QObject* o)
{
    QObject* ret = 0;

#define TRY(Klass)           \
    if (Klass::canWrap(o)) { \
        ret = new Klass(o);  \
        break;               \
    }

    /* Order is important here; classes should be listed in order of
     * most to least derived. */

    do {
        /* Qt widgets */
        TRY(TestDateEdit);
        TRY(TestTimeEdit);
        TRY(TestDateTimeEdit);
        TRY(TestCalendarWidget);
        TRY(TestDateTimeEdit);
        TRY(TestHeaderView);
        TRY(TestTreeWidget);
        TRY(TestTreeView);
        TRY(TestListView);
        TRY(TestAbstractItemView);
        TRY(TestTabBar);
        TRY(TestMenuBar);
        TRY(TestToolBar);
        TRY(TestMenu);
        TRY(TestDockWidget);
#if !defined(QT_NO_WEBKIT)
        TRY(TestWebView);
#endif	
        TRY(TestLabel);
        TRY(TestAbstractSpinBox);
        TRY(TestGroupBox);
        TRY(TestCheckBox);
        TRY(TestComboBox);
        TRY(TestLineEdit);
        TRY(TestTextEdit);
        TRY(TestPushButton);
        TRY(TestAbstractButton);
        TRY(TestGenericCheckWidget);
        TRY(TestGenericTextWidget);
        TRY(TestIgnore);
        TRY(TestWidget);
        TRY(TestGenericInputMethod);
    } while(0);

    TestWidgetsLog() << o << ret;

    return ret;
}

QStringList TestWidgetsFactory::keys() const
{
    /* Order doesn't matter here. */
    return QStringList()
        << "QAbstractButton"
        << "QAbstractItemView"
        << "QAbstractSlider"
        << "QAbstractSpinBox"
        << "QCalendarWidget"
        << "QCheckBox"
        << "QComboBox"
        << "QDateEdit"
        << "QDateTimeEdit"
        << "QDockWidget"
        << "QFrame"
        << "QGroupBox"
        << "QHeaderView"
        << "QLCDNumber"
        << "QLabel"
        << "QLineEdit"
        << "QListView"
        << "QMenu"
        << "QMenuBar"
        << "QMessageBox"
        << "QProgressBar"
        << "QPushButton"
        << "QStackedWidget"
        << "QTabBar"
        << "QTextEdit"
        << "QTimeEdit"
        << "QToolBar"
        << "QTreeView"
        << "QTreeWidget"
#if !defined(QT_NO_WEBKIT)	
        << "QWebView"
#endif	
        << "QWidget"
        << "QInputContext"
        ;
}

}
#include <qplugin.h>
Q_EXPORT_PLUGIN2(qttestwidgets, QtUiTest::TestWidgetsFactory)
