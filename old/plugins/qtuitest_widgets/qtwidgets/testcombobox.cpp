/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testcombobox.h"
#include "testwidgetslog.h"

#include <qtuitestnamespace.h>

#include <QComboBox>
#include <QAbstractItemView>

namespace QtUiTest {

TestComboBox::TestComboBox(QObject *_q)
    : TestWidget(_q), q(qobject_cast<QComboBox*>(_q))
{
    TestWidgetsLog();
    connect(q, SIGNAL(activated(QString)), this, SIGNAL(selected(QString)));
}

QString TestComboBox::text() const
{ TestWidgetsLog(); return list().join("\n"); }

QString TestComboBox::selectedText() const
{ TestWidgetsLog(); return q->currentText(); }

QStringList TestComboBox::list() const
{
    QStringList ret;
    for (int i = 0, max = q->count(); i < max; ++i) {
        ret << q->itemText(i);
    }
    TestWidgetsLog() << ret;
    return ret;
}

QRect TestComboBox::visualRect(QString const &item) const
{
    TestWidgetsLog();
    QRect ret;
    QtUiTest::ListWidget *viewList
        = qtuitest_cast<QtUiTest::ListWidget*>(q->view());
    if (viewList) {
        ret = viewList->visualRect(item);
    }
    return ret;
}

QString TestComboBox::labelText() const
{
    QString ret = selectedText();
    if (ret.isEmpty())
        return ret;

    QObject *w = parent();
    while (w) {
        QtUiTest::Widget *qw = qtuitest_cast<QtUiTest::Widget*>(w);
        QtUiTest::LabelWidget *lw = qtuitest_cast<QtUiTest::LabelWidget*>(w);
        if (lw) {
            if (!lw->labelText().isEmpty()) {
                ret.prepend(lw->labelText() + "/");
                break;
            }
        }
        w = qw->parent();
    }
    return ret;
}

QObject* TestComboBox::buddy() const
{
    return q;
}

bool TestComboBox::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestComboBox::select(QString const &item)
{
    TestWidgetsLog() << item;
    if (!canSelect(item)) return false;
    if (selectedText() == item) return true;

    QtUiTest::Widget *wView
        = qtuitest_cast<QtUiTest::Widget*>(q->view());
    QtUiTest::SelectWidget *sView
        = qtuitest_cast<QtUiTest::SelectWidget*>(q->view());
    if (!wView || !sView) {
        QtUiTest::setErrorString("Can't find a pointer to the item view for this combobox.");
        return false;
    }

    TestWidgetsLog() << "view isVisible()" << wView->isVisible();

    /* Open the view if it is not currently open. */
    if (!wView->isVisible()) {
        if (QtUiTest::mousePreferred()) {
            QStyle const* style = q->style();
            QStyleOptionComboBox opt;
            opt.initFrom(q);
            QRect rect = style->subControlRect( QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxArrow, q);
            QPoint pos = rect.center();
            TestWidgetsLog() << "local pos" << pos;
            pos = mapToGlobal(pos);
            /* May take more than one click. */
            for (int i = 0; i < 2 && !wView->isVisible(); ++i) {
                TestWidgetsLog() << "global pos" << pos;
                TestWidgetsLog() << "Going to click at" << pos << "to open the view";
                QtUiTest::mouseClick(pos, Qt::LeftButton);
                QtUiTest::waitForSignal(q, SIGNAL(highlighted(int)));
            }
            // Combo box tries to detect and ignore "accidental" double clicks,
            // so wait for a bit to ensure any subsequent clicks aren't
            // ignored.
            QtUiTest::wait( qApp->doubleClickInterval() );
        } else {
            TestWidgetsLog() << "Going to give myself focus";
            if (!setFocus()) {
                QtUiTest::setErrorString("Could not give focus to combobox");
                return false;
            }
            if (q->isEditable()) {
                return selectWithKeys(item);
            } else {
                //FIXME: This doesn't work if the combobox is editable
                QtUiTest::keyClick(QtUiTest::Key_Select);
                if (!q->view()->hasFocus() && !QtUiTest::waitForEvent( q->view(), QEvent::FocusIn)) {
                    QtUiTest::setErrorString("Could not give focus to combobox popup.");
                    return false;
                }
            }
        }
    }

    /* Select the desired item from the view. */
    TestWidgetsLog() << "Calling select() on the view";
    bool ret = sView->select(item);

    // Wait up to 1 second for the view to disappear after selection.
    for (int i = 0; i < 1000 && wView->isVisible(); i+=100, QtUiTest::wait(100))
    {}

    if (!ret && QtUiTest::errorString().isEmpty()) {
        QtUiTest::setErrorString(
            "Selecting from embedded item view in combo box failed for an unknown reason.");
    }

    return ret;
}

bool TestComboBox::selectWithKeys(QString const &item)
{
    QStringList allItems = list();

    const int maxtries = 100;
    int desiredIndex = allItems.indexOf(item);

    if (-1 == desiredIndex)
        return false;

    int currentIndex = q->currentIndex();

    // Move vertically
    Qt::Key key;
    if (desiredIndex > currentIndex)
        key = Qt::Key_Down;
    else
        key = Qt::Key_Up;

    for (int i = 0; i < maxtries && selectedText() != item; ++i) {
        TestWidgetsLog() << "keyClick" << (key==Qt::Key_Down ? "Down" : "Up");
        if (!QtUiTest::keyClick(q, key)) return false;
    }
    QString selected = selectedText();
    TestWidgetsLog() << "selectedText() now" << selected;
    if (selected != item) {
        QtUiTest::setErrorString(QString(
            "Up/down keys should have caused item %1 to become selected, but item %2 "
            "is selected instead.").arg(item).arg(selected));
        return false;
    }
    return true;
}

bool TestComboBox::canEnter(QVariant const& item) const
{
    if (q->isEditable()) {
        if (!item.canConvert<QString>()) return false;
        int dontcare = 0;
        QString text = item.toString();
        const QValidator *validator = q->validator();
        if (validator) {
            return (QValidator::Acceptable==validator->validate(text, dontcare));
        } else {
            return true;
        }
    } else {
        return canSelect(item.toString());
    }
}

bool TestComboBox::enter(QVariant const& item, bool noCommit)
{
    if (q->isEditable()) {
        return TestWidget::enter(item, noCommit);
    } else {
        return select(item.toString());
    }
}

bool TestComboBox::canWrap(QObject *o)
{ return qobject_cast<QComboBox*>(o); }

}