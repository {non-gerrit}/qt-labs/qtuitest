/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testcheckbox.h"
#include "testwidgetslog.h"

#include <QCheckBox>

namespace QtUiTest {

TestCheckBox::TestCheckBox(QObject *_q)
    : TestAbstractButton(_q), q(qobject_cast<QCheckBox*>(_q))
{ TestWidgetsLog(); }

bool TestCheckBox::isTristate() const
{ TestWidgetsLog(); return q->isTristate(); }

Qt::CheckState TestCheckBox::checkState() const
{ TestWidgetsLog(); return q->checkState(); }

bool TestCheckBox::setCheckState(Qt::CheckState state)
{
    Qt::CheckState initState = checkState();
    if (state == initState) return true;

    bool ret = activate(QtUiTest::NoOptions);
    TestWidgetsLog() << "activated:" << ret;

    if (ret && (state == initState) && !QtUiTest::waitForSignal(q, SIGNAL(stateChanged(int)))) {
        QtUiTest::setErrorString("Successfully activated button, but check state did not change "
                "to expected value.");
        return false;
    }

    // Give application time to respond to first activation
    QtUiTest::wait(20);

    if (state != checkState()) {
        ret = activate(QtUiTest::NoOptions);
        TestWidgetsLog() << "activated:" << ret;
    }

    if (ret && (state != checkState()) && !QtUiTest::waitForSignal(q, SIGNAL(stateChanged(int)))) {
        QtUiTest::setErrorString("Successfully activated button, but check state did not change "
                "to expected value.");
        return false;
    }
    TestWidgetsLog() << "state:" << ret;
    return ret;
}

bool TestCheckBox::canWrap(QObject *o)
{ return qobject_cast<QCheckBox*>(o); }

}