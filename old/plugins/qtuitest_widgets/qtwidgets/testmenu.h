/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTMENU_H
#define TESTMENU_H

#include <testwidget.h>

#include <QtGlobal>

class QMenu;
class QAction;

namespace QtUiTest {

class TestMenu : public TestWidget, public QtUiTest::TextWidget,
    public QtUiTest::ListWidget, public QtUiTest::CheckWidget,
    public QtUiTest::SelectWidget, public QtUiTest::CheckItemWidget
{
    Q_OBJECT
    Q_INTERFACES(
            QtUiTest::TextWidget
            QtUiTest::ListWidget
            QtUiTest::CheckWidget
            QtUiTest::CheckItemWidget
            QtUiTest::SelectWidget)

public:
    TestMenu(QObject*);

    virtual QString text() const;
    virtual QString selectedText() const;

    virtual QStringList list() const;
    virtual QRect visualRect(QString const&) const;
    virtual bool ensureVisible(QString const&);

    virtual Qt::CheckState checkState() const;

    virtual bool canSelect(const QString&) const;
    virtual bool select(const QString&);

    virtual bool isCheckable(const QString&);
    virtual bool isChecked(const QString&) const;
    virtual bool setChecked(const QString&, bool);

    virtual bool hasFocus() const;

    static bool canWrap(QObject*);

signals:
    void stateChanged(int);
    void selected(const QString&);

private slots:
    void on_hovered(QAction*);
    void on_triggered(bool);
    void on_toggled(bool);

private:
    QMenu *q;
    QAction *lastAction;

    QAction *actionForItem(QString const&) const;
};

}

#endif

