/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtabbar.h"

#include "testwidgetslog.h"

#include <QTabBar>
#include <QTabWidget>
#include <QToolButton>

namespace QtUiTest {

TestTabBar::TestTabBar(QObject *_q)
    : TestWidget(_q), q(qobject_cast<QTabBar*>(_q))
{
    connect(q,    SIGNAL(currentChanged(int)),
            this, SLOT(on_currentChanged(int)));
}

void TestTabBar::on_currentChanged(int index)
{ emit selected(q->tabText(index)); }

QString TestTabBar::text() const
{ return list().join("\n"); }

QString TestTabBar::selectedText() const
{ return q->tabText(q->currentIndex()); }

QStringList TestTabBar::list() const
{
    QStringList ret;
    for (int i = 0, max = q->count(); i < max; ++i)
        ret << q->tabText(i);
    return ret;
}

QRect TestTabBar::visualRect(QString const &item) const
{
    int index = indexForItem(item);
    if (index != -1)
        return q->visibleRegion().subtracted(q->childrenRegion()).intersected(QRegion(q->tabRect(index))).boundingRect();
    else
        return QRect();
}

bool TestTabBar::ensureVisible(QString const& item)
{
    /* First, find desired index. */
    int desired = indexForItem(item);
    if (-1 == desired) return false;

    QToolButton* leftB = 0;
    QToolButton* rightB = 0;

    QList<QToolButton*> buttons = q->findChildren<QToolButton*>();
    QRegion buttonRegion;
    foreach (QToolButton* b, buttons) {
        if (b->arrowType() == Qt::LeftArrow) {
            leftB = b;
            buttonRegion |= b->geometry();
        } else if (b->arrowType() == Qt::RightArrow) {
            rightB = b;
            buttonRegion |= b->geometry();
        }
    }
    QRect buttonRect = buttonRegion.boundingRect();

    int clicks = 0;
    /* While desired tab isn't visible... */
    while (q->visibleRegion().subtracted(buttonRect).intersected( q->tabRect(desired) ).isEmpty()
        && clicks < 50) {

        TestWidgetsLog() << "visible:" << q->visibleRegion().boundingRect() << "buttons:" << buttonRect << "tab:" << q->tabRect(desired);

        QObject* button = 0;

        /* Shall we go to the left or the right? */
        if (q->tabRect(desired).left() >= q->visibleRegion().subtracted(buttonRect).boundingRect().right())
            button = rightB;
        else
            button = leftB;

        QtUiTest::ActivateWidget* aw
            = qtuitest_cast<QtUiTest::ActivateWidget*>(button);
        if (!aw) return false;
        aw->activate();

        ++clicks;
    }

    return !q->visibleRegion().subtracted(buttonRect).intersected( q->tabRect(desired) ).isEmpty();
}

bool TestTabBar::canSelect(QString const& item) const
{
    return indexForItem(item) != -1;
}

bool isAncestor(QObject* parent, QObject* child)
{
    bool ret = false;
    QObject* p = child;
    while (p && !ret) {
        ret = (p == parent);
        p = p->parent();
    }
    return ret;
}

bool TestTabBar::select(QString const& item)
{
    using namespace QtUiTest;

    QStringList allTabs;
    int desired = -1;
    desired = indexForItem(item);
    for (int i = 0, max = q->count(); i < max && -1 == desired; ++i) {
        if (q->tabText(i) == item)
            desired = i;
        allTabs << q->tabText(i);
    }
    if (-1 == desired) {
        setErrorString(
            "Could not select tab '" + item + "' because there is no tab with that text");
        return false;
    }
    if (q->currentIndex() == desired) return true;

    QString originalTab = q->tabText(q->currentIndex());

    if (mousePreferred()) {
        if (!ensureVisible(item)) return false;
        QPoint p = visualRect(item).center();
        if (!ensureVisiblePoint(p)) return false;
        mouseClick(mapToGlobal(p));
        QtUiTest::waitForSignal(q, SIGNAL(currentChanged(int)));
        if (q->currentIndex() != desired) {
            setErrorString(
                "Clicked on tab '" + item + "' but it did not appear to become selected.\n"
                "Selected tab is: " + selectedText());
            return false;
        }
        return true;
    }

    if (!setFocus()) return false;

    Qt::Key key;
    int diff = 0;
    if (desired < q->currentIndex()) {
        key = Qt::Key_Left;
        diff = q->currentIndex() - desired;
    }
    else {
        key = Qt::Key_Right;
        diff = desired - q->currentIndex();
    }
    for (int i = 0; i < diff; ++i) {
        if (!setFocus()) return false;
        if (!keyClick(q, SIGNAL(currentChanged(int)), key))
            return false;
    }

    if (q->currentIndex() == desired) return true;
    setErrorString(
        QString("Can't change tabs: pressed the %1 key %2 times which should have "
                "moved from '%3' to '%4', but the current tab ended up as '%5'.")
        .arg((key == Qt::Key_Right) ? "Right" : "Left")
        .arg(diff)
        .arg(originalTab)
        .arg(item)
        .arg(q->tabText(q->currentIndex()))
    );
    return false;
}

int TestTabBar::indexForItem(QString const& item) const
{
    for (int i = 0, max = q->count(); i < max; ++i) {
        if (q->tabText(i) == item || labelText(q->tabText(i)) == item)
            return i;
    }
    return -1;
}

bool TestTabBar::inherits(QtUiTest::WidgetType type) const
{ return (QtUiTest::TabBar == type); }

bool TestTabBar::canWrap(QObject *o)
{ return qobject_cast<QTabBar*>(o); }

}