SOURCES += \
        testabstractbutton.cpp \
        testabstractitemview.cpp \
        testabstractspinbox.cpp \
        testcalendarwidget.cpp \
        testcheckbox.cpp \
        testcombobox.cpp \
        testdateedit.cpp \
        testdatetimeedit.cpp \
        testdockwidget.cpp \
        testfactory.cpp \
        testgenericcheckwidget.cpp \
        testgenerictextwidget.cpp \
        testgroupbox.cpp \
        testheaderview.cpp \
        testignore.cpp \
        testlabel.cpp \
        testlineedit.cpp \
        testlistview.cpp \
        testmenu.cpp \
        testmenubar.cpp \
        testpushbutton.cpp \
        testtabbar.cpp \
        testtext.cpp \
        testtextedit.cpp \
        testtimeedit.cpp \
        testtoolbar.cpp \
        testtreeview.cpp \
        testtreewidget.cpp \
        testgenericinputmethod.cpp

HEADERS += \
        testabstractbutton.h \
        testabstractitemview.h \
        testabstractspinbox.h \
        testcalendarwidget.h \
        testcheckbox.h \
        testcombobox.h \
        testdateedit.h \
        testdatetimeedit.h \
        testdockwidget.h \
        testfactory.h \
        testgenericcheckwidget.h \
        testgenerictextwidget.h \
        testgroupbox.h \
        testheaderview.h \
        testignore.h \
        testlabel.h \
        testlineedit.h \
        testlistview.h \
        testwidgetslog.h \
        testmenu.h \
        testmenubar.h \
        testpushbutton.h \
        testtabbar.h \
        testtext.h \
        testtextedit.h \
        testtimeedit.h \
        testtoolbar.h \
        testtreeview.h \
        testtreewidget.h \
        testgenericinputmethod.h

TEMPLATE=lib
CONFIG+=plugin
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
TARGET=qtwidgets
TARGET=$$qtLibraryTarget($$TARGET)

include($$SRCROOT/libqtuitest/libqtuitest.pri)


target.path=$$[QT_INSTALL_PLUGINS]/qtuitest_widgets
unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData NetworkServices
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
    LIBS+= -lws32 -leuser -lcone
}
INSTALLS += target

contains(QT_CONFIG, webkit) {
    QT+=webkit
    SOURCES+=testwebview.cpp
    HEADERS+=testwebview.h
}


