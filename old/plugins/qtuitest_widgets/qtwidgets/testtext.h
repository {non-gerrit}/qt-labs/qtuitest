/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTTEXT_H
#define TESTTEXT_H

#include <qtuitestnamespace.h>
#include <qrect.h>
#include <qpoint.h>
#include "testwidgetslog.h"

namespace QtUiTest {

namespace TestText {

    inline
    QByteArray textChangedSignal(QObject* q)
    {
        QByteArray textChangedSignal("textChanged(QString)");
        if (-1 == q->metaObject()->indexOfSignal(textChangedSignal)) {
            textChangedSignal = "textChanged()";
        }
        textChangedSignal.prepend(QSIGNAL_CODE);
        return textChangedSignal;
    }

    // Erase the text in the widget q by using key clicks
    template <typename T> inline
    bool eraseTextByKeys(T* q)
    {
        using namespace QtUiTest;

        if (!q->hasFocus()) {
            setErrorString("Cannot erase text from unfocused widget.");
            return false;
        }

        Widget* w = qtuitest_cast<Widget*>(q);
        bool hadEditFocus = w->hasEditFocus();
        if (!w->setEditFocus(true)) {
            setErrorString("Couldn't erase text from widget: couldn't give edit focus to widget.");
            return false;
        }

        const int MAX = q->text().length()+1;
        int i = 0;

        int pos = q->cursorPosition();
        int oldPos = -1;
        TestWidgetsLog() << "pos" << pos << "MAX" << MAX;

        while (oldPos != pos && (++i < MAX) && (pos < MAX-1)) {
            keyClick(q, SIGNAL(cursorPositionChanged(int,int)), Qt::Key_Right);
            oldPos = pos;
            pos = q->cursorPosition();
        }

        if (i >= MAX) {
            setErrorString("Could not move cursor to rightmost position in widget.");
            return false;
        }

        QByteArray textChanged = textChangedSignal(q);
        i = 0;
        do {
            if (!keyClick(q, textChanged, Qt::Key_Backspace))
                return false;
        } while (q->cursorPosition() != 0 && (++i < MAX));

        if (i >= MAX || !q->text().isEmpty()) {
            setErrorString("Could not erase all text by pressing backspace.");
            return false;
        }

        if (!w->setEditFocus(hadEditFocus)) {
            setErrorString("Couldn't return widget to original edit focus state.");
            return false;
        }

        return true;
    }

    template <typename T> inline
    bool eraseTextByMouse(T* q)
    {
//        QtUiTest::setErrorString("Erasing text by mouse is not implemented.");
//        return false;

        //FIXME: For now, erase text using keys. Not sure if erasing text using
        // the mouse makes sense in Qt.
        return eraseTextByKeys(q);
    }

    bool enterText(QObject* q, QString const& item, QString const& expectedText, bool commit = true);
    bool enterTextByProxy(QtUiTest::InputWidget* proxy, QObject* q, QString const& item, QString const& expectedText, bool commit = true);
};

}

#endif

