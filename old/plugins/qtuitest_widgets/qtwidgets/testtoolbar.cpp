/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtoolbar.h"
#include "testwidgetslog.h"

#include <QToolBar>
#include <QAction>

namespace QtUiTest {

TestToolBar::TestToolBar(QObject *_q)
    : TestWidget(_q), q(qobject_cast<QToolBar*>(_q)), lastAction(0)
{
    // For accurate ordering of events recording, these connections
    // must come before all others.
//    QtUiTest::connectFirst(q,    SIGNAL(hovered(QAction*)),
//                        this, SLOT(on_hovered(QAction*)));
}

QStringList TestToolBar::list() const
{
    QStringList ret;

    /* Iterate through every action */
    foreach( QAction *a, q->actions() ) {
        if (!a->isVisible())
            continue;
        QString t = a->text();
        t.replace("/","\\/");
        if (!t.isEmpty()) ret << t;
    }

    return ret;
}

QRect TestToolBar::visualRect(QString const &item) const
{
//FIXME: Implement me
    Q_UNUSED(item);
    QRect ret;
    return ret;
}

bool TestToolBar::ensureVisible(QString const&)
{ return false; }

QString TestToolBar::labelText() const
{
    return q->windowTitle();
}

QObject* TestToolBar::buddy() const
{
    return q;
}

bool TestToolBar::canSelect(QString const &item) const
{
    return list().contains(item);
}

bool TestToolBar::select(QString const &item)
{
//FIXME: Implement me
    Q_UNUSED(item);
    return false;
}

bool TestToolBar::hasFocus() const
{
    return q->hasFocus();
}

bool TestToolBar::canWrap(QObject *o)
{ return qobject_cast<QToolBar*>(o); }

}
