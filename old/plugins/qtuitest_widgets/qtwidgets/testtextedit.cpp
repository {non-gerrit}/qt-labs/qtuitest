/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testtext.h"
#include "testtextedit.h"
#include "testwidgetslog.h"

#include <QLayout>
#include <QPointer>
#include <QTextDocumentFragment>
#include <QTextEdit>
#include <QTextBlock>
#include <QTextFragment>
#include <QScrollBar>

namespace QtUiTest {

TestTextEdit::TestTextEdit(QObject *_q)
    : TestWidget(_q)
      , lastEntered()
      , lastCursorPosition(-1)
      , committed(false)
      , q(qobject_cast<QTextEdit*>(_q))
{
    lastCursorPosition = cursorPosition();
    connect(q, SIGNAL(textChanged()),           this, SLOT(onTextChanged()));
    connect(q, SIGNAL(textChanged()),           this, SIGNAL(textChanged()));
    connect(q, SIGNAL(cursorPositionChanged()), this, SLOT(onCursorPositionChanged()));
}

QString TestTextEdit::text() const
{ return q->toPlainText(); }

QString TestTextEdit::selectedText() const
{ return q->textCursor().selection().toPlainText(); }

QStringList TestTextEdit::list() const
{
    QStringList ret;
    QTextDocument *doc = q->document();

    for (int i = 0, blockCount = doc->blockCount(); i < blockCount; ++i) {
        QTextBlock block = doc->findBlockByNumber(i);
        QTextBlock::iterator it;
        for (it = block.begin(); !(it.atEnd()); ++it) {
            QTextFragment fragment = it.fragment();
            if (fragment.isValid() && fragment.charFormat().isAnchor()) {
                ret << fragment.text();
            }
        }
     }

    return ret;
}

QTextFragment TestTextEdit::fragmentForItem(QString const &item) const
{
    QTextFragment ret;
    QTextDocument *doc = q->document();

    for (int i = 0, blockCount = doc->blockCount(); i < blockCount; ++i) {
        QTextBlock block = doc->findBlockByNumber(i);
        QTextBlock::iterator it;
        for (it = block.begin(); !(it.atEnd()); ++it) {
            QTextFragment fragment = it.fragment();
            if (fragment.isValid() && fragment.charFormat().isAnchor() &&
                fragment.text() == item) {
                return fragment;
            }
        }
     }

    return ret;
}

QRect TestTextEdit::visualRect(QString const &item) const
{
    return visualRect(fragmentForItem(item));
}

QRect TestTextEdit::visualRect(QTextFragment const &fragment) const
{
    QRect ret;

    if (fragment.isValid()) {
        QTextCursor cursor(q->document());
        cursor.setPosition(fragment.position()+1);
        ret = q->cursorRect(cursor);
    }

    return ret;
}

bool TestTextEdit::canEnter(QVariant const& item) const
{
    return item.canConvert<QString>();
}

bool TestTextEdit::enter(QVariant const& item, bool noCommit)
{
    if (!canEnter(item)) return false;

    bool hadEditFocus = hasEditFocus();

    if (!hadEditFocus && !setEditFocus(true)) return false;

    using namespace QtUiTest;

    QPointer<QObject> safeThis = this;
    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    QString oldText = text();
    QString itemString = item.toString();
    QString expectedText = oldText;
    expectedText.insert(q->textCursor().position(), itemString);

    /* If there's text currently in the field, and we committed it,
       then erase it first. */
    if (!oldText.isEmpty() && committed) {
        if (QtUiTest::mousePreferred()) {
            if (!TestText::eraseTextByMouse(this)) return false;
        } else {
            if (!TestText::eraseTextByKeys(this)) return false;
        }
        expectedText = itemString;
    }

    InputWidget* iw = qtuitest_cast<InputWidget*>(inputProxy());

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    TestWidgetsLog() << iw;

    if (iw) {
        if (!TestText::enterTextByProxy(iw, q, itemString, expectedText, !noCommit)) return false;
    } else {
        if (!TestText::enterText(q, itemString, expectedText, !noCommit)) return false;
    }

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    if (!noCommit && hasEditFocus()) {
        if (!setEditFocus(false)) {
            return false;
        }
    }

    committed = !noCommit;

    return true;
}

bool TestTextEdit::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestTextEdit::ensureVisible(QTextFragment const &item)
{
    QPoint p = visualRect(item).center();

    if (q->viewport()->rect().contains(p)) {
        TestWidgetsLog() << "item is already visible";
        return true;
    }

    if (!QtUiTest::mousePreferred())
        return false;

    /* Figure out the points to click for scrolling in each direction */
    QScrollBar *vbar = q->verticalScrollBar();
    QScrollBar *hbar = q->horizontalScrollBar();
    QPoint up    = vbar->mapToGlobal(QPoint(vbar->width()/2,5));
    QPoint down  = vbar->mapToGlobal(QPoint(vbar->width()/2,vbar->height()-5));
    QPoint left  = hbar->mapToGlobal(QPoint(5,              hbar->height()/2));
    QPoint right = hbar->mapToGlobal(QPoint(hbar->width()-5,hbar->height()/2));

    // While p is above rect...
    while (p.y() < q->viewport()->rect().top()) {
        if (!vbar->isVisible()) return false;
        TestWidgetsLog() << "up" << "\nrect:" << rect() << "p:" << p;
        QtUiTest::mouseClick(up);
        waitForSignal(vbar, SIGNAL(valueChanged(int)));
        p = visualRect(item).center();
    }
    // While p is below rect...
    while (p.y() > q->viewport()->rect().bottom()) {
        if (!vbar->isVisible()) return false;
        TestWidgetsLog() << "down" << "\nrect:" << rect() << "p:" << p;
        QtUiTest::mouseClick(down);
        waitForSignal(vbar, SIGNAL(valueChanged(int)));
        p = visualRect(item).center();
    }
    // While p is left of rect...
    while (p.x() < q->viewport()->rect().left()) {
        if (!hbar->isVisible()) return false;
        TestWidgetsLog() << "left" << "\nrect:" << rect() << "p:" << p;
        QtUiTest::mouseClick(left);
        waitForSignal(hbar, SIGNAL(valueChanged(int)));
        p = visualRect(item).center();
    }
    // While p is right of rect...
    while (p.x() > q->viewport()->rect().right()) {
        if (!hbar->isVisible()) return false;
        TestWidgetsLog() << "right" << "\nrect:" << rect() << "p:" << p;
        QtUiTest::mouseClick(right);
        waitForSignal(hbar, SIGNAL(valueChanged(int)));
        p = visualRect(item).center();
    }

    if (!q->viewport()->rect().contains(p)) {
        TestWidgetsLog() << "failed" << "\nrect:" << q->viewport()->rect() << "p:" << p;
        return false;
    }

    return true;
}

bool TestTextEdit::select(QString const &item)
{
    QTextFragment fragment = fragmentForItem(item);
    if (!fragment.isValid()) return false;
    ensureVisible(fragment);
    QPoint pos = visualRect(item).center();
    QtUiTest::mouseClick(q->viewport()->mapToGlobal(pos), Qt::LeftButton);
    return true;

}

bool TestTextEdit::canWrap(QObject *o)
{ return qobject_cast<QTextEdit*>(o); }

int TestTextEdit::cursorPosition() const
{
    return q->textCursor().position();
}

void TestTextEdit::onTextChanged()
{
    if (!hasFocus() || !isVisible()) return;
    if (q->toPlainText() == lastEntered) return;
    lastEntered = q->toPlainText();
    emit entered(lastEntered);
}

void TestTextEdit::onCursorPositionChanged()
{
    int prev = lastCursorPosition;
    lastCursorPosition = cursorPosition();
    emit cursorPositionChanged(prev, lastCursorPosition);
}

}