/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTTEXTEDIT_H
#define TESTTEXTEDIT_H

#include <testwidget.h>

class QTextEdit;
class QTextFragment;

namespace QtUiTest {

class TestTextEdit : public TestWidget,
    public QtUiTest::TextWidget,
    public QtUiTest::ListWidget,
    public QtUiTest::SelectWidget
{
    Q_OBJECT
    Q_INTERFACES(
        QtUiTest::TextWidget
        QtUiTest::ListWidget
        QtUiTest::SelectWidget)

public:
    TestTextEdit(QObject*);

    virtual QString text() const;
    virtual QString selectedText() const;

    virtual QStringList list() const;
    virtual QRect visualRect(QString const&) const;

    virtual bool canEnter(QVariant const&) const;
    virtual bool enter(QVariant const&,bool);

    virtual bool canSelect(QString const&) const;
    virtual bool select(QString const&);

    static bool canWrap(QObject*);

    int cursorPosition() const;

signals:
    void cursorPositionChanged(int,int);
    void textChanged();
    void entered(QVariant const&);

protected:
    QTextFragment fragmentForItem(QString const&) const;
    virtual QRect visualRect(QTextFragment const&) const;
    virtual bool ensureVisible(QTextFragment const&);

private slots:
    void onTextChanged();
    void onCursorPositionChanged();

private:
    QString lastEntered;
    int     lastCursorPosition;
    bool    committed;
    QTextEdit *q;
};

}

#endif

