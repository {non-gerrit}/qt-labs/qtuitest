/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTWIDGETSLOG_H
#define TESTWIDGETSLOG_H

#include <QApplication>
#include <QStringList>

#include <QDebug>
#define qLog(A) if (1); else qDebug() << #A

/*
    This header provides a slight convenience layer on top of qLog.

    By using the TestWidgetsLog macro in the same way qLog would normally be used,
    a log message will be output which automatically includes the name of the current
    function.
*/

namespace TestWidgetsLogImpl
{
    /*
        Returns a log header to be output at the beginning of each TestWidgetsLog.
    */
    inline QByteArray header(char const*,int,char const* function)
    {
        QString ret = qApp->applicationName();

        // Extract just the name of the function (discard return type and arguments)
        QString id;
        QStringList l1 = QString(function).split(" ");
        QStringList l2 = l1.filter("(");
        if (l2.count()) id = l2.at(0);
        else            id = l1.at(0);
        if (id.contains("(")) id = id.left(id.indexOf("("));

        ret += " " + id;

        return ret.toLocal8Bit();
    }
};

#define TestWidgetsLog() qLog(QtUitestWidgets) << TestWidgetsLogImpl::header(__FILE__,__LINE__,Q_FUNC_INFO).constData()

#endif

