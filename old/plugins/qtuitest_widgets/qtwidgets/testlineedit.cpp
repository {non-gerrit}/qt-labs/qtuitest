/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testlineedit.h"
#include "testtext.h"
#include "testwidgetslog.h"

#include <QDesktopWidget>
#include <QLayout>
#include <QLineEdit>
#include <QPointer>
#include <QValidator>
#include <QVariant>
#include <QDateTimeEdit>

namespace QtUiTest {

TestLineEdit::TestLineEdit(QObject *_q)
    : TestGenericTextWidget(_q), q(qobject_cast<QLineEdit*>(_q))
{
    if (q && qobject_cast<QDateTimeEdit*>(q->parent())) {
        // The test object of the parent will handle recording
        return;
    }

    connect(q, SIGNAL(textEdited(QString)), this, SLOT(onTextEdited(QString)));
}

void TestLineEdit::onTextEdited(QString const& item)
{ emit entered(item); }

bool TestLineEdit::canEnter(QVariant const& item) const
{
    if (!item.canConvert<QString>()) return false;
    if (!q->validator())             return true;

    int dontcare = 0;
    QString text = item.toString();
    return (QValidator::Acceptable==q->validator()->validate(text, dontcare));
}

bool TestLineEdit::enter(QVariant const& item, bool noCommit)
{
    if (!canEnter(item)) return false;

    bool hadEditFocus = false;

    if (!hadEditFocus && !setEditFocus(true)) return false;

    using namespace QtUiTest;

    QPointer<QObject> safeThis = this;

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    QString oldText = text();
    QString selectedText = q->selectedText();
    QString itemString = item.toString();

    QString expectedText = oldText;
    if (selectedText == "") {
        expectedText.insert(q->cursorPosition(), itemString);
        // If there's text currently in the field, and we don't already have
        // edit focus, then erase it first.
        if (!oldText.isEmpty() && !hadEditFocus) {
            if (!TestText::eraseTextByKeys(q)) return false;
            expectedText = itemString;
        }
    } else {
        expectedText.replace(selectedText, itemString);
    }

    InputWidget *iw = qtuitest_cast<InputWidget*>(inputProxy());

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    TestWidgetsLog() << iw;

    if (iw) {
        if (!TestText::enterTextByProxy(iw, q, itemString, expectedText, !noCommit)) return false;
    } else {
        if (!TestText::enterText(q, itemString, expectedText, !noCommit)) return false;
    }

    if (!safeThis) {
        setErrorString("Widget was destroyed while entering text.");
        return false;
    }

    if (!noCommit && hasEditFocus()) {
        if (!setEditFocus(false)) {
            return false;
        }
    }

    return true;
}

bool TestLineEdit::canWrap(QObject *o)
{ return qobject_cast<QLineEdit*>(o); }

}