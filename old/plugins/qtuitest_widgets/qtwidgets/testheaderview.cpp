/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testheaderview.h"
#include "testwidgetslog.h"

#include <QListView>
#include <QTimer>
#include <QScrollBar>
#include <QComboBox>

#include <qtuitestnamespace.h>

namespace QtUiTest {

TestHeaderView::TestHeaderView(QObject *_q)
    : TestWidget(_q), q(static_cast<QHeaderView*>(_q))
{
}

QStringList TestHeaderView::list() const
{
    QStringList ret;

    for (int i=0; i<q->count(); ++i) {
        ret << (q->model()->headerData(i, q->orientation())).toString();
    }

    return ret;
}

QRect TestHeaderView::visualRect(QString const &item) const
{
    int index = list().indexOf(item);
    QPoint zero;
    int sectionSize = q->sectionSize(index);
    int sectionPos = q->sectionPosition(index);
    return q->orientation() == Qt::Horizontal
        ? QRect(zero.x() + sectionPos, zero.y(), sectionSize, q->height())
        : QRect(zero.x(), zero.y() + sectionPos, q->width(), sectionSize);
}

bool TestHeaderView::canWrap(QObject* o)
{ return qobject_cast<QHeaderView*>(o); }

}