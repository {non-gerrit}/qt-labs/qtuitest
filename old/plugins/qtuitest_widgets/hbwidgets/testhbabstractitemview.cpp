/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testhbabstractitemview.h"
//#include "testwidgetslog.h"

//#include <QListView>
//#include <QTimer>
//#include <QScrollBar>
//#include <QComboBox>
#include <HbAbstractViewItem>

#include <QDebug>
#include <qtuitestnamespace.h>

namespace QtUiTest {

TestHbAbstractItemView::TestHbAbstractItemView(QObject *_q)
    : TestHbWidget(_q), q(qobject_cast<HbAbstractItemView*>(_q))
{
}

QString TestHbAbstractItemView::selectedText() const
{
    return q->currentIndex().data().toString();
}

QString TestHbAbstractItemView::text() const
{
    return list().join("\n");
}

QStringList TestHbAbstractItemView::list() const
{
    class HbModelListGetter : public HbModelViewIterator<HbAbstractItemView>
    {
    public:
        HbModelListGetter(HbAbstractItemView *view)
         : HbModelViewIterator<HbAbstractItemView>(view) {};

        QStringList getList() {
            list.clear();
            iterate(view()->rootIndex());
            return list;
        }
    protected:
        virtual void visit(QModelIndex const &index)
        { list << itemForIndex(index); }

        QStringList list;
    };

    return HbModelListGetter(q).getList();
}

QModelIndex TestHbAbstractItemView::indexForItem(QString const &item) const
{
    QModelIndex ret;

    class HbModelIndexGetter : public HbModelViewIterator<HbAbstractItemView>
    {
    public:
        HbModelIndexGetter(HbAbstractItemView *view, QString const &item)
         : HbModelViewIterator<HbAbstractItemView>(view), matches(0), m_item(item) {};
        QModelIndex index;
        int matches;

    protected:
        void visit(QModelIndex const &idx) {

            if (itemForIndex(idx) == m_item) {
                ++matches;
                index = idx;
            }
        }
    private:
        QString m_item;
    };

    HbModelIndexGetter indexGetter(q, item);
    indexGetter.iterate(q->rootIndex());

    // No matching item
    if (!indexGetter.matches) {
//        TestWidgetsLog() << "no matching item for" << item;
    }

    // More than one matching item
    else if (indexGetter.matches > 1) {
        qWarning("QtUitest: more than one item matches '%s' in item view", qPrintable(item));
//        TestWidgetsLog() << indexGetter.matches << "matches for" << item;
    }

    else
        ret = indexGetter.index;

    return ret;
}

QModelIndex TestHbAbstractItemView::indexFromList(QVariantList const &list) const
{
    QVariantList indexList = list;
    QModelIndex ret;
    if (list.size() > 1) {
        int column = indexList.takeLast().toInt();
        int row = indexList.takeLast().toInt();
        ret = q->model()->index(row, column, indexFromList(indexList));
    }
    return ret;
}


QVariantList TestHbAbstractItemView::listFromIndex(QModelIndex const &index) const
{
    QVariantList ret;
    if (index.parent().isValid()) {
        ret << listFromIndex(index.parent());
    }
    if (index.isValid()) {
        ret << index.row() << index.column();
    }

    return ret;
}

QRect TestHbAbstractItemView::visualRect(QString const &item) const
{
    QRect ret;

    QModelIndex index = indexForItem(item);

    if (index.isValid()) {
        HbAbstractViewItem *vItem = q->itemByIndex(index);
        if (vItem) {
            ret = vItem->boundingRect().toRect();
//            ret.moveTopLeft(vItem->mapToScene(QPoint()));
        }
    }

    return ret;
}

bool TestHbAbstractItemView::isMultiSelection() const
{ return (q->selectionMode() > HbAbstractItemView::SingleSelection); }

bool TestHbAbstractItemView::canSelect(QString const &item) const
{
    if (q->selectionMode() == HbAbstractItemView::NoSelection)
        return false;

    return list().contains(item);
}

bool TestHbAbstractItemView::canSelectMulti(QStringList const &items) const
{
    if (!isMultiSelection())
        return false;

    QSet<QString> itemSet = items.toSet();
    return ((itemSet & list().toSet()) == itemSet);
}

bool TestHbAbstractItemView::select(QString const &item)
{
    if (!canSelect(item)) {
//        TestWidgetsLog() << "can't select" << item;
        return false;
    }

    if (!QtUiTest::mousePreferred() && (!setFocus() || !hasFocus())) {
        QtUiTest::setErrorString("Couldn't give focus to item view");
        return false;
    }

    return select(indexForItem(item));
}

bool TestHbAbstractItemView::select(QModelIndex const &index)
{
    if (QtUiTest::mousePreferred()) {
        if (!ensureVisible(index)) {
            return false;
        }
        HbAbstractViewItem *item = q->itemByIndex(index);
        QtUiTest::ActivateWidget* aw = qtuitest_cast<QtUiTest::ActivateWidget*>(item);

        if (aw) {
            return aw->activate();
        }
    }

    return false;
}

bool TestHbAbstractItemView::selectMulti(QStringList const &items)
{
    return false;
}

bool TestHbAbstractItemView::selectIndex(QVariantList const &indexList)
{
    QModelIndex idx = indexFromList(indexList);
    if (idx.isValid()) {
        return select(idx);
    }
    else {
        QtUiTest::setErrorString("Invalid index");
        return false;
    }
}

QVariantList TestHbAbstractItemView::selectedIndex() const
{
    return listFromIndex(q->currentIndex());
}

bool TestHbAbstractItemView::ensureVisible(QString const &item)
{
    return ensureVisible(indexForItem(item));
}

bool TestHbAbstractItemView::ensureVisible(QModelIndex const &index)
{
    //FIXME: QtUiTest should simulate the input events required to scroll the view
    q->scrollTo(index);
    return true;
}

bool TestHbAbstractItemView::canEnter(QVariant const& item) const
{
    bool ret = false;
    return ret;
}

bool TestHbAbstractItemView::enter(QVariant const& item, bool noCommit)
{
    bool ret = false;
    return ret;
}

bool TestHbAbstractItemView::canWrap(QObject* o)
{ return qobject_cast<HbAbstractItemView*>(o); }

}
