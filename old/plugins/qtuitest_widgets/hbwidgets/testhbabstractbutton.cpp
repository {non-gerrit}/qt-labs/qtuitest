/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testhbabstractbutton.h"

#include <HbAbstractButton>
#include <HbMainWindow>

namespace QtUiTest {

TestHbAbstractButton::TestHbAbstractButton(QObject *_q)
    : TestHbWidget(_q), q(qobject_cast<HbAbstractButton*>(_q))
{
}

Qt::CheckState TestHbAbstractButton::checkState() const
{ return q->isChecked() ? Qt::Checked : Qt::Unchecked; }

bool TestHbAbstractButton::setCheckState(Qt::CheckState state)
{
    if (state == checkState()) return true;
    if (!q->isCheckable()) {
        QtUiTest::setErrorString("This abstract button is not checkable.");
        return false;
    }
    bool ret = activate();

    if (ret && (state != checkState()) && !QtUiTest::waitForSignal(q, SIGNAL(toggled(bool)))) {
        QtUiTest::setErrorString("Successfully activated button, but check state did not change "
                "to expected value.");
        return false;
    }
    return ret;
}

bool TestHbAbstractButton::activate()
{
    HbMainWindow *mw = q->mainWindow();
    QtUiTest::mouseClick(mw->mapToGlobal(mw->mapFromScene(q->mapToScene(q->boundingRect().center()))));
    QtUiTest::wait(500);
    return true;
}

bool TestHbAbstractButton::canWrap(QObject *o)
{ return qobject_cast<HbAbstractButton*>(o); }

}