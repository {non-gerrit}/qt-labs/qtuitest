/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testhbfactory.h"

#include "testhbmainwindow.h"
#include "testhbwidget.h"
#include "testhbabstractbutton.h"
#include "testhbpushbutton.h"
#include "testhbabstractitemview.h"
#include "testhblabel.h"
#include "testhblineedit.h"
#include "testhbcombobox.h"
#include "testhbinputmethod.h"
#include "testhbabstractviewitem.h"

#include <QApplication>
#include <HbInputMethod>

namespace QtUiTest {

TestHbFactory::TestHbFactory()
{
}

QObject* TestHbFactory::find(QtUiTest::WidgetType type)
{
    if (type == InputMethod) {
        return HbInputMethod::activeInputMethod();
    }

    return 0;
}


QObject* TestHbFactory::create(QObject* o)
{
    QObject* ret = 0;

#define TRY(Klass)           \
    if (Klass::canWrap(o)) { \
        ret = new Klass(o);  \
        break;               \
    }

    /* Order is important here; classes should be listed in order of
     * most to least derived. */

    do {
        TRY(TestHbComboBox);
        TRY(TestHbLineEdit);
        TRY(TestHbPushButton);
        TRY(TestHbLabel);
        TRY(TestHbAbstractItemView);
        TRY(TestHbAbstractViewItem);
        TRY(TestHbAbstractButton);
        TRY(TestHbWidget);
        TRY(TestHbInputMethod);
        TRY(TestHbMainWindow);
    } while(0);

    return ret;
}

QStringList TestHbFactory::keys() const
{
    /* Order doesn't matter here. */
    return QStringList()
        << "HbMainWindow"
        << "HbWidget"
        << "HbLabel"
        << "HbComboBox"
        << "HbLineEdit"
        << "HbAbstractItemView"
        << "HbAbstractViewItem"
        << "HbAbstractButton"
        << "HbPushButton"
        << "HbInputMethod"
        ;
}

}
#include <qplugin.h>
Q_EXPORT_PLUGIN2(hbwidgets, QtUiTest::TestHbFactory)

