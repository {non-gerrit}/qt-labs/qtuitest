/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testhbcombobox.h"

#include <qtuitestnamespace.h>

#include <HbComboBox>
#include <HbListView>
#include <HbMainWindow>
#include <HbInputMethod>

#include <QDebug>

namespace QtUiTest {

TestHbComboBox::TestHbComboBox(QObject *_q)
    : TestHbWidget(_q), q(qobject_cast<HbComboBox*>(_q))
{
}

QString TestHbComboBox::text() const
{
    return list().join("\n");
}

QString TestHbComboBox::selectedText() const
{
    return q->currentText();
}

QStringList TestHbComboBox::list() const
{
    return q->items();
}

QRect TestHbComboBox::visualRect(QString const &item) const
{
    QRect ret;
    return ret;
}

QString TestHbComboBox::labelText() const
{
    QString ret = selectedText();
    return ret;
}

QObject* TestHbComboBox::buddy() const
{
    return q;
}

bool TestHbComboBox::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestHbComboBox::select(QString const &item)
{
    HbListView *lv = listView();
    if (!lv || !lv->QGraphicsItem::isVisible()) {
        // need to activate list view
        QGraphicsItem *button = q->primitive(HbStyle::P_ComboBoxButton_toucharea);
        HbMainWindow *mw = q->mainWindow();
        if (button && mw) {
            QtUiTest::mouseClick(mw->mapToGlobal(mw->mapFromScene(button->mapToScene(button->boundingRect().center()))));
            QtUiTest::wait(500);
        } else {
            // Something wrong here...
            return false;
        }
        if (!lv) lv = listView();
        // TODO: The click may not have activated the dropdown - eg, if another combobox had its
        // dropdown active. Need to fix this.
    }

    // At this point, the HbListView should be visible, should now call select on
    // the listview...
    QtUiTest::SelectWidget *sView = qtuitest_cast<QtUiTest::SelectWidget*>(lv);
    return sView->select(item);
}

bool TestHbComboBox::canEnter(QVariant const& item) const
{
    if (q->isEditable()) {
        if (!item.canConvert<QString>()) return false;
        return true;
/*
        int dontcare = 0;
        QString text = item.toString();
        const HbValidator *validator = q->validator();
        if (validator) {
            return (QValidator::Acceptable==validator->validate(text, dontcare));
        } else {
            return true;
        }
*/
    } else {
        return canSelect(item.toString());
    }
}

bool TestHbComboBox::enter(QVariant const& item, bool noCommit)
{
    if (q->isEditable()) {
        HbMainWindow *mw = q->mainWindow();
        QtUiTest::mouseClick(mw->mapToGlobal(mw->mapFromScene(q->mapToScene(q->boundingRect().center()))));
        QtUiTest::wait(1000);
        q->clear();
        HbInputMethod::activeInputMethod()->receiveText(item.toString());
        QtUiTest::wait(1000);
        QtUiTest::mouseClick(mw->mapToGlobal(mw->mapFromScene(q->mapToScene(QPoint(-2, 0)))));
        return true;
    } else {
        return select(item.toString());
    }
}

bool TestHbComboBox::canWrap(QObject *o)
{ return qobject_cast<HbComboBox*>(o); }

HbListView *TestHbComboBox::listView()
{
    HbListView *ret = 0;
    HbMainWindow *mw = q->mainWindow();
    if (mw) {
        foreach(QGraphicsItem *child, mw->items()) {
            HbListView *lv = qgraphicsitem_cast<HbListView*>(child);
            if (lv && lv->model() == q->model()) {
                ret = lv;
                break;
            }
        }
    }
    return ret;
}

}