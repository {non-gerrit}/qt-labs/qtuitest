SOURCES += \
        testhbmainwindow.cpp \
        testhblabel.cpp \
        testhblineedit.cpp \
        testhbcombobox.cpp \
        testhbwidget.cpp \
        testhbabstractitemview.cpp \
        testhbabstractviewitem.cpp \
        testhbabstractbutton.cpp \
        testhbpushbutton.cpp \
        testhbinputmethod.cpp \
        testhbfactory.cpp

HEADERS += \
        testhbmainwindow.h \
        testhblabel.h \
        testhblineedit.h \
        testhbcombobox.h \
        testhbwidget.h \
        testhbabstractitemview.h \
        testhbabstractviewitem.h \
        testhbabstractbutton.h \
        testhbpushbutton.h \
        testhbinputmethod.h \
        testhbfactory.h

TEMPLATE=lib
CONFIG+=plugin hb hb_install
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
TARGET=hbwidgets
TARGET=$$qtLibraryTarget($$TARGET)

include($$SRCROOT/libqtuitest/libqtuitest.pri)

target.path=$$[QT_INSTALL_PLUGINS]/qtuitest_widgets

unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
}

INSTALLS+=target
