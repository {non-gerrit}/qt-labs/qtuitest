/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativefactory.h"
#include "testdeclarativeitem.h"
#include "testdeclarativetext.h"
#include "testdeclarativetextinput.h"
#include "testdeclarativemousearea.h"
#include "testdeclarativelistview.h"
#include "testdeclarativewebview.h"
#include "testdeclarativeview.h"

#include <QDebug>

#include <QDeclarativeView>
#include <QDeclarativeItem>
#include <private/qdeclarativelistview_p.h>
#include <private/qdeclarativetext_p.h>
#include <private/qdeclarativetextinput_p.h>
#include <private/qdeclarativemousearea_p.h>

#if !defined(QT_NO_WEBKIT)
#  include <private/qdeclarativewebview_p.h>
#endif

TestQmlFactory::TestQmlFactory()
{
}

QObject* TestQmlFactory::create(QObject* o)
{
    QObject* ret = 0;

#define TRY(Type,TestType) while (!ret) { \
    Type* thing = qobject_cast<Type*>(o); \
    if (thing) ret = new TestType(thing); \
    break;                                \
}

    TRY(QDeclarativeView, TestDeclarativeView);
#if !defined(QT_NO_WEBKIT)
    TRY(QDeclarativeWebView, TestDeclarativeWebView);
#endif
    TRY(QDeclarativeListView, TestDeclarativeListView);
    TRY(QDeclarativeTextInput, TestDeclarativeTextInput);
    TRY(QDeclarativeText, TestDeclarativeText);
    TRY(QDeclarativeMouseArea, TestDeclarativeMouseArea);
    TRY(QDeclarativeItem, TestDeclarativeItem);

    return ret;
}

QStringList TestQmlFactory::keys() const
{
    return QStringList()
        << "QDeclarativeItem"
        << "QDeclarativeView"
        << "QDeclarativeListView"
        << "QDeclarativeMouseArea"
        << "QDeclarativeText"
        << "QDeclarativeTextInput"
#if !defined(QT_NO_WEBKIT)
        << "QDeclarativeWebView"
#endif
        ;
}

#include <qplugin.h>
Q_EXPORT_PLUGIN2(qmlwidgets, TestQmlFactory)
