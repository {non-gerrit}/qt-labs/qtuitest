/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativetextinput.h"

#include <private/qdeclarativetextinput_p.h>

TestDeclarativeTextInput::TestDeclarativeTextInput(QDeclarativeTextInput* _q)
    : TestDeclarativeItem(_q)
    , q(_q)
{}

QString TestDeclarativeTextInput::text() const
{ return q->text(); }

QString TestDeclarativeTextInput::selectedText() const
{ return text(); }

bool TestDeclarativeTextInput::canEnter(QVariant const& item) const
{
    if (!item.canConvert<QString>()) return false;
    if (!q->validator())             return true;

    int dontcare = 0;
    QString text = item.toString();
    return (QValidator::Acceptable==q->validator()->validate(text, dontcare));
}

bool TestDeclarativeTextInput::enter(QVariant const& item, bool noCommit)
{
    if (!canEnter(item)) return false;
    QString itemString = item.toString();
    using namespace QtUiTest;

    if (!q->hasFocus()) {
        QPoint p = visibleRegion().boundingRect().center();
        ensureVisiblePoint(p);
        QtUiTest::mouseClick(mapToGlobal(p), Qt::LeftButton);
        QtUiTest::waitForSignal(q, SIGNAL(focusChanged()));
        if (!q->hasFocus()) {
            QtUiTest::setErrorString("Couldn't give focus to TextInput");
            return false;
        }
    }

    foreach (QChar const& c, itemString) {
        if (!keyClick(q, SIGNAL(textChanged()), asciiToKey(c.toLatin1()), asciiToModifiers(c.toLatin1()) ))
            return false;
    }
    if (!noCommit) QtUiTest::keyClick(QtUiTest::Key_Activate);

    return true;
}
