/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativewebview.h"

#include <private/qdeclarativewebview_p.h>
#include <private/qdeclarativeflickable_p.h>

#include <QWebPage>
#include <QWebFrame>
#include <QWebElementCollection>

TestDeclarativeWebView::TestDeclarativeWebView(QDeclarativeWebView *_q)
    : TestDeclarativeItem(_q), q(_q)
{
}

QString TestDeclarativeWebView::text() const
{
    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        return page->mainFrame()->toPlainText();
    }
    return QString();
}

QString TestDeclarativeWebView::selectedText() const
{
    return text();
}

QStringList TestDeclarativeWebView::list() const
{
    QStringList ret;

    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        QWebElementCollection links = page->mainFrame()->findAllElements("a[href]");
        foreach (QWebElement el, links) {
            QString str = el.toPlainText();
            if (!str.isEmpty()) ret << str;
        }
    }
    return ret;
}

QWebElement TestDeclarativeWebView::elementForItem(QString const &item) const
{
    QWebElement ret;
    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        QWebElementCollection links = page->mainFrame()->findAllElements("a[href]");
        foreach (QWebElement el, links) {
            if (el.toPlainText() == item) {
                ret = el;
                break;
            }
        }
    }
    return ret;
}

QRect TestDeclarativeWebView::visualRect(QString const &item) const
{
    return visualRect(elementForItem(item));
}

QRect TestDeclarativeWebView::visualRect(QWebElement const &el) const
{
    if (!el.isNull()) {
        QRect geo = el.geometry();
        qreal scale = q->contentsScale();
        return QRect(geo.x() * scale, geo.y() * scale, geo.width() * scale, geo.height() * scale);
    }
    return QRect();
}

bool TestDeclarativeWebView::canSelect(QString const &item) const
{ return list().contains(item); }

bool TestDeclarativeWebView::select(QString const &item)
{
    QWebElement el(elementForItem(item));
    if (el.isNull()) return false;

    QRect rect = visualRect(el);
    QPoint pos = rect.center();

    if (q->parentItem()) {
        QDeclarativeFlickable *flick = qobject_cast<QDeclarativeFlickable*>(q->parentItem()->parentItem());

/*
    qDebug() << "Viewport X     " << flick->viewportX();
    qDebug() << "Viewport Y     " << flick->viewportY();
    qDebug() << "Viewport Width " << flick->viewportWidth();
    qDebug() << "Viewport Height" << flick->viewportHeight();
    qDebug() << "Bottom         " << flick->viewportY() + flick->height() + q->y();
    qDebug() << "Item at        " << pos;
*/
        if (flick && (flick->contentY() + q->y() > pos.y() ||
            flick->contentY() + flick->height() + q->y() < pos.y())) {
      //      qDebug() << "    *** MOVING VIEWPORT ***";
            flick->setContentY(pos.y() - flick->height()/2);
        }
    }

//    qDebug() << "Select:" << item << "by clicking at" << pos << mapToGlobal(pos);

    QtUiTest::mouseClick(mapToGlobal(pos), Qt::LeftButton);

    return true;
}

bool TestDeclarativeWebView::canEnter(QVariant const& item) const
{
    // Accept everything for now...
    return true;
}

bool TestDeclarativeWebView::enter(QVariant const& item, bool noCommit)
{
    QString itemString = item.toString();
    foreach (QChar const& c, itemString) {
        QtUiTest::keyClick(QtUiTest::asciiToKey(c.toLatin1()), QtUiTest::asciiToModifiers(c.toLatin1()));
    }
    if (!noCommit) QtUiTest::keyClick(QtUiTest::Key_Activate);

    return true;
}

bool TestDeclarativeWebView::selectIndex(QVariantList const &indexList)
{
    QWebElement el;
    QWebPage *page = q->page();
    if (page && page->mainFrame()) {
        if (indexList.size() > 1) {
            bool ok;
            int elIndex = indexList[1].toInt(&ok);
            if (!ok) {
                QtUiTest::setErrorString("Element index must be an integer");
                return false;
            }
            QWebElementCollection els = page->mainFrame()->findAllElements(indexList[0].toString());
            if (els.count() < (elIndex-1)) {
                QtUiTest::setErrorString("Element index out of range");
                return false;
            }
            el = els[elIndex];
        } else if (indexList.size() == 1) {
            el = page->mainFrame()->findFirstElement(indexList[0].toString());
        } else return false;
    }

    if (el.isNull()) return false;

    QRect rect = visualRect(el);
    QPoint pos = rect.center();

    if (q->parentItem()) {
        QDeclarativeFlickable *flick = qobject_cast<QDeclarativeFlickable*>(q->parentItem()->parentItem());

        if (flick && (flick->contentY() + q->y() > pos.y() ||
            flick->contentY() + flick->height() + q->y() < pos.y())) {
            flick->setContentY(pos.y() - flick->height()/2);
        }
    }

    QtUiTest::mouseClick(mapToGlobal(pos), Qt::LeftButton);

    return true;
}