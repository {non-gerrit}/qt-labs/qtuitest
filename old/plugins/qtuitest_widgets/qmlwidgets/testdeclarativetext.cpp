/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativetext.h"

#include <private/qdeclarativetext_p.h>

TestDeclarativeText::TestDeclarativeText(QDeclarativeText* _q)
    : TestDeclarativeItem(_q)
    , q(_q)
{}

QString TestDeclarativeText::text() const
{ return q->text(); }

QString TestDeclarativeText::selectedText() const
{ return text(); }

QString TestDeclarativeText::labelText() const
{ return text(); }

bool TestDeclarativeText::isLabel() const
{ return true; }

// For QML items, assume that the text is a label for the parent.
QObject* TestDeclarativeText::buddy() const
{
    QObject *parent = TestDeclarativeItem::parent();
    QtUiTest::Widget *pw = qtuitest_cast<Widget*>(parent);
    if (pw)
        return parent;
    else
        return 0;
}

bool TestDeclarativeText::implements(char const* interface) const
{
    if (interface == QLatin1String("LabelWidget")) return isLabel();
    return TestDeclarativeItem::implements(interface);
}

