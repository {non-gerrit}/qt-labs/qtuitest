/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TEST_DECLARATIVE_WEBVIEW_H
#define TEST_DECLARATIVE_WEBVIEW_H

#include "testdeclarativeitem.h"

class QDeclarativeWebView;
class QWebElement;

class TestDeclarativeWebView : public TestDeclarativeItem,
    public QtUiTest::IndexedWidget
{
    Q_OBJECT
    Q_INTERFACES(
        QtUiTest::TextWidget
        QtUiTest::ListWidget
        QtUiTest::SelectWidget
        QtUiTest::InputWidget
        QtUiTest::IndexedWidget)

public:
    TestDeclarativeWebView(QDeclarativeWebView*);

    virtual QString text() const;
    virtual QString selectedText() const;

    virtual QStringList list() const;
    virtual QRect visualRect(QString const&) const;

    virtual bool canSelect(QString const&) const;
    virtual bool select(QString const&);

    virtual bool canEnter(QVariant const&) const;
    virtual bool enter(QVariant const&,bool);

    virtual bool selectIndex(QVariantList const&);


protected:
    QWebElement elementForItem(QString const&) const;
    virtual QRect visualRect(QWebElement const&) const;

private:
    QDeclarativeWebView *q;
};

#endif

