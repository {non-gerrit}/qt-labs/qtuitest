/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativelistview.h"
#include <private/qdeclarativelistview_p.h>
#include <private/qlistmodelinterface_p.h>


TestDeclarativeListView::TestDeclarativeListView(QDeclarativeListView *_q)
    : TestDeclarativeItem(_q)
    , q(_q)
{}

QString TestDeclarativeListView::selectedText() const
{
//    waitForModelUpdate(q->model());
    return getData(q->currentIndex());
}


QString TestDeclarativeListView::getData(int index) const
{
    QListModelInterface *model = qobject_cast<QListModelInterface*>(q->model().value<QObject*>());
//    QListModelInterface *model = qobject_cast<QListModelInterface*>(q->model());
    if (model) {
        QStringList sl;
        QList<int> roles = model->roles();
        QHash<int,QVariant> data = model->data(index, roles);
        foreach (QVariant value, data)
            sl << value.toString();
        return sl.join(" ");
    }
    return QString();
}

QString TestDeclarativeListView::text() const
{
    return list().join("\n");
}

QStringList TestDeclarativeListView::list() const
{
    QStringList ret;
    QListModelInterface *model = qobject_cast<QListModelInterface*>(q->model().value<QObject*>());
//    QListModelInterface *model = qobject_cast<QListModelInterface*>(q->model());

    for (int i = 0; i < model->count(); i++) {
        ret << getData(i);
    }

    return ret;
}

QRect TestDeclarativeListView::visualRect(QString const &item) const
{
    using namespace QtUiTest;

    QRect ret;
/*
    // Allow testwidgets to make decisions based on the view associated with this item.
//    QVariant view = QVariant::fromValue((QObject*)q);
//    q->model()->setProperty("_q_qtuitest_itemview", view);
    ListWidget* lw = qtuitest_cast<ListWidget*>(q->model());
    if (!lw) {
        QString model;
        QDebug(&model) << q->model();
        setErrorString("Could not find a ListWidget interface for model " + model);
    } else {
        ret = lw->visualRect(item);
    }
*/
    return ret;
}

bool TestDeclarativeListView::isMultiSelection() const
{
    return false;
}

bool TestDeclarativeListView::canSelect(QString const &item) const
{
    return (list().contains(item));
}

bool TestDeclarativeListView::canSelectMulti(QStringList const &items) const
{
    return false;
}

bool TestDeclarativeListView::select(QString const &item)
{
    if (!canSelect(item)) {
        return false;
    }

    // FIXME: Shouldn't set focus this way...
    q->setFocus(true);
/*

    //FIXME: Should support both mouse and keyboard

        // Consume pending key events, if any.
        while (QtUiTest::waitForEvent(q, QEvent::KeyRelease, 200, Qt::QueuedConnection))
        {}

        QStringList allItems = list();

        const int maxtries = 100;
        int desiredIndex = allItems.indexOf(item);
        int currentIndex = q->currentIndex();

        // Move vertically
        Qt::Key key;
        if (desiredIndex > currentIndex)
            key = (q->orientation() == Qt::Vertical) ? Qt::Key_Down : Qt::Key_Right;
        else
            key = (q->orientation() == Qt::Vertical) ? Qt::Key_Up : Qt::Key_Left;

        for (int i = 0; i < maxtries && selectedText() != item; ++i) {
            if (!QtUiTest::keyClick(q->canvas(), key)) return false;
QtUiTest::wait(200);
        }
        QString selected = selectedText();
        qDebug() << "selectedText() now" << selected;
        if (selected != item) {
            QtUiTest::setErrorString(QString(
                "Up/down keys should have caused item %1 to become selected, but item %2 "
                "is selected instead.").arg(item).arg(selected));
            return false;
        }
        return true;
*/

    return false;

}

bool TestDeclarativeListView::selectMulti(QStringList const &items)
{
    return false;
}

bool TestDeclarativeListView::ensureVisible(QString const &item)
{
    return true;
}

bool TestDeclarativeListView::canWrap(QObject* o)
{ return qobject_cast<QDeclarativeListView*>(o); }

