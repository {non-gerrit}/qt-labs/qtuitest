/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testdeclarativeitem.h"

#include <QDeclarativeItem>
#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QDeclarativeListProperty>
#include <QScriptValue>
#include <QMetaObject>
#include <QMetaProperty>
#include <private/qdeclarativemetatype_p.h>
#include <QDebug>

TestDeclarativeItem::TestDeclarativeItem(QDeclarativeItem* _q)
    : QtUiTest::TestGraphicsItem(_q),
      q(_q),
      m_childActivate(false)
{
}

const QObjectList& TestDeclarativeItem::children() const
{
    m_children.clear();

    foreach (QObject *child, q->children()) {
        if (qobject_cast<QDeclarativeItem*>(child))
            m_children << child;
    }

    return m_children;
}

bool TestDeclarativeItem::implements(char const* interface) const
{
    if (interface == QLatin1String("ActivateWidget")) {
        m_childActivate = 0;
        foreach (QObject *child, children()) {
            if (qtuitest_cast<QtUiTest::ActivateWidget*>(child)) {
                m_childActivate = child;
                return true;
            }
        }
        return false;
    }
    return (interface == QLatin1String("Widget"));
}

static const uint qt_meta_data_TestDeclarativeItem[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eodinherits
};

static const char qt_meta_stringdata_TestDeclarativeItem[] = {
    "TestDeclarativeItem\0"
};

const QMetaObject TestDeclarativeItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TestDeclarativeItem,
      qt_meta_data_TestDeclarativeItem, 0 }
};

const QMetaObject *TestDeclarativeItem::metaObject() const
{
    return &staticMetaObject;
}

void *TestDeclarativeItem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;

    void* ret = 0;

    do {
#define DO(Type,String)                                                                      \
    if (!strcmp(_clname,#String) && implements(static_cast<Type*>(0)->_q_interfaceName())) { \
        ret = static_cast<Type*>(this);                                                      \
        break;                                                                               \
    }

        DO(QtUiTest::Widget, QtUiTest::Widget);
        DO(QtUiTest::Widget, com.nokia.qt.QtUiTest.Widget/1.0);
        DO(QtUiTest::CheckWidget, QtUiTest::CheckWidget);
        DO(QtUiTest::CheckWidget, com.nokia.qt.QtUiTest.CheckWidget/1.0);
        DO(QtUiTest::ActivateWidget, QtUiTest::ActivateWidget);
        DO(QtUiTest::ActivateWidget, com.nokia.qt.QtUiTest.ActivateWidget/1.0);
        DO(QtUiTest::LabelWidget, QtUiTest::LabelWidget);
        DO(QtUiTest::LabelWidget, com.nokia.qt.QtUiTest.LabelWidget/1.0);
        DO(QtUiTest::TextWidget, QtUiTest::TextWidget);
        DO(QtUiTest::TextWidget, com.nokia.qt.QtUiTest.TextWidget/1.0);
        DO(QtUiTest::ListWidget, QtUiTest::ListWidget);
        DO(QtUiTest::ListWidget, com.nokia.qt.QtUiTest.ListWidget/1.0);
        DO(QtUiTest::InputWidget, QtUiTest::InputWidget);
        DO(QtUiTest::InputWidget, com.nokia.qt.QtUiTest.InputWidget/1.0);
        DO(QtUiTest::SelectWidget, QtUiTest::SelectWidget);
        DO(QtUiTest::SelectWidget, com.nokia.qt.QtUiTest.SelectWidget/1.0);
#undef DO
    } while(0);

    if (!ret) ret = QObject::qt_metacast(_clname);

    return ret;
}

/*************************************************************************************************/

static const QString TestDeclarativeItemSubclassError(
    "Internal error: TestDeclarativeItem::%1() called.  Either %2::implements() is wrong or %2 forgot to override %1."
);

#define SETERROR() \
    QtUiTest::setErrorString(TestDeclarativeItemSubclassError.arg(__FUNCTION__).arg(metaObject()->className()))

bool TestDeclarativeItem::activate()
{
    if (QtUiTest::ActivateWidget *aw = qtuitest_cast<QtUiTest::ActivateWidget*>(m_childActivate)) {
        return aw->activate();
    }

    return false;
}

QString TestDeclarativeItem::labelText() const
{ SETERROR(); return QString(); }
bool TestDeclarativeItem::isTristate() const
{ SETERROR(); return false; }
Qt::CheckState TestDeclarativeItem::checkState() const
{ SETERROR(); return Qt::CheckState(0); }
bool TestDeclarativeItem::setCheckState(Qt::CheckState)
{ SETERROR(); return false; }
QString TestDeclarativeItem::selectedText() const
{ SETERROR(); return QString(); }
QString TestDeclarativeItem::text() const
{ SETERROR(); return QString(); }
QStringList TestDeclarativeItem::list() const
{ SETERROR(); return QStringList(); }
QRect TestDeclarativeItem::visualRect(const QString&) const
{ SETERROR(); return QRect(); }
bool TestDeclarativeItem::ensureVisible(const QString&)
{ SETERROR(); return false; }
bool TestDeclarativeItem::canEnter(const QVariant&) const
{ SETERROR(); return false; }
bool TestDeclarativeItem::enter(const QVariant&,bool)
{ SETERROR(); return false; }
bool TestDeclarativeItem::isMultiSelection() const
{ SETERROR(); return false; }
bool TestDeclarativeItem::canSelect(const QString&) const
{ SETERROR(); return false; }
bool TestDeclarativeItem::canSelectMulti(const QStringList&) const
{ SETERROR(); return false; }
bool TestDeclarativeItem::select(const QString&)
{ SETERROR(); return false; }
bool TestDeclarativeItem::selectMulti(const QStringList&)
{ SETERROR(); return false; }



///////

bool TestDeclarativeItem::isVisible() const
{
    return q->isVisible();
}

QObject* TestDeclarativeItem::parent() const
{ return q->parentItem() ? q->parentItem() : TestGraphicsItem::parent(); }

bool TestDeclarativeItem::ensureVisibleRegion(const QRegion&)
{ return true; }

bool TestDeclarativeItem::hasFocus() const
{ return q->hasFocus(); }

QString unSlash(const QString &text)
{
    int lastSlash = text.lastIndexOf(QLatin1Char('/'));
    if (lastSlash != -1)
        return text.mid(lastSlash+1);
    else
        return text;
}

QVariant TestDeclarativeItem::getProperty(const QString& name) const
{
    QVariant ret;

    if (name.toUpper() == "QMLTYPE") {
        QString typeName;
        QDeclarativeType *type = QDeclarativeMetaType::qmlType(q->metaObject());
        if (type) {
            typeName = QLatin1String(type->qmlTypeName());
        } else {
            typeName = QString::fromUtf8(q->metaObject()->className());
            int marker = typeName.indexOf(QLatin1String("_QMLTYPE_"));
            if (marker != -1)
                typeName = typeName.left(marker);
            else {
                marker = typeName.indexOf(QLatin1String("_QML_"));
                if (marker != -1)
                    typeName = typeName.left(marker);

                foreach (QDeclarativeType* type, QDeclarativeMetaType::qmlTypes()) {
                    if (type->typeName() == typeName) {
                        typeName = QLatin1String(type->qmlTypeName());
                        break;
                    }
                }
            }
        }

        if (!typeName.isNull()) {
            int lastSlash = typeName.lastIndexOf(QLatin1Char('/'));
            if (lastSlash != -1)
                typeName = typeName.mid(lastSlash+1);

            return typeName;
        }
    } else if (name.contains('.')) {
        // This is an attached property
        int dot = name.indexOf('.');
        QString attachTypeName = name.left(dot);
        QDeclarativeType *attachType = 0;
        foreach (QDeclarativeType* type, QDeclarativeMetaType::qmlTypes()) {
            if (unSlash(type->qmlTypeName()) == attachTypeName) {
                attachType = type;
                break;
            }
        }
        if (attachType) {
            QDeclarativeAttachedPropertiesFunc func = attachType->attachedPropertiesFunction();
            if (func) {
                QObject *attached = func(q);
                if (attached) {
                    ret = attached->property(name.mid(dot+1).toLatin1());
                }
            }
        }
    } else {
        ret = q->property(name.toLatin1());
    }

    if (ret.type() == QVariant::UserType) {
        bool ok;
        QObject *obj = QDeclarativeMetaType::toQObject(ret, &ok);
        if (ok) {
            return QVariant::fromValue(obj);
        }
        return QVariant();
    }

    return ret;
}
