SOURCES +=                            \
        testdeclarativefactory.cpp    \
        testdeclarativeitem.cpp       \
        testdeclarativelistview.cpp   \
        testdeclarativetext.cpp       \
        testdeclarativetextinput.cpp  \
        testdeclarativeview.cpp       \
        testdeclarativemousearea.cpp

HEADERS +=                            \
        testdeclarativefactory.h      \
        testdeclarativeitem.h         \
        testdeclarativelistview.h     \
        testdeclarativetext.h         \
        testdeclarativetextinput.h    \
        testdeclarativeview.h         \
        testdeclarativemousearea.h

TEMPLATE=lib
CONFIG+=plugin

MOC_DIR=$$OUT_PWD/.moc
OBJECTS_DIR=$$OUT_PWD/.obj
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
INCLUDEPATH+=$$SRCROOT/libqtwidgets
QT+=declarative script opengl
TARGET=qmlwidgets
INCLUDEPATH+=$(HOME)/opengles2/SDKPackage/Builds/OGLES2/Include

include($$SRCROOT/libqtuitest/libqtuitest.pri)

unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
    target.path=$$[QT_INSTALL_PLUGINS]/qtuitest_widgets
    INSTALLS+=target
}

win32 {
    target.path=$$[QT_INSTALL_PLUGINS]/qtuitest_widgets
    INSTALLS+=target
}

contains(QT_CONFIG, webkit) {
    QT+=webkit
    SOURCES+=testdeclarativewebview.cpp
    HEADERS+=testdeclarativewebview.h
}

