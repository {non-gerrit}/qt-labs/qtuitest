/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testgraphicsviewfactory.h"

#include "testgraphicsproxywidget.h"
#include "testgraphicstextitem.h"
#include "testgraphicsview.h"
#include "testgraphicswidget.h"

//#include "testwidgetslog.h"

#include <QApplication>

namespace QtUiTest {

TestGraphicsViewFactory::TestGraphicsViewFactory()
{
}

QObject* TestGraphicsViewFactory::create(QObject* o)
{
    QObject* ret = 0;

#define TRY(Klass)           \
    if (Klass::canWrap(o)) { \
        ret = new Klass(o);  \
        break;               \
    }

    /* Order is important here; classes should be listed in order of
     * most to least derived. */

    do {
        /* Qt Graphics View items */
        TRY(TestGraphicsTextItem);
        TRY(TestGraphicsProxyWidget);
        TRY(TestGraphicsWidget);
        TRY(TestGraphicsView);
//        TRY(TestGraphicsItem);
    } while(0);

//    TestWidgetsLog() << o << ret;

    return ret;
}

QStringList TestGraphicsViewFactory::keys() const
{
    /* Order doesn't matter here. */
    return QStringList()
        << "QGraphicsTextItem"
        << "QGraphicsProxyWidget"
        << "QGraphicsView"
        << "QGraphicsWidget"
        ;
}

QObject* TestGraphicsViewFactory::supportedGraphicsItem(QGraphicsItem* item)
{
    QObject *ret = 0;
    if ( (ret = qgraphicsitem_cast<QGraphicsTextItem*>(item)) ) {return ret;}
    if ( (ret = qobject_cast<QObject*>(qgraphicsitem_cast<QGraphicsProxyWidget*>(item))) ) {return ret;}
    if ( (ret = qgraphicsitem_cast<QGraphicsWidget*>(item)) ) {return ret;}
    return ret;
}

}
#include <qplugin.h>
Q_EXPORT_PLUGIN2(qtgraphicsviewwidgets, QtUiTest::TestGraphicsViewFactory)

