/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QGraphicsProxyWidget>
#include "testgraphicsproxywidget.h"
#include <QWidget>

namespace QtUiTest {

TestGraphicsProxyWidget::TestGraphicsProxyWidget(QObject* _q)
    : TestGraphicsItem(static_cast<QGraphicsProxyWidget*>(_q))
    , q(static_cast<QGraphicsProxyWidget*>(_q))
{}

const QObjectList& TestGraphicsProxyWidget::children() const
{
    m_children.clear();
    m_children << qobject_cast<QObject*>(q->widget());
    return m_children;
}

bool TestGraphicsProxyWidget::canWrap(QObject *o)
{ return qobject_cast<QGraphicsProxyWidget*>(o); }
/*
static const uint qt_meta_data_TestGraphicsProxyWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eod
};

static const char qt_meta_stringdata_TestGraphicsProxyWidget[] = {
    "TestFxItem\0"
};

const QMetaObject TestGraphicsProxyWidget::staticMetaObject = {
    { &TestGraphicsItem::staticMetaObject, qt_meta_stringdata_TestGraphicsProxyWidget,
      qt_meta_data_TestGraphicsProxyWidget, 0 }
};

const QMetaObject *TestGraphicsProxyWidget::metaObject() const
{
    return &staticMetaObject;
}

void *TestGraphicsProxyWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;

    void* ret = 0;

    do {
#define DO(Type,String)                                                  \
    if (!strcmp(_clname,#String) && qtuitest_cast<Type*>(q->widget())) { \
        ret = static_cast<Type*>(this);                                  \
        break;                                                           \
    }
        // No need for Widget - TestGfxCanvasItem already handles it.
        //DO(QtUiTest::Widget, QtUiTest::Widget);
        //DO(QtUiTest::Widget, com.nokia.qt.QtUiTest.Widget/1.0);
        DO(QtUiTest::CheckWidget, QtUiTest::CheckWidget);
        DO(QtUiTest::CheckWidget, com.nokia.qt.QtUiTest.CheckWidget/1.0);
        DO(QtUiTest::ActivateWidget, QtUiTest::ActivateWidget);
        DO(QtUiTest::ActivateWidget, com.nokia.qt.QtUiTest.ActivateWidget/1.0);
        DO(QtUiTest::LabelWidget, QtUiTest::LabelWidget);
        DO(QtUiTest::LabelWidget, com.nokia.qt.QtUiTest.LabelWidget/1.0);
        DO(QtUiTest::TextWidget, QtUiTest::TextWidget);
        DO(QtUiTest::TextWidget, com.nokia.qt.QtUiTest.TextWidget/1.0);
        DO(QtUiTest::ListWidget, QtUiTest::ListWidget);
        DO(QtUiTest::ListWidget, com.nokia.qt.QtUiTest.ListWidget/1.0);
        DO(QtUiTest::InputWidget, QtUiTest::InputWidget);
        DO(QtUiTest::InputWidget, com.nokia.qt.QtUiTest.InputWidget/1.0);
        DO(QtUiTest::SelectWidget, QtUiTest::SelectWidget);
        DO(QtUiTest::SelectWidget, com.nokia.qt.QtUiTest.SelectWidget/1.0);
#undef DO
    } while(0);

    if (!ret) ret = TestGraphicsItem::qt_metacast(_clname);

    return ret;
}

bool TestGraphicsProxyWidget::activate()
{
    QtUiTest::ActivateWidget *w = qtuitest_cast<QtUiTest::ActivateWidget*>(q->widget());
    if (w) return w->activate();
    return false;
}

QString TestGraphicsProxyWidget::labelText() const
{
    QtUiTest::LabelWidget *w = qtuitest_cast<QtUiTest::LabelWidget*>(q->widget());
    if (w) return w->labelText();
    return QString();
}

QObject* TestGraphicsProxyWidget::buddy() const
{
    QObject *ret = 0;
    QtUiTest::LabelWidget *w = qtuitest_cast<QtUiTest::LabelWidget*>(q->widget());
    if (w) ret = w->buddy();
    if (ret == q->widget()) ret = q;
    return ret;
}

bool TestGraphicsProxyWidget::isTristate() const
{
    QtUiTest::CheckWidget *w = qtuitest_cast<QtUiTest::CheckWidget*>(q->widget());
    if (w) return w->isTristate();
    return false;
}

Qt::CheckState TestGraphicsProxyWidget::checkState() const
{
    QtUiTest::CheckWidget *w = qtuitest_cast<QtUiTest::CheckWidget*>(q->widget());
    if (w) return w->checkState();
    return Qt::CheckState(0);
}

bool TestGraphicsProxyWidget::setCheckState(Qt::CheckState state)
{
    QtUiTest::CheckWidget *w = qtuitest_cast<QtUiTest::CheckWidget*>(q->widget());
    if (w) return w->setCheckState(state);
    return false;
}

QString TestGraphicsProxyWidget::selectedText() const
{
    QtUiTest::TextWidget *w = qtuitest_cast<QtUiTest::TextWidget*>(q->widget());
    if (w) return w->selectedText();
    return QString();
}

QString TestGraphicsProxyWidget::text() const
{
    QtUiTest::TextWidget *w = qtuitest_cast<QtUiTest::TextWidget*>(q->widget());
    if (w) return w->text();
    return QString();
}

QStringList TestGraphicsProxyWidget::list() const
{
    QtUiTest::ListWidget *w = qtuitest_cast<QtUiTest::ListWidget*>(q->widget());
    if (w) return w->list();
    return QStringList();
}

QRect TestGraphicsProxyWidget::visualRect(const QString& item) const
{
    QtUiTest::ListWidget *w = qtuitest_cast<QtUiTest::ListWidget*>(q->widget());
    if (w) return w->visualRect(item);
    return QRect();
}

bool TestGraphicsProxyWidget::ensureVisible(const QString& item)
{
    QtUiTest::ListWidget *w = qtuitest_cast<QtUiTest::ListWidget*>(q->widget());
    if (w) return w->ensureVisible(item);
    return false;
}

bool TestGraphicsProxyWidget::canEnter(const QVariant& value) const
{
    QtUiTest::InputWidget *w = qtuitest_cast<QtUiTest::InputWidget*>(q->widget());
    if (w) return w->canEnter(value);
    return false;
}

bool TestGraphicsProxyWidget::enter(const QVariant& value, bool noCommit)
{
    QtUiTest::InputWidget *w = qtuitest_cast<QtUiTest::InputWidget*>(q->widget());
    if (w) return w->enter(value, noCommit);
    return false;
}

bool TestGraphicsProxyWidget::isMultiSelection() const
{
    QtUiTest::SelectWidget *w = qtuitest_cast<QtUiTest::SelectWidget*>(q->widget());
    if (w) return w->isMultiSelection();
    return false;
}

bool TestGraphicsProxyWidget::canSelect(const QString& item) const
{
    QtUiTest::SelectWidget *w = qtuitest_cast<QtUiTest::SelectWidget*>(q->widget());
    if (w) return w->canSelect(item);
    return false;
}

bool TestGraphicsProxyWidget::canSelectMulti(const QStringList& items) const
{
    QtUiTest::SelectWidget *w = qtuitest_cast<QtUiTest::SelectWidget*>(q->widget());
    if (w) return w->canSelectMulti(items);
    return false;
}

bool TestGraphicsProxyWidget::select(const QString& item)
{
    QtUiTest::SelectWidget *w = qtuitest_cast<QtUiTest::SelectWidget*>(q->widget());
    if (w) return w->select(item);
    return false;
}

bool TestGraphicsProxyWidget::selectMulti(const QStringList& items)
{
    QtUiTest::SelectWidget *w = qtuitest_cast<QtUiTest::SelectWidget*>(q->widget());
    if (w) return w->selectMulti(items);
    return false;
}
*/

}