SOURCES += \
        testgraphicsitem.cpp \
        testgraphicsproxywidget.cpp \
        testgraphicstextitem.cpp \
        testgraphicswidget.cpp \
        testgraphicsview.cpp \
        testgraphicsviewfactory.cpp

HEADERS += \
        testgraphicsitem.h \
        testgraphicsproxywidget.h \
        testgraphicstextitem.h \
        testgraphicswidget.h \
        testgraphicsview.h \
        testgraphicsviewfactory.h

TEMPLATE=lib
CONFIG+=plugin
VPATH+=$$PWD
INCLUDEPATH+=$$PWD
TARGET=qgraphicsviewwidgets
TARGET=$$qtLibraryTarget($$TARGET)

include($$SRCROOT/libqtuitest/libqtuitest.pri)

target.path=$$[QT_INSTALL_PLUGINS]/qtuitest_widgets

unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
}
INSTALLS += target
