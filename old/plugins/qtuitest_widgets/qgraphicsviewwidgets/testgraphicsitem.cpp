/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testgraphicsitem.h"
#include "testgraphicsviewfactory.h"
#include <qgraphicsview.h>
#include <QDebug>

namespace QtUiTest {

TestGraphicsItem::TestGraphicsItem(QGraphicsItem* _q)
    : q(_q)
{}

// As the item may be transformed (eg, rotated relative to view) the
// area of the bounding rect may be considerably larger than that of the item.
const QRect& TestGraphicsItem::geometry() const
{
    QGraphicsView *view = q->scene()->views().first();
    m_geometry = view->mapFromScene(q->sceneBoundingRect()).boundingRect();

    return m_geometry;
}

bool TestGraphicsItem::isVisible() const
{
    return q->isVisibleTo(0);
}

QRegion TestGraphicsItem::visibleRegion() const
{
    //FIXME 

    QRegion ret;

    if (isVisible()) {
        ret = q->boundingRegion(QTransform());
    }

    return ret;
}

const QObjectList& TestGraphicsItem::children() const
{
    m_children.clear();
    foreach(QGraphicsItem *child, q->childItems()) {
        QObject *obj = TestGraphicsViewFactory::supportedGraphicsItem(child);
        if (obj)
            m_children << obj;
    }
    return m_children;
}

QObject* TestGraphicsItem::parent() const
{
// return QObject::parent(); 
    return q->scene()->views().first();
}

QPoint TestGraphicsItem::mapToGlobal(const QPoint& pos) const
{
    QPointF posf(pos);
    QGraphicsView *view = q->scene()->views().first();
    return view->mapToGlobal(view->mapFromScene(q->mapToScene(posf)));
}

QPoint TestGraphicsItem::mapFromGlobal(const QPoint& pos) const
{
    QGraphicsView *view = q->scene()->views().first();
    return q->mapFromScene(view->mapToScene(pos)).toPoint();
}

bool TestGraphicsItem::ensureVisibleRegion(const QRegion&)
{ return true; }

bool TestGraphicsItem::hasFocus() const
{ return q->hasFocus(); }

#ifdef Q_WS_QWS
bool TestGraphicsItem::hasEditFocus() const
{ return hasFocus(); }

bool TestGraphicsItem::setEditFocus(bool)
{ return false; }
#endif

bool TestGraphicsItem::grabPixmap(QPixmap &pixmap) const
{
    QGraphicsView *view = q->scene()->views().first();
    pixmap = QPixmap::grabWidget( view, geometry() );
    return true;
}

}