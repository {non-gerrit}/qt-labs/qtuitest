/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include <QWidget>
#include <QDebug>
#include <qtestslave.h>
#include "style.h"
#include <QCoreApplication>
#ifdef Q_OS_SYMBIAN
#  include <QS60Style>
#  include <sys/socket.h>
#  include <net/if.h>
#elif defined Q_OS_MAC
#  include <QMacStyle>
#endif

#if defined(Q_WS_WIN) || defined(Q_WS_MAC)
extern Q_GUI_EXPORT bool qt_use_native_dialogs;
#endif

struct TestSlaveServer : public QTcpServer
{
    TestSlaveServer()
        : QTcpServer(), showDebug(false), started(false)
    {
    }

    ~TestSlaveServer()
    {
        close();
    }

    void incomingConnection(int sd)
    { slave.setSocket(sd); }

    void startService();
    void stopService();


    QTestSlave slave;
    bool showDebug;
    bool started;
};

Q_GLOBAL_STATIC(TestSlaveServer, testSlaveServer);


void TestSlaveServer::startService()
{
    short aut_port = DEFAULT_AUT_PORT;
    bool parseOk;
    bool setAutPort = false;
    QStringList args = QCoreApplication::arguments();
    QByteArray autPortEnvBa = qgetenv("QTUITEST_PORT");
    QString autPortEnv = QString(autPortEnvBa);
    QByteArray debugEnvBa = qgetenv("QTUITEST_DEBUG");
    QString debugEnv = QString(debugEnvBa);


#ifndef QTUITEST_DEBUG
    int debugValue = debugEnv.toInt(&parseOk);
    if (parseOk && (debugValue > 0))
        showDebug = true;
#else
    showDebug = true;
#endif

    if (showDebug)
        qWarning("QtUiTest: Start service");

    for (int index = 0; index < args.count(); index++){
        if (args[index].endsWith("qtuitestrunner")){
            if (showDebug)
                qWarning() << "QtUiTest: Not starting TestSlaveServer::startService for qtuitestrunner";
            return;
        }
    }

    if (started){
        if (showDebug)
            qWarning() << "QtUiTest: Closing TestSlaveServer connection";

        close();
    }
    if (!autPortEnv.isEmpty()){
        aut_port = autPortEnv.toUShort(&parseOk);
        if (!parseOk){
            aut_port = DEFAULT_AUT_PORT;
            qWarning() << "QtUiTest: Unable to parse QTUITEST_PORT" << autPortEnv;
        }else{
            setAutPort = true;
            if (showDebug)
                qWarning() << "QtUiTest: Set port via QTUITEST_PORT to" << aut_port;
        }
    }
    for (int index = 0; index < args.count(); index++){
        if ((args[index] == QLatin1String("-autport")) && (index + 1 < args.count())){
            aut_port = args[index + 1].toUShort(&parseOk);
            if (!parseOk){
                aut_port = DEFAULT_AUT_PORT;
                qWarning() << "QtUiTest: Unable to parse -autport" << args[index];
            }else{
                setAutPort = true;
                if (showDebug)
                    qWarning() << "QtUiTest: Set port via -autport to" << aut_port;
            }
        }
    }

    if (!listen(QHostAddress::Any, aut_port)) {
        qWarning() << "QtUiTest: couldn't listen for connections on " << aut_port << " :"
            << errorString() << " started :" << started;
        started = false;
        close();
    }else {
        started = true;
        if (showDebug)
            qWarning() << "QtUiTest: listening for connections on" << aut_port;
    }
}

void TestSlaveServer::stopService()
{
    if (showDebug)
        qWarning("QtUiTest : Stoping service");

    close();
    started = false;
}

QtUiTestStylePlugin::~QtUiTestStylePlugin()
{
    testSlaveServer()->stopService();
}

void QtUiTestStylePlugin::initSlave()
{
#ifdef Q_OS_SYMBIAN
    struct ifreq ifReq;
    strcpy( ifReq.ifr_name, "qtuitest");
    int err = setdefaultif( &ifReq );
#endif

    testSlaveServer()->startService();

#if defined(Q_WS_WIN) || defined(Q_WS_MAC)
    // Use QFileDialog instead of native OS file dialog
    qt_use_native_dialogs = false;
#endif

#ifdef Q_OS_MAC
    QTimer::singleShot(200, this, SLOT(raiseWindow()));
#endif
}

QStringList QtUiTestStylePlugin::keys() const
{
    return QStringList() << "QtUiTest_NoStyle" << "QtUiTest";
}

QStyle *QtUiTestStylePlugin::create(const QString &key)
{
    static bool initialised = false;

    if (key.toLower() == "qtuitest_nostyle") {
        // QApplication::style() has failed to initialise app style
        // so is trying all styles. In this case we DO NOT want the
        // QtUiTest style to be initialised unintentionally...
        initialised = true;
    }
    if (key.toLower() == "qtuitest" && !initialised) {
        initSlave();
        initialised = true;
#ifdef Q_OS_SYMBIAN
        return new QS60Style();
#elif defined Q_WS_MAC
        return new QMacStyle();
#else
        return qApp->style();
#endif
    }
    return 0;
}

void QtUiTestStylePlugin::raiseWindow()
{
    QWidget *aw = qApp->activeWindow();
    if (!aw) {
        bool found = false;
        foreach (QWidget *w, qApp->topLevelWidgets()) {
            if (w->isVisible()) {
                found = true;
                w->raise();
                break;
            }
        }
        if (!found) qWarning() << "no visible window";
    }
}

#include <qplugin.h>
Q_EXPORT_PLUGIN2(QtUiTestStylePlugin, QtUiTestStylePlugin)
