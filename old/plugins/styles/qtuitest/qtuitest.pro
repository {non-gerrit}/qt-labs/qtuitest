TEMPLATE=lib
CONFIG+=plugin
SOURCES+=style.cpp
HEADERS+=style.h
TARGET=qtuiteststyle
TARGET=$$qtLibraryTarget($$TARGET)
QT+=network
INCLUDEPATH+=$$SRCROOT/libqtslave
INCLUDEPATH+=$$SRCROOT
DEPENDPATH+=$$SRCROOT

include($$SRCROOT/libqtuitest/libqtuitest.pri)

target.path=$$[QT_INSTALL_PLUGINS]/styles
unix:!symbian {
    MOC_DIR=$$OUT_PWD/.moc
    OBJECTS_DIR=$$OUT_PWD/.obj
    INSTALLS+=target
    !mac {
        LIBS+=-L$$BUILDROOT/lib -lqtslave
    }
}

symbian {
    TARGET.EPOCALLOWDLLDATA=1
    TARGET.CAPABILITY += AllFiles ReadDeviceData ReadUserData SwEvent WriteUserData NetworkServices
    MOC_DIR=$$OUT_PWD/moc
    OBJECTS_DIR=$$OUT_PWD/obj
    LIBS+= -lqtslave.lib -lws32 -leuser -lcone
}

mac {
    CONFIG(debug,debug|release): LIBS += -L$$BUILDROOT/lib -lqtslave_debug
    CONFIG(release,debug|release): LIBS += -L$$BUILDROOT/lib -lqtslave
}

win32 {
    CONFIG(debug,debug|release):  LIBS+= -L$$BUILDROOT/libqtslave/debug -lqtslaved
    CONFIG(release,debug|release):LIBS+= -L$$BUILDROOT/libqtslave/release -lqtslave
}

INSTALLS += target
